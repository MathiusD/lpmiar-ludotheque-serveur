package lp.projects.init.serveur.db.loan;

import lp.projects.init.serveur.BaseAPITest;
import lp.projects.init.serveur.core.auth.Role;
import lp.projects.init.serveur.core.game.Game;
import lp.projects.init.serveur.core.loan.Loan;
import lp.projects.init.serveur.core.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Class for test repo of MinimalLoan
 */
public class MinimalLoanRepoTest extends BaseAPITest {

    /**
     * Method for test import
     */
    @Test
    public void testImportMinimalLoan() {
        cleanRepo();
        Assertions.assertEquals(minimalLoanRepository.count(), 0);
        Assertions.assertEquals(gameRepository.count(), 0);
        Assertions.assertEquals(userRepository.count(), 0);
        Assertions.assertEquals(roleRepository.count(), 0);
        Loan loan = libraryLoans.get(0);
        Game game = games.get(games.indexOf(loan.getGame()));
        User user = users.get(users.indexOf(loan.getUser()));
        Role role = Role.ALL_ROLES.get(Role.ALL_ROLES.indexOf(user.getRole()));
        roleRepository.save(role);
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), role);
        userRepository.save(user);
        Assertions.assertEquals(userRepository.count(), 1);
        Assertions.assertEquals(userRepository.findAll().get(0), user);
        gameRepository.save(game);
        Assertions.assertEquals(gameRepository.count(), 1);
        Assertions.assertEquals(gameRepository.findAll().get(0), game);
        minimalLoanRepository.save(loan);
        Assertions.assertEquals(minimalLoanRepository.count(), 1);
        Assertions.assertEquals(minimalLoanRepository.findAll().get(0), loan);
    }
}
