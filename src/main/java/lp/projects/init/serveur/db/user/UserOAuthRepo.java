package lp.projects.init.serveur.db.user;

import lp.projects.init.serveur.core.user.UserOAuth;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface represent Repo for UserOAuth
 */
public interface UserOAuthRepo  extends JpaRepository<UserOAuth, String>{
}
