package lp.projects.init.serveur.responses;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test NotFoundException
 */
public class NotFoundExceptionTest {

    /**
     * For test Instantiation of NotFoundException
     */
    @Test
    public void testInstantiation() {
        Error notFoundError = new Error(HttpStatus.NOT_FOUND);
        NotFoundException baseNotFound = new NotFoundException();
        assertEquals(baseNotFound, notFoundError);
        notFoundError = new Error(HttpStatus.NOT_FOUND, "truc");
        assertNotEquals(baseNotFound, notFoundError);
        baseNotFound = new NotFoundException("truc");
        assertEquals(baseNotFound, notFoundError);
    }
}
