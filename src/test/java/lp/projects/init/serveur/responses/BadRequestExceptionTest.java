package lp.projects.init.serveur.responses;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Class for test BadRequestException
 */
public class BadRequestExceptionTest {

    /**
     * For test Instantiation of BadRequestException
     */
    @Test
    public void testInstantiation() {
        Error badRequestError = new Error(HttpStatus.BAD_REQUEST);
        BadRequestException baseBadRequest = new BadRequestException();
        assertEquals(baseBadRequest, badRequestError);
        badRequestError = new Error(HttpStatus.BAD_REQUEST, "truc");
        assertNotEquals(baseBadRequest, badRequestError);
        baseBadRequest = new BadRequestException("truc");
        assertEquals(baseBadRequest, badRequestError);
    }
}
