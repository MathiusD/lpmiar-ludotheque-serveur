package lp.projects.init.serveur.responses;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test ForbiddenException
 */
public class ForbiddenExceptionTest {

    /**
     * For test Instantiation of ForbiddenException
     */
    @Test
    public void testInstantiation() {
        Error forbiddenError = new Error(HttpStatus.FORBIDDEN);
        ForbiddenException baseForbidden = new ForbiddenException();
        assertEquals(baseForbidden, forbiddenError);
        forbiddenError = new Error(HttpStatus.FORBIDDEN, "truc");
        assertNotEquals(baseForbidden, forbiddenError);
        baseForbidden = new ForbiddenException("truc");
        assertEquals(baseForbidden, forbiddenError);
    }
}
