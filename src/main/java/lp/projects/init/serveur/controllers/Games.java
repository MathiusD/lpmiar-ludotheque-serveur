package lp.projects.init.serveur.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lp.projects.init.serveur.DBRepo;
import lp.projects.init.serveur.core.auth.Token;
import lp.projects.init.serveur.core.game.*;
import lp.projects.init.serveur.core.loan.Loan;
import lp.projects.init.serveur.core.loan.LoanRelated;
import lp.projects.init.serveur.responses.AccessDeniedException;
import lp.projects.init.serveur.responses.Error;
import lp.projects.init.serveur.responses.InvalidTokenException;
import lp.projects.init.serveur.responses.NotFoundException;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static lp.projects.init.serveur.core.game.GameSchema.ALL_SCHEMA;

/**
 * Class represent Controller of Games
 */
@RestController
public class Games extends DBRepo {

    /**
     * For get games in this library (With filter if you want)
     * @param type String represent Type of games you are looking for
     * @param schema String represent Schema for manage amount of data received
     * @param state String represent State of games you are looking for
     * @param minYear Integer represent minYear of games you are looking for
     * @param nameGame String represent Name of games you are looking for
     * @param companyName String represent Games of company you are looking for
     * @param illustrator String represent Games of specific illustrator you are looking for
     * @param author String represent Games of specific author you are looking for
     * @param start Integer represent start of results (ex : start = 2, request all games except before 2nd game)
     * @param max Integer represent max result displayed
     * @param location String represent Location Filter
     * @param minNbPlayer Integer represent Minimum Number of Player wanted
     * @return games
     */
    @CrossOrigin
    @Operation(summary = "Get games in this library")
    @ApiResponse(responseCode = "200", description = "View all games",
        content = @Content(mediaType = "application/json",
            array = @ArraySchema(uniqueItems = true, schema = @Schema(implementation = MinimalGame.class)
            )
        )
    )
    @RequestMapping(value = "/games", method = RequestMethod.GET)
    @NonNull
    public List<? extends MinimalGame> games(@Parameter(description = "Type of games you are looking for",
                                               example = "Cartes") @RequestParam(required = false) @Nullable
                                             String type, @Parameter(
                                                 description = "Schema for manage amount of data received",
                                                    example = "fullGame",
                                                    examples = {
                                                        @ExampleObject(value = "fullGame",
                                                            description = "For get games with all attributes"),
                                                        @ExampleObject(value = "shortGame",
                                                            description = "For get games with all attributes except companyName, description, author, illustrator, demoLink, location"),
                                                        @ExampleObject(value = "minimalGame",
                                                            description = "For get games with only id, name and State of Game")
                                                    }
                                             ) @RequestParam(required = false, defaultValue = "minimalGame")
                                             @Nullable String schema, @RequestParam(required = false)
                                             @Parameter(description = "State of games you are looking for",
                                                example = "available") @Nullable String state,
                                             @RequestParam(required = false) @Parameter(
                                                description = "Minimal year to play of games you are looking for",
                                                example = "12") @Nullable Integer minYear,
                                             @RequestParam(required = false) @Parameter(
                                                description = "Name of games you are looking for",
                                                example = "Monopoly") @Nullable String nameGame,
                                             @RequestParam(required = false) @Parameter(
                                                description = "Games of company you are looking for",
                                                example = "Haba") @Nullable String companyName,
                                             @RequestParam(required = false) @Parameter(
                                                description = "List of game of the illustrator you are looking for",
                                                example = "Martin") @Nullable String illustrator,
                                             @RequestParam(required = false) @Parameter(
                                                description = "List of game of the author you are looking for",
                                                example = "Theo et Ora Coster") @Nullable String author,
                                             @RequestParam(required = false) @Parameter(
                                                description = "Number of games you want to display",
                                                example = "50") @Nullable Integer max,
                                             @RequestParam(required = false) @Parameter(
                                                description = "Start to display with game number x",
                                                example = "1") @Nullable Integer start,
                                             @RequestParam(required = false) @Parameter(
                                                     description = "Location Filter",
                                                     example = "RDC") @Nullable String location,
                                             @RequestParam(required = false) @Parameter(
                                                     description = "Minimum Number of Player wanted",
                                                     example = "3") @Nullable Integer minNbPlayer) {
        GameSchema schemaUsed = GameSchema.MINIMAL_DATA;
        for (GameSchema gameSchema : ALL_SCHEMA)
            if (gameSchema.name.equalsIgnoreCase(schema))
                schemaUsed = gameSchema;
        List<Game> baseGames = gameRepository.findAll();
        List<Game> defaultGamesReturn = new ArrayList<>(baseGames);
        defaultGamesReturn.removeIf(game ->
            (type != null && (game.getType() == null || !game.getType().equalsIgnoreCase(type))) ||
            (location != null && (game.getLocation()== null || !game.getLocation().equalsIgnoreCase(location))) ||
            (state != null && !game.getGameState().getName().equalsIgnoreCase(state)) ||
            (companyName != null && (game.getCompanyName() == null ||
                    !game.getCompanyName().equalsIgnoreCase(companyName))) ||
            (nameGame != null && (!game.getName().equalsIgnoreCase(nameGame))) ||
            (minYear != null && (game.getYearRecommended() == null || game.getYearRecommended().getMin() > minYear)) ||
            (illustrator != null && (game.getIllustrator() == null ||
                    !game.getIllustrator().equalsIgnoreCase(illustrator))) ||
            (author != null && (game.getAuthor() == null || !game.getAuthor().equalsIgnoreCase(author))) ||
            (minNbPlayer != null && (game.getNbPlayer() == null || game.getNbPlayer().getMin() > minNbPlayer))
        );
        List<Game> gamesReturn = new ArrayList<>(defaultGamesReturn);
        gamesReturn.removeIf(game ->
            (max != null &&
                max - 1 + (start != null ? start : 0) < defaultGamesReturn.indexOf(game)) ||
            (start != null && (start > defaultGamesReturn.indexOf(game))));
        return schemaUsed.matchingGameWithThisSchema(gamesReturn);
    }

    /**
     * For get Game by id
     * @param id String represent id of Game research
     * @return game
     */
    @CrossOrigin
    @Operation(summary = "Get a Game by its id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found the game",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Game.class)
            )
        ),
        @ApiResponse(responseCode = "404", description = "Game not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Error.class)
            )
        )
    })
    @RequestMapping(value = "/games/{id}", method = RequestMethod.GET)
    @NonNull
    public Game game(@Parameter(description = "Id of game you are looking for", example = "T111")
                         @PathVariable(value="id") @NonNull String id) throws Error {
        return gameRepository.findById(id).orElseThrow(new NotFoundException(
            String.format("Game with the given id (%s) was not found", id)));
    }

    /**
     * For borrow specific Game
     * @param token String represent Token for identify your account
     * @param id String represent id of Game related
     * @return borrowResult
     */
    @CrossOrigin
    @Operation(summary = "For borrow specific Game by its id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Response of borrow Request",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = BorrowResult.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "404", description = "Game not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Error.class)
            )
        )
    })
    @RequestMapping(value = "/games/{id}/take", method = RequestMethod.GET)
    @NonNull
    public BorrowResult takeGame(@Parameter(description = "Token for identify your account",
                                    example = "abcEFG145") @RequestHeader(required = false)
                                 @Nullable String token, @Parameter(
                                     description = "Id of game you are looking for", example = "T111")
                                 @PathVariable(value="id") @NonNull String id) throws Error {
        Token tokenValue = identifyToken(token);
        Game game = game(id);
        if (!game.getGameState().getIsAvailable())
            return new BorrowResult(String.format("Game isn't available (Reason : %s)",
                    game.getGameState().getNote()));
        game.setGameState(GameState.REQUESTED);
        updateGame(game);
        Loan loan = new Loan(game, tokenValue.getUser(), TimeUnit.DAYS, 30);
        insertLoan(loan);
        return new BorrowResult(loan);
    }

    /**
     * For update Game by id
     * @param token String represent Token for identify your account
     * @param id String represent id of Game research
     * @param update GameUpdate represent instructions for update this game
     * @return gameUpdateResult
     */
    @CrossOrigin
    @Operation(summary = "For update Game by id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Result of Game Update",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = GameUpdateResult.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        ),
        @ApiResponse(responseCode = "404", description = "Game not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Error.class)
            )
        )
    })
    @RequestMapping(value = "/games/{id}", method = RequestMethod.POST)
    @NonNull
    public GameUpdateResult updateGame(@Parameter(description = "Token for identify your account",
                                       example = "abcEFG145") @RequestHeader(required = false)
                                       @Nullable String token,
                                       @Parameter(description = "Id of game you are looking for", example = "T111")
                                       @PathVariable(value="id") @NonNull String id,
                                       @Parameter(description = "Object contain update instruction")
                                       @RequestBody GameUpdate update) throws Error {
        Token tokenRelated = identifyToken(token);
        Game game = game(id);
        grantAccess(tokenRelated, String.format("/games/%s", game.getId()));
        String tempMessage = "";
        boolean errorOccurred = false;
        String raisonProvided;
        if (update.state != null && !update.state.equals(game.getGameState())) {
            raisonProvided = null;
            boolean hasLoanLinked = loanRelated(token, id, null).getCurrentLoanRelated() != null;
            if (hasLoanLinked) {
                errorOccurred = true;
                raisonProvided = "Loan are linked to this game close this loan before perform this action";
            } else
                game.setGameState(update.state);
            tempMessage = String.format("%s%n\tState are%s updated%s.",
                tempMessage, hasLoanLinked ? "n't" : "",
                raisonProvided != null ? String.format(" (Reason : %s)", raisonProvided) : "");
        }
        if (update.description != null && !update.description.equals(game.getDescription())) {
            game.setDescription(update.description);
            tempMessage = String.format("%s%n\tDescription are updated.", tempMessage);
        }
        if (update.location != null && !update.location.equals(game.getLocation())) {
            game.setLocation(update.location);
            tempMessage = String.format("%s%n\tLocation are updated.", tempMessage);
        }
        if (tempMessage.equals(""))
            return new GameUpdateResult(game, "Game doesn't updated because any update are provided");
        else
            updateGame(game);
        return new GameUpdateResult(game, String.format(
            "Game are %s.%s", errorOccurred ? "updated with errors" : "successfully updated", tempMessage)
        );
    }

    /**
     * For show loans related to specificGame
     * @param token String represent Token for identify your account
     * @param id String represent id of Game related
     * @param schema String represent Schema for manage amount of data received
     * @return loanRelated
     */
    @CrossOrigin
    @Operation(summary = "For show loans related to specificGame")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Loans Related to this Game",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = LoanRelated.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        ),
        @ApiResponse(responseCode = "404", description = "Game not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Error.class)
            )
        )
    })
    @RequestMapping(value = "/games/{id}/loanRelated", method = RequestMethod.GET)
    @NonNull
    public LoanRelated loanRelated(@Parameter(description = "Token for identify your account",
                                   example = "abcEFG145") @RequestHeader(required = false)
                                   @Nullable String token, @Parameter(
                                   description = "Id of game you are looking for", example = "T111")
                                   @PathVariable(value="id") @NonNull String id, @Parameter(
                                        description = "Schema for manage amount of data received",
                                        example = "fullGame",
                                        examples = {
                                            @ExampleObject(value = "fullGame",
                                                description = "For get games with all attributes"),
                                            @ExampleObject(value = "shortGame",
                                                description = "For get games with all attributes except companyName, description, author, illustrator, demoLink, location"),
                                            @ExampleObject(value = "minimalGame",
                                                description = "For get games with only id, name and State of Game")
                                        }
                                    ) @RequestParam(required = false, defaultValue = "minimalGame")
                                   @Nullable String schema) throws Error {
        Token tokenRelated = identifyToken(token);
        GameSchema schemaUsed = GameSchema.MINIMAL_DATA;
        for (GameSchema gameSchema : ALL_SCHEMA)
            if (gameSchema.name.equalsIgnoreCase(schema))
                schemaUsed = gameSchema;
        Game game = game(id);
        grantAccess(tokenRelated, String.format("/games/%s/loanRelated", game.getId()));
        List<Loan> loansRelated = loanRepository.findAll();
        loansRelated.removeIf(loan -> !(loan.getGame().equals(game)));
        return new LoanRelated(loansRelated, schemaUsed.matchingGameWithThisSchema(game));
    }

    /**
     * For upload games from csv file
     * @param token String represent Token for identify your account
     * @return gamesUploaded
     */
    @CrossOrigin
    @Operation(summary = "For upload games from csv file")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Loans Related to this Game",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = GameUploadSummary.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        )
    })
    @RequestMapping(value = "/games/import", method = RequestMethod.POST,
        consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @NonNull
    public GameUploadSummary uploadGamesFromCSV(@Parameter(description = "Token for identify your account",
                                   example = "abcEFG145") @RequestHeader(required = false)
                                   @Nullable String token, @NonNull @Parameter(
                                   description = "File used for extract games to upload")
                                   @RequestParam MultipartFile file) throws Error {
        identifyToken(token, true, "/games/import");
        List<Game> gamesExtracted;
        try {
            gamesExtracted = Game.extractGamesFromCSV(file);
        } catch (IOException e) {
            return new GameUploadSummary("Error occurred during file extraction !");
        }
        List<Game> gamesInserted = new ArrayList<>();
        List<Game> gamesNotInserted = new ArrayList<>();
        for (Game game : gamesExtracted) {
            try {
                game(game.getId());
                gamesNotInserted.add(game);
            } catch (NotFoundException notFoundException) {
                insertGame(game);
                gamesInserted.add(game);
            }
        }
        return new GameUploadSummary(gamesInserted, gamesNotInserted);
    }

    /**
     * For get games schema for manage amount of data received
     * @return schemas
     */
    @CrossOrigin
    @Operation(summary = "Get games Schema for manage amount of data received")
    @ApiResponse(responseCode = "200", description = "View All Games Schema",
        content = @Content(mediaType = "application/json",
            array = @ArraySchema(uniqueItems = true, schema = @Schema(implementation = GameSchema.class))
        )
    )
    @RequestMapping(value = "/games/schemas", method = RequestMethod.GET)
    @NonNull
    public List<GameSchema> schemas() {
        return ALL_SCHEMA;
    }

    /**
     * For get games types in this library
     * @return types
     */
    @CrossOrigin
    @Operation(summary = "Get games types in this Library")
    @ApiResponse(responseCode = "200", description = "View All Games Types",
        content = @Content(mediaType = "application/json",
            array = @ArraySchema(uniqueItems = true,
                schema = @Schema(implementation = String.class, example = "Cartes", name = "Type")
            )
        )
    )
    @RequestMapping(value = "/games/types", method = RequestMethod.GET)
    @NonNull
    public List<String> types() {
        List<String> types = new ArrayList<>();
        for (Game game : gameRepository.findAll())
            if (game.getType() != null && !types.contains(game.getType().toLowerCase().trim()))
                types.add(game.getType().toLowerCase().trim());
        Collections.sort(types);
        return types;
    }

    /**
     * For get games states in this library
     * @return types
     */
    @CrossOrigin
    @Operation(summary = "Get games states in this Library")
    @ApiResponse(responseCode = "200", description = "View All Games States",
        content = @Content(mediaType = "application/json",
            array = @ArraySchema(uniqueItems = true,
                schema = @Schema(implementation = GameState.class)
            )
        )
    )
    @RequestMapping(value = "/games/states", method = RequestMethod.GET)
    @NonNull
    public List<GameState> states() {
        return GameState.ALL_STATES;
    }

    /**
     * For get company games related to games in this Library
     * @return companyNames
     */
    @CrossOrigin
    @Operation(summary = "Get company games related to games in this Library")
    @ApiResponse(responseCode = "200", description = "View All Company Name related to Library Games",
        content = @Content(mediaType = "application/json",
            array = @ArraySchema(uniqueItems = true,
                schema = @Schema(implementation = String.class, example = "Haba", name = "companyName")
            )
        )
    )
    @RequestMapping(value = "/games/companyNames", method = RequestMethod.GET)
    @NonNull
    public List<String> companyNames() {
        List<String> companyNames = new ArrayList<>();
        for (Game game : gameRepository.findAll())
            if (game.getCompanyName() != null && !companyNames.contains(game.getCompanyName().toLowerCase().trim()))
                companyNames.add(game.getCompanyName().toLowerCase().trim());
        Collections.sort(companyNames);
        return companyNames;
    }
}