package lp.projects.init.serveur.db.user;

import lp.projects.init.serveur.core.user.MinimalUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface represent Repo for MinimalUser
 */
public interface MinimalUserRepo  extends JpaRepository<MinimalUser, String>{
}
