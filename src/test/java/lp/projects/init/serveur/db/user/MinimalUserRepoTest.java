package lp.projects.init.serveur.db.user;

import lp.projects.init.serveur.BaseAPITest;
import lp.projects.init.serveur.core.auth.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Class for test repo of MinimalUser
 */
public class MinimalUserRepoTest extends BaseAPITest {

    /**
     * Method for test import
     */
    @Test
    public void testImportMinimalUser() {
        cleanRepo();
        Assertions.assertEquals(minimalUserRepository.count(), 0);
        Assertions.assertEquals(roleRepository.count(), 0);
        roleRepository.save(Role.MANAGER);
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), Role.MANAGER);
        minimalUserRepository.save(my);
        Assertions.assertEquals(minimalUserRepository.count(), 1);
        Assertions.assertEquals(minimalUserRepository.findAll().get(0), my);
    }

    /**
     * Method for test import by own method
     */
    @Test
    public void testMethodOfDBRepo() {
        cleanRepo();
        Assertions.assertEquals(minimalUserRepository.count(), 0);
        Assertions.assertEquals(roleRepository.count(), 0);
        Assertions.assertTrue(insertUser(my));
        Assertions.assertFalse(insertUser(my));
        Assertions.assertEquals(minimalUserRepository.count(), 1);
        Assertions.assertEquals(minimalUserRepository.findAll().get(0), my);
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), Role.MANAGER);
        Assertions.assertTrue(removeUser(my));
        Assertions.assertFalse(removeUser(my));
        Assertions.assertEquals(minimalUserRepository.count(), 0);
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), Role.MANAGER);
    }
}
