package lp.projects.init.serveur.db.loan;

import lp.projects.init.serveur.core.loan.MinimalLoan;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface represent Repo for MinimalLoan
 */
public interface MinimalLoanRepo extends JpaRepository<MinimalLoan, String> {
}
