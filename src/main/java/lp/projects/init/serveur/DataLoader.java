package lp.projects.init.serveur;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * Class represent base Data Loader
 */
@Component
public class DataLoader extends DBData implements ApplicationRunner {

    /**
     * Method for clean Database and insert data
     * @param args ApplicationArguments of app
     */
    public void run(ApplicationArguments args) {
        insertData();
    }
}
