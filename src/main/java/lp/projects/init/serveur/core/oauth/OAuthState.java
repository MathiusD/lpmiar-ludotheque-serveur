package lp.projects.init.serveur.core.oauth;

import lp.projects.init.serveur.core.user.MinimalUser;
import org.apache.commons.codec.binary.Base64;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Class represent state for OAuth transaction
 */
public class OAuthState {

    public final String key;
    public final String codeVerifier;
    public final String resourceReached;
    public final MinimalUser userRelated;
    public final OAuthProvider provider;
    public final String headerRelated;
    public final String requestKey;
    protected ResponseEntity<?> requestResult;
    private static final List<String> codeVerifierInActivity = new ArrayList<>();
    private static final List<String> keyInActivity = new ArrayList<>();
    private static final List<String> requestKeyInActivity = new ArrayList<>();

    /**
     * Method for generate random code verifier
     * @return codeVerifier
     */
    @NonNull
    private static String generateRandomCodeVerifier() {
        SecureRandom sr = new SecureRandom();
        byte[] code = new byte[32];
        sr.nextBytes(code);
        String codeVerifier = java.util.Base64.getUrlEncoder().withoutPadding().encodeToString(code);
        if (codeVerifierInActivity.contains(codeVerifier))
            return generateRandomCodeVerifier();
        codeVerifierInActivity.add(codeVerifier);
        return codeVerifier;
    }

    /**
     * Method for generate random key
     * @return key
     */
    @NonNull
    private static String generateKey() {
        String key = UUID.randomUUID().toString();
        if (keyInActivity.contains(key))
            return generateKey();
        keyInActivity.add(key);
        return key;
    }

    /**
     * Full Constructor of OAuthState
     * @param key Key used
     * @param codeVerifier codeVerifier used for transaction
     * @param resourceReached Path of resource reached
     * @param userRelated User related if applicable
     * @param provider Provider related to this exchange
     * @param requestKey String represent key given in request
     * @param headerRelated String represent header used for this request
     */
    private OAuthState(@NonNull String key, @NonNull String codeVerifier,
                       @NonNull String resourceReached, @Nullable MinimalUser userRelated,
                       @NonNull OAuthProvider provider, @Nullable String requestKey,
                       @Nullable String headerRelated) {
        if (requestKey != null) {
            if (requestKeyInActivity.contains(requestKey))
                throw new IllegalArgumentException("This userKey is already used");
            requestKeyInActivity.add(requestKey);
        }
        this.requestKey = requestKey;
        this.headerRelated = headerRelated;
        this.key = key;
        this.codeVerifier = codeVerifier;
        this.resourceReached = resourceReached;
        this.userRelated = userRelated;
        this.provider = provider;
    }

    /**
     * Public Constructor of OAuthState
     * @param resourceReached Path of resource reached
     * @param userRelated User related if applicable
     * @param provider Provider related to this exchange
     * @param requestKey String represent key given in request
     * @param headerRelated String represent header used for this request
     */
    public OAuthState(@NonNull String resourceReached, @Nullable MinimalUser userRelated,
                      @NonNull OAuthProvider provider, @Nullable String requestKey,
                      @Nullable String headerRelated) {
        this(generateKey(), generateRandomCodeVerifier(), resourceReached,
                userRelated, provider, requestKey, headerRelated);
    }

    /**
     * For destroy this OAuthState
     */
    public void invalidate(){
        codeVerifierInActivity.remove(codeVerifier);
        keyInActivity.remove(key);
        requestKeyInActivity.remove(requestKey);
    }

    /**
     * Method for get Hash related to value provided
     * @param value String represent value provide
     * @return hashOfValue
     */
    @Nullable
    private static String getHashOf(@NonNull String value) {
        try {
            byte[] bytes = value.getBytes(StandardCharsets.US_ASCII);
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(bytes, 0, bytes.length);
            byte[] digest = md.digest();
            return Base64.encodeBase64URLSafeString(digest);
        }
        catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    /**
     * Getter of Hash related to codeVerifier
     * @return hashOfCodeVerifier
     */
    @Nullable
    public String getHashOfCodeVerifier() {
        return getHashOf(codeVerifier);
    }

    /**
     * Getter for requestResult
     * @return requestResult
     */
    @Nullable
    public ResponseEntity<?> getRequestResult() {
        return requestResult;
    }

    /**
     * Setter for requestResult
     */
    public void setRequestResult(@NonNull ResponseEntity<?> requestResult) {
        this.requestResult = requestResult;
    }
}
