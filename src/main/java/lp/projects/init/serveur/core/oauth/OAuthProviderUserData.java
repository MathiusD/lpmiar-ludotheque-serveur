package lp.projects.init.serveur.core.oauth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.user.MinimalUser;
import lp.projects.init.serveur.core.user.User;
import lp.projects.init.serveur.core.user.UserOAuth;
import org.springframework.core.env.Environment;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.HashMap;
import java.util.Objects;

/**
 * Class represent where is UserData of OAuth provider
 */
public class OAuthProviderUserData extends PropertyData {

    @JsonIgnore
    public final OAuthProvider provider;
    private String id;
    private String name;
    private String email;
    private String nickname;

    /**
     * Basic Constructor of OAuthProviderUserData
     * @param provider OAuthProvider represent base provider
     * @param environment Environment used for get properties related to this provider
     */
    public OAuthProviderUserData(@NonNull OAuthProvider provider, @NonNull Environment environment) {
        super(environment);
        this.provider = provider;
    }

    /**
     * For get parent related to this PropertyData
     * @return parent
     */
    @Nullable
    @Override
    @Schema(description = "Parent of this Data")
    public PropertyData getParent() {
        if (parent == null)
            parent = provider.getParent() != null ?
                ((OAuthProvider) provider.getParent()).getUserData() : null;
        return parent;
    }

    /**
     * Method return base name for get properties related to this object
     * @return baseProperty
     */
    @Override
    @JsonIgnore
    @NonNull
    public String getBaseProperty() {
        return String.format("%s.userData", provider.getBaseProperty());
    }

    /**
     * Getter for id
     * @return id
     */
    @NonNull
    public String getId() {
        if (id == null)
            id = getSpecificProperty("id");
        return id;
    }

    /**
     * Getter for name
     * @return name
     */
    @NonNull
    public String getName() {
        if (name == null) {
            name = getSpecificProperty("name");
            if (name.equals("name")) {
                String firstName = getSpecificPropertyWithoutDefaultValue("firstName");
                String lastName = getSpecificPropertyWithoutDefaultValue("lastName");
                if (firstName != null || lastName != null)
                    name = String.format("%s;%s", firstName != null ? firstName : "",
                        lastName != null ? lastName : "");
            }
        }
        return name;
    }

    /**
     * Getter for email
     * @return email
     */
    @NonNull
    public String getEmail() {
        if (email == null)
            email = getSpecificProperty("email");
        return email;
    }

    /**
     * Getter for nickName
     * @return nickName
     */
    @NonNull
    public String getNickname() {
        if (nickname == null) {
            nickname = getSpecificPropertyWithoutDefaultValue("nickname");
            if (nickname == null)
                nickname = Objects.requireNonNull(
                    getSpecificProperty("firstName", "nickname"));
        }
        return nickname;
    }

    /**
     * Method for extract property in givenData if present
     * @param nameProperty target property
     * @param usedData Data used
     * @return property
     */
    @Nullable
    public String extractPropertyOf(@NonNull String nameProperty, @NonNull HashMap usedData) {
        return usedData.containsKey(nameProperty) ?
            usedData.get(nameProperty) != null ?
                String.valueOf(usedData.get(nameProperty)) : null
            : null;
    }

    /**
     * Method for create UserOAuth from userData fetched from related provider
     * @param receivedData HashMap represent userData
     * @return userOAuth
     */
    @NonNull
    public UserOAuth createUserOAuthFrom(@NonNull HashMap receivedData) {
        String id = extractPropertyOf(getId(), receivedData);
        if (id == null)
            throw new IllegalArgumentException("Required Data isn't present for exploit this OAuth (Id is required)");
        return new UserOAuth(createUserFrom(receivedData), provider, id);
    }

    /**
     * Method for create User from userData fetched from related provider
     * @param receivedData HashMap represent userData
     * @return User
     */
    @NonNull
    public User createUserFrom(@NonNull HashMap receivedData) {
        String email = extractPropertyOf(getEmail(), receivedData);
        String nickName = extractPropertyOf(getNickname(), receivedData);
        if (email == null || nickName == null)
            throw new IllegalArgumentException(String.format(
                "Required Data isn't present for exploit this OAuth (%s%s)",
                email == null ? "Email is required" : "", nickName == null ?
                    String.format("%sNickName is required", email == null ? " and " : "") : ""
            ));
        String name = null;
        String lastName = null;
        String firstName = null;
        if (!getName().contains(";"))
            name = extractPropertyOf(getName(), receivedData);
        else {
            String[] baseNameRegex = getName().split(";");
            firstName = !baseNameRegex[0].equals("") ? extractPropertyOf(baseNameRegex[0],
                receivedData) : null;
            lastName = !baseNameRegex[1].equals("") ? extractPropertyOf(baseNameRegex[1],
                receivedData) : null;
        }
        if (!getName().contains(";") && name != null) {
            String[] nameSplit = name.split(" ");
            if (nameSplit.length > 0) {
                if (nameSplit.length > 1) {
                    lastName = nameSplit[nameSplit.length - 1];
                    firstName = name.substring(0, (name.length() - lastName.length()) - 1);
                } else
                    firstName = nameSplit[0];
            }
        }
        return new User(new MinimalUser(lastName, firstName, email, nickName));
    }
}
