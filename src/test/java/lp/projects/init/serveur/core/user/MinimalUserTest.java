package lp.projects.init.serveur.core.user;

import lp.projects.init.serveur.core.game.MinimalGame;
import lp.projects.init.serveur.core.loan.Loan;
import org.junit.jupiter.api.Test;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test MinimalUser
 */
public class MinimalUserTest {

    /**
     * Assert MinimalUsers are exactly same attributes value
     * @param first MinimalUser represent first user
     * @param second MinimalUser represent second user
     */
    public static void assertUsersAreStrictlyEquals(@NonNull MinimalUser first, @NonNull MinimalUser second) {
        assertEquals(first, second);
        assertEquals(first.id, second.id);
        assertEquals(first.firstName, second.firstName);
        assertEquals(first.lastName, second.lastName);
    }

    /**
     * For test Instantiation of MinimalUser
     */
    @Test
    public void testMinimalUserInstantiation() {
        MinimalUser firstUser = new MinimalUser("ABC", "Example", "truc",
                "truc@mail.org", "Truc");
        MinimalUser tempUser = new MinimalUser(firstUser);
        assertUsersAreStrictlyEquals(firstUser, tempUser);
        MinimalGame game = new MinimalGame("machin", "truc");
        Loan loan = new Loan("zef", game, firstUser, 42);
        assertUsersAreStrictlyEquals(firstUser, new MinimalUser(loan));
    }

    /**
     * For test method extractMinimalUserFrom of MinimalUser
     */
    @Test
    public void testExtractMinimalUserFrom() {
        MinimalUser user = new MinimalUser("BIDULE", "Truc",
                "Machin", "truc@mail.org", "Truc");
        MinimalUser otherUser = new MinimalUser("PAS_BIDULE", "Truc",
                "Machin", "truc@mail.org", "Truc");
        MinimalGame game = new MinimalGame("Id", "truc");
        Loan loan = new Loan("Ba", game, user, 4600);
        Loan otherLoan = new Loan("Ba", game, otherUser, 4600);
        assertUsersAreStrictlyEquals(loan.getUser(), MinimalUser.extractMinimalUserFrom(loan));
        assertNotEquals(loan.getUser(), MinimalUser.extractMinimalUserFrom(otherLoan));
        assertUsersAreStrictlyEquals(loan.getUser(), MinimalUser.extractMinimalUserFrom(loan, otherLoan));
        List<Loan> temp = new ArrayList<>();
        temp.add(null);
        temp.add(loan);
        assertUsersAreStrictlyEquals(loan.getUser(), MinimalUser.extractMinimalUserFrom(temp));
        assertThrows(IllegalArgumentException.class, () ->
                        MinimalUser.extractMinimalUserFrom(null, null),
                "This method required at least one Loan"
        );
        assertThrows(IllegalArgumentException.class, () ->
                        MinimalUser.extractMinimalUserFrom(new ArrayList<>()),
                "This method required at least one Loan"
        );
    }
}
