package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.db.BaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;

/**
 * Class represent indication of year recommended
 */
@Embeddable
@Schema(description = "Object for represent indication of Year recommended to play Game")
public class YearRecommended extends MinMaxInteger {

    /**
     * Shit Constructor for YearRecommended Stored in DB
     * Used only by reflexion !
     */
    protected YearRecommended() {
        super();
    }

    /**
     * Basic Constructor for YearRecommended
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param andMoreYear Boolean represent if Game are played for minimum year and more (If true maximum is null)
     */
    @JsonCreator
    @PersistenceConstructor
    public YearRecommended(@NonNull @JsonProperty("min") Integer minYear,
                           @Nullable @JsonProperty("max") Integer maxYear,
                           @NonNull @JsonProperty("andMore") Boolean andMoreYear) {
        super(minYear, maxYear, andMoreYear);
    }

    /**
     * Basic Constructor for YearRecommended
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     */
    public YearRecommended(@NonNull Integer minYear, @Nullable Integer maxYear) {
        this(minYear, maxYear, false);
    }

    /**
     * Basic Constructor for YearRecommended
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param andMoreYear Boolean represent if Game are played for minimum year and more (If true maximum is null
     *                      else minimum and maximum are equal to actualYear)
     */
    public YearRecommended(@NonNull Integer actualYear, @NonNull Boolean andMoreYear) {
        this(actualYear, actualYear, andMoreYear);
    }

    /**
     * Basic Constructor for YearRecommended
     * @param minYear Integer represent minimum year for this Game
     */
    public YearRecommended(@NonNull Integer minYear) {
        this(minYear, true);
    }

    /**
     * Copy Constructor for YearRecommended
     * @param minMaxInteger NbPlayer represent instance used for copy this
     */
    public YearRecommended(@NonNull MinMaxInteger minMaxInteger) {
        this(minMaxInteger.min, minMaxInteger.max, minMaxInteger.andMore);
    }

    /**
     * Getter for andMore of YearRecommended
     * @return andMore
     */
    @Schema(description = "Boolean represent if Game are played for year player and more (If true maximum is null)",
            example = "false")
    @Override
    @NonNull
    @Access(AccessType.PROPERTY)
    public Boolean getAndMore() {
        return super.getAndMore();
    }

    /**
     * Getter for max of YearRecommended
     * @return max
     */
    @Schema(description = "Max Year of Player for play this Game", example = "7")
    @Override
    @Nullable
    @Access(AccessType.PROPERTY)
    public Integer getMax() {
        return super.getMax();
    }

    /**
     * Getter for min of YearRecommended
     * @return min
     */
    @Schema(description = "Minimum Year of Player for play this Game", example = "3")
    @Override
    @NonNull
    @Access(AccessType.PROPERTY)
    public Integer getMin() {
        return super.getMin();
    }

    @Nullable
    public static YearRecommended extractFrom(@Nullable String dataUsed) {
        MinMaxInteger baseData = MinMaxInteger.extractFrom(dataUsed);
        return baseData != null ? new YearRecommended(baseData) : null;
    }
}
