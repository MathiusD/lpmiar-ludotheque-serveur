package lp.projects.init.serveur.core.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.db.BaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.*;

import static javax.persistence.InheritanceType.SINGLE_TABLE;

/**
 * Class represent base information of Role of User in this Library
 */
@Entity
@Inheritance(strategy=SINGLE_TABLE)
@DiscriminatorValue("MIN_ROLE")
@Schema(description = "Base Informations of Role in this Library")
@JsonIgnoreProperties({"isAdmin"})
public class MinimalRole extends BaseEntity {

    @Id
    @Column(name = "role_name")
    @Schema(description = "Name of this role", example = "Customer")
    protected String name;
    @Column(name = "role_is_admin")
    @Schema(description = "Permission of this role", example = "true")
    protected boolean isAdmin;

    /**
     * Shit Constructor for MinimalRole Stored in DB
     * Used only by reflexion !
     */
    protected MinimalRole() {
        super();
    }

    /**
     * Basic constructor of MinimalRole
     * @param name String represent name of this role
     * @param isAdmin Boolean represent if this role are permission to execute sudo
     */
    @PersistenceConstructor
    @JsonCreator
    public MinimalRole(@NonNull String name, boolean isAdmin) {
        this.name = name;
        this.isAdmin = isAdmin;
    }

    /**
     * Basic constructor of MinimalRole
     * @param name String represent name of this role
     */
    public MinimalRole(@NonNull String name) {
        this(name, false);
    }

    /**
     * Basic constructor of MinimalRole by other role
     * @param otherRole MinimalRole represent other role for extract base information
     */
    public MinimalRole(@NonNull MinimalRole otherRole) {
        this(otherRole.getName(), otherRole.getIsAdmin());
    }

    /**
     * Method for get MinimalRole
     * @return minimalRole
     */
    @NonNull
    @JsonIgnore
    public MinimalRole getMinimalRole() {
        return new MinimalRole(this);
    }

    /**
     * Method for get name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Method for get isAdmin
     * @return isAdmin
     */
    public boolean getIsAdmin() {
        return isAdmin;
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof MinimalRole)) return false;
        MinimalRole minimalRole = (MinimalRole) o;
        return hashCode() == minimalRole.hashCode();
    }

    /**
     * Method for get HashCode of this MinimalRole
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name).toHashCode();
    }

    /**
     * Method for Write MinimalRole
     * @return minimalRoleReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("Role %s%s", name, isAdmin ? " and is Administrator" : "");
    }
}
