package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Class represent game data schemas
 */
@Schema(description = "Object To represent the different game data schemas")
public class GameSchema {

    @Schema(description = "Name of this Schema", example = "fullGame")
    public final String name;
    @Schema(description = "Explication for why or for what Schema exist",
            example = "For display full data of Game")
    public final String description;
    @JsonIgnore
    public final Class<? extends MinimalGame> gameClass;

    /**
     * Basic constructor for Game Schema
     * @param name String represent name of this Schema
     * @param description String represent why this schema exist
     * @param gameClass Class represent for what Game schema exist
     */
    public GameSchema(@NonNull String name, @Nullable String description,
                      @NonNull Class<? extends MinimalGame> gameClass) {
        this.name = name;
        this.description = description;
        this.gameClass = gameClass;
    }

    /**
     * Basic constructor for Game Schema
     * @param name String represent name of this Schema
     */
    public GameSchema(@NonNull String name) {
        this(name, (String) null);
    }
    /**
     * Basic constructor for Game Schema
     * @param name String represent name of this Schema
     * @param description String represent why this schema exist
     */
    @JsonCreator
    public GameSchema(@NonNull String name, @Nullable String description) {
        GameSchema foundSchema = null;
        for (GameSchema schema: ALL_SCHEMA)
            if (schema.name.equalsIgnoreCase(name) && foundSchema == null &&
                    (description == null || description.equalsIgnoreCase(schema.description)))
                foundSchema = schema;
        if (foundSchema == null)
            throw new IllegalArgumentException("This Schema don't match with any existing Schema");
        this.name = foundSchema.name;
        this.description = foundSchema.description;
        this.gameClass = foundSchema.gameClass;
    }

    /**
     * Basic constructor for Game Schema
     * @param name String represent name of this Schema
     * @param gameClass Class represent for what Game schema exist
     */
    public GameSchema(@NonNull String name, @NonNull Class<? extends MinimalGame> gameClass) {
        this(name, null, gameClass);
    }

    /**
     * Method for transform Game to Game related to this Schema
     * @param game Game represent Game used for convert
     * @return gameMatchingWithThisSchema
     */
    @NonNull
    public MinimalGame matchingGameWithThisSchema(@NonNull Game game) {
        try {
            Constructor<? extends MinimalGame> gameConstructor;
            try {
               gameConstructor = gameClass.getDeclaredConstructor(gameClass);
            } catch (NoSuchMethodException e) {
                gameConstructor = gameClass.getDeclaredConstructor(MinimalGame.class);
            }
            return gameConstructor.newInstance(game);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException
                | InvocationTargetException e) {
            throw new IllegalArgumentException("Class in this object doesn't have constructor match to following signature public <? extends MinimalGame>(MinimalGame)");
        }
    }

    /**
     * Method for transform Collection of Game to Games related to this Schema
     * @param games Collection of Game represent games used for convert
     * @return gameMatchingWithThisSchema
     */
    @NonNull
    public List<MinimalGame> matchingGameWithThisSchema(@NonNull Collection<Game> games) {
        List<MinimalGame> gamesMatching = new ArrayList<>();
        for(Game game: games)
            gamesMatching.add(matchingGameWithThisSchema(game));
        return gamesMatching;
    }

    /**
     * Method for transform Many Games to Games related to this Schema
     * @param games Many Games represent games used for convert
     * @return gameMatchingWithThisSchema
     */
    @NonNull
    public List<MinimalGame> matchingGameWithThisSchema(@NonNull Game... games) {
        return matchingGameWithThisSchema(Arrays.asList(games));
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof GameSchema)) return false;
        GameSchema gameSchema = (GameSchema) o;
        return hashCode() == gameSchema.hashCode();
    }

    /**
     * Method for get HashCode of this GameSchema
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(name).append(gameClass).toHashCode();
    }

    public static final GameSchema FULL_DATA = new GameSchema("fullGame",
            "For display all data related to this Game", Game.class);
    public static final GameSchema SHORT_DATA = new GameSchema("shortGame",
            "For display all data related to this Game except : companyName, description, author, illustrator, demoLink, location",
            ShortGame.class);
    public static final GameSchema MINIMAL_DATA = new GameSchema("minimalGame",
            "For display only id, name and State of Game", MinimalGame.class);
    public static final List<GameSchema> ALL_SCHEMA;

    static {
        List<GameSchema> temp = new ArrayList<>();
        temp.add(FULL_DATA);
        temp.add(SHORT_DATA);
        temp.add(MINIMAL_DATA);
        ALL_SCHEMA = temp;
    }

    /**
     * Method for Write GameSchema
     * @return gameSchemaReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("GameSchema %n for target class %s", name, gameClass.getSimpleName());
    }
}
