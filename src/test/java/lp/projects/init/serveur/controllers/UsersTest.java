package lp.projects.init.serveur.controllers;

import lp.projects.init.serveur.BaseAPITest;
import lp.projects.init.serveur.core.auth.Token;
import lp.projects.init.serveur.core.game.Game;
import lp.projects.init.serveur.core.game.GameSchema;
import lp.projects.init.serveur.core.game.MinMax;
import lp.projects.init.serveur.core.loan.Loan;
import lp.projects.init.serveur.core.loan.MinimalLoan;
import lp.projects.init.serveur.core.loan.State;
import lp.projects.init.serveur.core.user.MinimalUser;
import lp.projects.init.serveur.core.user.User;
import lp.projects.init.serveur.core.user.UserOAuth;
import lp.projects.init.serveur.core.user.UserSchema;
import lp.projects.init.serveur.responses.AccessDeniedException;
import lp.projects.init.serveur.responses.InvalidTokenException;
import lp.projects.init.serveur.responses.NotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.NonNull;

import java.util.*;
import java.util.stream.Stream;

import static lp.projects.init.serveur.core.game.MinimalGameTest.assertGamesAreStrictlyEquals;
import static lp.projects.init.serveur.core.user.UserTest.assertUsersAreStrictlyEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UsersTest extends BaseAPITest {

    /**
     * Method for get base uri of Users testing
     * @return baseUri for springTests
     */
    @NonNull
    @Override
    public String baseUri() {
        return String.format("%s/users", super.baseUri());
    }

    /**
     * Test for route /users/{id} with errors
     */
    @ParameterizedTest
    @MethodSource("testTokenProvider")
    public void testUserError(Token token) {
        assertEquals(new InvalidTokenException(), customExchange("/truc", InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", "truc");
        assertEquals(customExchange("/truc", headers,  InvalidTokenException.class),
                new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", token.getToken());
        assertEquals(new NotFoundException("User with the given id (truc) was not found"),
                customExchange("/truc", headers,  NotFoundException.class));
    }

    public Stream<Arguments> testTokenProvider() {
        List<Arguments> args = new ArrayList<>();
        for (Token token: userTokens)
            args.add(Arguments.of(token));
        return extractSample(args, maxDataForParameterizedTest,false).stream();
    }

    /**
     * Test for route /users/{id}
     */
    @ParameterizedTest
    @MethodSource("testUserProvider")
    public void testUser(User user, Token token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", token.getToken());
        assertUsersAreStrictlyEquals(token.getUser(),
            customExchange(String.format("/%s", token.getUser().getId()), headers, User.class));
        if (user.equals(token.getUser()) || token.getUser().getRole().getIsAdmin())
            assertUsersAreStrictlyEquals(user,
                customExchange(String.format("/%s", user.getId()), headers, User.class));
        else
            assertEquals(new AccessDeniedException(token.getUser(),
                    String.format("/users/%s", user.getId())),
                customExchange(String.format("/%s", user.getId()), headers,
                    AccessDeniedException.class));
    }

    public Stream<Arguments> testUserProvider() {
        List<Arguments> args = new ArrayList<>();
        for(User user : users)
            for (Token token: userTokens)
                args.add(Arguments.of(user, token));
        return extractSample(args, maxDataForParameterizedTest,false).stream();
    }

    /**
     * Test for route /users/{id}/loans
     */
    @ParameterizedTest
    @MethodSource("testUserLoansProvider")
    public void testUserLoans(Token token, User user, State state, GameSchema gameSchema,
                              Long endAt, Long startedAt, Game game, Long since, Long before) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", token.getToken());
        String baseUri = String.format("/%s/loans", token.getUser().getId());
        boolean stateIsSet = state != null;
        boolean schemaIsSet = gameSchema != null;
        boolean endAtIsSet = endAt != null;
        boolean startedAtIsSet = startedAt != null;
        boolean gameIsSet = game != null;
        boolean sinceIsSet = since != null;
        boolean beforeIsSet = before != null;
        boolean stateOrSchemaIsSet = stateIsSet || schemaIsSet;
        boolean stateOrSchemaOrEndAtIsSet = stateOrSchemaIsSet || endAtIsSet;
        boolean stateOrSchemaOrEndAtOrStartedAtIsSet = stateOrSchemaOrEndAtIsSet || startedAtIsSet;
        boolean anyoneIsSetExceptSinceAndBefore = stateOrSchemaOrEndAtOrStartedAtIsSet || gameIsSet;
        boolean anyoneIsSetExceptBefore = anyoneIsSetExceptSinceAndBefore || sinceIsSet;
        boolean anyoneIsSet = anyoneIsSetExceptBefore || beforeIsSet;
        String urlArgs = anyoneIsSet ? String.format("?%s%s%s%s%s%s%s",
                stateIsSet ? String.format("state=%s", state.getAccessKey()) : "",
                schemaIsSet ? String.format("%sgameSchema=%s",
                    stateIsSet ? "&" : "", gameSchema.name) : "",
                endAtIsSet ? String.format("%sendAt=%d",
                        stateOrSchemaIsSet ? "&" : "", endAt) : "",
                startedAtIsSet ? String.format("%sstartedAt=%d",
                        stateOrSchemaOrEndAtIsSet ? "&" : "", startedAt) : "",
                gameIsSet ? String.format("%sgameId=%s",
                        stateOrSchemaOrEndAtOrStartedAtIsSet ? "&" : "", game.getId()) : "",
                sinceIsSet ? String.format("%ssince=%d",
                        anyoneIsSetExceptSinceAndBefore ? "&" : "", since) : "",
                beforeIsSet ? String.format("%sbefore=%d",
                        anyoneIsSetExceptBefore ? "&" : "", before) : ""
            ) : "";
        checkUserLoan(token.getUser(), state, gameSchema, endAt, startedAt, game, since,
            before, urlArgs, headers);
        if (token.getUser().getRole().getIsAdmin()) {
            checkUserLoan(user, state, gameSchema, endAt, startedAt, game, since,
                before, urlArgs, headers);
        }
    }

    public void checkUserLoan(MinimalUser user, State state, GameSchema gameSchema,
                              Long endAt, Long startedAt, Game game, Long since,
                              Long before, String urlArgs, HttpHeaders headers) {
        String baseUri = String.format("/%s/loans", user.getId());
        List<MinimalLoan> loansExpected = new ArrayList<>();
        for (Loan loan : libraryLoans)
            if ((user.equals(loan.getUser())) &&
                (state == null || loan.getState().equals(state)) &&
                (endAt == null || loan.getEndAt().equals(endAt)) &&
                (startedAt == null || loan.getStartedAt().equals(startedAt)) &&
                (game == null || loan.getGame().equals(game)) &&
                (since == null || since <= loan.getStartedAt()) &&
                (before == null || before >= loan.getStartedAt())
            )
            loansExpected.add(loan.getMinimalLoan());
        List<MinimalLoan> loansReceived = Arrays.asList(customExchange(
                String.format("%s%s", baseUri, urlArgs), headers, MinimalLoan[].class));
        assertEquals(loansReceived.size(), loansExpected.size());
        for (MinimalLoan expected : loansExpected) {
            if (gameSchema != null) {
                MinimalLoan received = loansReceived.get(loansReceived.indexOf(expected));
                assertGamesAreStrictlyEquals(games.get(games.indexOf(received.getGame())),
                    received.getGame());
            }
        }
    }

    public Stream<Arguments> testUserLoansProvider() {
        List<Arguments> argsUsed = new ArrayList<>();
        List<State> states = extractSample(State.ALL_STATES);
        List<GameSchema> schemas = extractSample(GameSchema.ALL_SCHEMA);
        List<Long> endAt = new ArrayList<>();
        endAt.add(getRandomLong(endAtMinMax));
        endAt.add(this.endAt.get(new Random().nextInt(this.endAt.size())));
        endAt.add(null);
        List<Long> startedAt = new ArrayList<>();
        startedAt.add(getRandomLong(startedAtMinMax));
        startedAt.add(this.startedAt.get(new Random().nextInt(this.startedAt.size())));
        startedAt.add(null);
        List<Long> duration = new ArrayList<>();
        duration.add(getRandomLong(startedAtMinMax));
        duration.add(null);
        if (startedAtMinMax.getMin() > Long.MIN_VALUE)
            duration.add(startedAtMinMax.getMin() - 1);
        if (startedAtMinMax.getMax() != null && startedAtMinMax.getMax() < Long.MAX_VALUE)
            duration.add(startedAtMinMax.getMax() + 1);
        List<Game> games = extractSample(this.games);
        for(User user : users)
            for(Token token: userTokens)
                for(State state : states)
                    for(GameSchema schema : schemas)
                        for (Long end : endAt)
                            for (Long start : startedAt)
                                for (Game game : games)
                                    for (Long since : duration)
                                        for (Long before : duration)
                                            argsUsed.add(Arguments.of(token, user,
                                                state, schema, end, start, game,
                                                since, before));
        return extractSample(argsUsed, maxDataForParameterizedTest,false).stream();
    }

    /**
     * Test for route /users/{id}/loans with errors
     */
    @ParameterizedTest
    @MethodSource("testUserProvider")
    public void testUserLoansError(User user, Token token) {
        assertEquals(new InvalidTokenException(), customExchange("/truc/loans",
            InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", "truc");
        assertEquals(customExchange("/truc/loans", headers,  InvalidTokenException.class),
            new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", token.getToken());
        assertEquals(customExchange("/truc/loans", headers,  NotFoundException.class),
            new NotFoundException("User with the given id (truc) was not found"));
        if (!token.getUser().getRole().getIsAdmin() && !user.equals(token.getUser()))
            assertEquals(customExchange(String.format("/%s/loans", user.getId()), headers,
                AccessDeniedException.class), new AccessDeniedException(token.getUser(),
                String.format("/users/%s/loans", user.getId())));
    }

    /**
     * Test for route /users/schema
     */
    @Test
    public void testGameSchemas () {
        assertEquals(UserSchema.ALL_SCHEMA, Arrays.asList(
            customExchange("/schemas", UserSchema[].class)));
    }

    /**
     * Test for route /users/{id}/oauth
     */
    @ParameterizedTest
    @MethodSource("testUserOAuthProvider")
    public void testUserOAuth(User user, Token token, List<UserOAuth> oAuthRelated) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", token.getToken());
        if (user.equals(token.getUser()) || token.getUser().getRole().getIsAdmin()) {
            List<UserOAuth> receivedOAuth = Arrays.asList(
                customExchange(String.format("/%s/oauth", user.getId()), headers, UserOAuth[].class));
            assertEquals(oAuthRelated.size(), receivedOAuth.size());
            boolean found;
            for (UserOAuth expected : oAuthRelated) {
                found = false;
                for (UserOAuth received : receivedOAuth)
                    if (expected.getUserIdOfProvider()
                            .equals(received.getUserIdOfProvider()) &&
                        expected.getProviderNameRelated()
                            .equals(received.getProviderNameRelated())) {
                        found = true;
                        break;
                    }
                assertTrue(found, String.format("%s is not found in %s", expected, receivedOAuth));
            }
        } else
            assertEquals(new AccessDeniedException(token.getUser(),
                        String.format("/users/%s/oauth", user.getId())),
                customExchange(String.format("/%s/oauth", user.getId()), headers,
                    AccessDeniedException.class));
    }

    public Stream<Arguments> testUserOAuthProvider() {
        List<Arguments> args = new ArrayList<>();
        Map<User, List<UserOAuth>> oAuthMap = new HashMap<>();
        for(User user : users)
            for (UserOAuth oAuth : oAuthList)
                if (oAuth.getUserRelated().equals(user)) {
                    if (!oAuthMap.containsKey(user))
                        oAuthMap.put(user, new ArrayList<>());
                    List<UserOAuth> temp =  oAuthMap.get(user);
                    temp.add(oAuth);
                    oAuthMap.put(user, temp);
                }
        for (Map.Entry<User, List<UserOAuth>> oAuthUsers : oAuthMap.entrySet())
            for (Token token: userTokens)
                args.add(Arguments.of(oAuthUsers.getKey(), token, oAuthUsers.getValue()));
        return extractSample(args, maxDataForParameterizedTest,false).stream();
    }

    /**
     * Test for route /users/{id}/oauth with errors
     */
    @ParameterizedTest
    @MethodSource("testTokenProvider")
    public void testUserOAuthError(Token token) {
        assertEquals(new InvalidTokenException(), customExchange("/truc/oauth", InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", "truc");
        assertEquals(customExchange("/truc/oauth", headers,  InvalidTokenException.class),
                new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", token.getToken());
        assertEquals(new NotFoundException("User with the given id (truc) was not found"),
                customExchange("/truc/oauth", headers,  NotFoundException.class));
    }
}
