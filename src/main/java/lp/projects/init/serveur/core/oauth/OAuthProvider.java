package lp.projects.init.serveur.core.oauth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.core.env.Environment;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Objects;

/**
 * Class represent OAuth Provider for this API
 */
@Schema(description = "Provider supported by this API")
public class OAuthProvider extends PropertyData {

    @Schema(description = "Name of this provider", example = "discord")
    public final String nameProvider;
    private String appId;
    private String secret;
    private String provider;
    private String baseEndpoint;
    private String baseOAuthEndpoint;
    private String authorizeEndpoint;
    private String tokenEndpoint;
    private String scope;
    private String authOpts;
    private String userEndpoint;
    private Boolean withCodeVerification;
    private OAuthProviderUserData userData;

    /**
     * Basic Constructor of OAuthProvider
     * @param nameProvider String represent name of Provider
     * @param environment Environment used for get properties related to this provider
     */
    public OAuthProvider(@NonNull String nameProvider, @NonNull Environment environment) {
        super(environment);
        this.nameProvider = nameProvider;
    }

    /**
     * Method return base name for get properties related to this object
     * @return baseProperty
     */
    @Override
    @JsonIgnore
    @NonNull
    public String getBaseProperty() {
        return String.format("oauth.%s", nameProvider);
    }

    /**
     * For get parent related to this PropertyData
     * @return parent
     */
    @Nullable
    @Schema(description = "Parent of this Data")
    public PropertyData getParent() {
        if (parent == null) {
            String parentName = getSpecificPropertyWithoutDefaultValue("parent", false);
            if (parentName != null)
                parent = new OAuthProvider(parentName, environment);
        }
        return parent;
    }

    /**
     * Method for get specific property related to endpoint in this provider or this parent
     * @param nameProperty property research
     * @return property
     */
    @NonNull
    public String getSpecificEndpointProperty(@NonNull String nameProperty) {
        return getSpecificEndpointProperty(nameProperty, true);
    }

    /**
     * Method for get specific property related to endpoint in this provider or this parent
     * @param nameProperty property research
     * @param specificEndpoint Specific endpoint used between provider and property
     * @return property
     */
    @NonNull
    public String getSpecificEndpointProperty(@NonNull String nameProperty,
                                              @Nullable String specificEndpoint) {
        return getSpecificEndpointProperty(nameProperty, true, specificEndpoint);
    }

    /**
     * Method for get specific property related to endpoint in this provider
     * @param nameProperty property research
     * @param isMightInherit boolean represent if research use parent or not
     * @return property
     */
    @NonNull
    public String getSpecificEndpointProperty(@NonNull String nameProperty, boolean isMightInherit) {
        return getSpecificEndpointProperty(nameProperty, isMightInherit, null);
    }

    /**
     * Method for get specific property related to endpoint in this provider
     * @param nameProperty property research
     * @param isMightInherit boolean represent if research use parent or not
     * @param specificEndpoint Specific endpoint used between provider and property
     * @return property
     */
    @NonNull
    public String getSpecificEndpointProperty(@NonNull String nameProperty, boolean isMightInherit,
                                              @Nullable String specificEndpoint) {
        String property = getSpecificPropertyWithoutDefaultValue(nameProperty, isMightInherit);
        return String.format("%s%s", property != null && property.startsWith("http") ? "" :
            specificEndpoint != null ? specificEndpoint : getProvider(),
            property != null ? property : "");
    }

    /**
     * Getter for appId
     * @return appId
     */
    @NonNull
    @JsonIgnore
    public String getAppId() {
        if (appId == null)
            appId = getSpecificPropertyWithoutDefaultValue("appId", false);
        return Objects.requireNonNull(appId);
    }

    /**
     * Getter for secret
     * @return secret
     */
    @NonNull
    @JsonIgnore
    public String getSecret() {
        if (secret == null)
            secret = getSpecificPropertyWithoutDefaultValue("secret", false);
        return Objects.requireNonNull(secret);
    }

    /**
     * Getter for loginPath
     * @return loginPath
     */
    @NonNull
    @Schema(description = "Path for use this provider with this api", example = "/login/github")
    public String getLoginPath() {
        return String.format("/login/%s", nameProvider);
    }

    /**
     * Getter for linkPath
     * @return linkPath
     */
    @NonNull
    @Schema(description = "Path for use this provider with this api", example = "/link/github")
    public String getLinkPath() {
        return String.format("/link/%s", nameProvider);
    }

    /**
     * Getter for OAuthPath
     * @return OAuthPath
     */
    @NonNull
    @JsonIgnore
    public String getOAuthPath() {
        return String.format("/oauth/%s", nameProvider);
    }

    /**
     * Getter for provider
     * @return provider
     */
    @NonNull
    @Schema(description = "Base uri for this provider", example = "https://google.com/")
    public String getProvider() {
        if (provider == null)
            provider = getSpecificPropertyWithoutDefaultValue("provider");
        return Objects.requireNonNull(provider);
    }

    /**
     * Getter for scope
     * @return scope
     */
    @NonNull
    @JsonIgnore
    public String getScope() {
        if (scope == null)
            scope = getSpecificPropertyWithoutDefaultValue("scope");
        return Objects.requireNonNull(scope);
    }

    /**
     * Getter for authOpts
     * @return authOpts
     */
    @Nullable
    @JsonIgnore
    public String getAuthOpts() {
        if (authOpts == null)
            authOpts = getSpecificPropertyWithoutDefaultValue("authOpts");
        return authOpts;
    }

    /**
     * Getter for withCodeVerification
     * @return withCodeVerification
     */
    @NonNull
    @JsonIgnore
    public boolean getWithCodeVerification() {
        if (withCodeVerification == null) {
            withCodeVerification = Boolean.parseBoolean(
                getSpecificProperty("withCodeVerification", "true"));
        }
        return withCodeVerification;
    }

    /**
     * Getter for userData
     * @return userData
     */
    @NonNull
    @JsonIgnore
    public OAuthProviderUserData getUserData() {
        if (userData == null)
            userData = new OAuthProviderUserData(this, this.environment);
        return userData;
    }

    /**
     * Getter for baseEndpoint
     * @return baseEndpoint
     */
    @Nullable
    @JsonIgnore
    public String getBaseEndpoint() {
        if (baseEndpoint == null)
            baseEndpoint = getSpecificEndpointProperty("baseEndpoint");
        return baseEndpoint;
    }

    /**
     * Getter for baseOAuthEndpoint
     * @return baseOAuthEndpoint
     */
    @Nullable
    @JsonIgnore
    public String getBaseOAuthEndpoint() {
        if (baseOAuthEndpoint == null)
            baseOAuthEndpoint = getSpecificEndpointProperty("baseOAuthEndpoint",
                getBaseEndpoint());
        return baseOAuthEndpoint;
    }

    /**
     * Getter for authorizeEndpoint
     * @return authorizeEndpoint
     */
    @NonNull
    @JsonIgnore
    public String getAuthorizeEndpoint() {
        if (authorizeEndpoint == null)
            authorizeEndpoint = getSpecificEndpointProperty("authorizeEndpoint",
                getBaseOAuthEndpoint());
        return authorizeEndpoint;
    }

    /**
     * Getter for tokenEndpoint
     * @return tokenEndpoint
     */
    @NonNull
    @JsonIgnore
    public String getTokenEndpoint() {
        if (tokenEndpoint == null)
            tokenEndpoint = getSpecificEndpointProperty("tokenEndpoint",
                getBaseOAuthEndpoint());
        return tokenEndpoint;
    }

    /**
     * Getter for userEndpoint
     * @return userEndpoint
     */
    @NonNull
    @JsonIgnore
    public String getUserEndpoint() {
        if (userEndpoint == null)
            userEndpoint = getSpecificEndpointProperty("userEndpoint",
                getBaseEndpoint());
        return userEndpoint;
    }

    /**
     * Method for Write OAuthProvider
     * @return OAuthProviderReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("%s (%s)", nameProvider, getProvider());
    }


    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OAuthProvider)) return false;
        OAuthProvider provider = (OAuthProvider) o;
        return provider.hashCode() == hashCode();
    }

    /**
     * Method for get HashCode of this OAuthProvider
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(nameProvider).toHashCode();
    }
}
