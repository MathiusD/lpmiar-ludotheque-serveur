package lp.projects.init.serveur;

import junit.framework.AssertionFailedError;
import lp.projects.init.serveur.core.game.MinMax;
import org.junit.After;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.http.*;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import lp.projects.init.serveur.responses.Error;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.TestPropertySources;

import java.util.*;

/**
 * Base Class for test Controller or App
 */
@PropertySources(value = {@PropertySource("classpath:test.properties")})
@TestPropertySources(value = {@TestPropertySource("classpath:test.properties")})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BaseAPITest extends DBData {

    @LocalServerPort
    protected int port;

    @Autowired
    protected TestRestTemplate restTemplate;
    @Value("${test.sample.maxSize}")
    protected int maxDataForParameterizedTest;

    /**
     * Default Constructor for BaseApiTest
     */
    public BaseAPITest() {
        super(true);
    }
    /**
     * Method for clean and insert data in database before and after each tests
     */
    @BeforeEach
    @AfterEach
    @Override
    public void insertData() {
        super.insertData();
    }

    /**
     * Method for clean database after tests
     */
    @After
    @Override
    public void cleanRepo() {
        super.cleanRepo();
    }

    /**
     * Method for get Random Long in bound provided
     * @param bound Bound used
     * @return randomLong
     */
    @NonNull
    public Long getRandomLong(MinMax<Long> bound) {
        long returnValue;
        do {
            returnValue = new Random().nextLong();
        } while (returnValue > bound.getMin() &&
                returnValue < (bound.getMax() != null ?
                    bound.getMax() : Long.MAX_VALUE));
        return returnValue;
    }

    /**
     * Method for extract Sample from List of Data
     * @param baseData BaseData
     * @param <T> Type of Object
     * @return sample
     */
    public static <T> List<T> extractSample(List<T> baseData) {
        return extractSample(baseData, true);
    }


    /**
     * Method for extract Sample from List of Data
     * @param baseData BaseData
     * @param addNullValue If null value must be in sample
     * @param <T> Type of Object
     * @return sample
     */
    public static <T> List<T> extractSample(List<T> baseData, boolean addNullValue) {
        return extractSample(baseData, baseData.size() + (addNullValue ? 1 : 0), addNullValue);
    }

    /**
     * Method for extract Sample from List of Data
     * @param baseData BaseData
     * @param sampleSize Size of Sample
     * @param <T> Type of Object
     * @return sample
     */
    public static <T> List<T> extractSample(List<T> baseData, int sampleSize) {
        return extractSample(baseData, sampleSize, true);
    }

    /**
     * Method for extract Sample from List of Data
     * @param baseData BaseData
     * @param sampleSize Size of Sample
     * @param addNullValue If null value must be in sample
     * @param <T> Type of Object
     * @return sample
     */
    public static <T> List<T> extractSample(List<T> baseData, int sampleSize, boolean addNullValue) {
        List<T> sample = new ArrayList<>(baseData);
        if (sampleSize > sample.size())
            sampleSize = sample.size();
        if (sampleSize < sample.size()) {
            int iterations = sample.size() - sampleSize;
            for (int iteration = 0; iteration < iterations; iteration++) {
                sample.remove(new Random().nextInt(sample.size() - 1));
            }
        }
        if (addNullValue && !sample.contains(null))
            sample.add(null);
        return sample;
    }

    /**
     * Method for get base uri of API testing
     * @return baseUri for springTests
     */
    @NonNull
    public String baseUri() {
        return String.format("http://localhost:%d", port);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param entity HttpEntity represent entity used for this exchange
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @param statusExpected HttpStatus represent Status expected
     * @return exchangeReturn
     */
    @NonNull
    public <T> ResponseEntity<T> baseCustomExchange(@NonNull String endpoint, @NonNull HttpMethod method,
                                                    @NonNull HttpEntity<?> entity, @NonNull Class<T> objectClass,
                                                    @Nullable HttpStatus statusExpected) {
        String uri = String.format("%s%s", baseUri(), endpoint);
        try {
            ResponseEntity<T> rawOut = this.restTemplate.exchange(uri, method, entity, objectClass);
            if (statusExpected == null) {
                if (Error.class.isAssignableFrom(objectClass)) {
                    try {
                        Class<Error> tempObjectClass = (Class<Error>) objectClass;
                        statusExpected = (HttpStatus) tempObjectClass.getMethod(
                            "getBaseHttpStatus").invoke(tempObjectClass);
                    } catch (Exception ignored) {
                        statusExpected = HttpStatus.OK;
                    }
                } else {
                    statusExpected = HttpStatus.OK;
                }
            }
            if (statusExpected.value() != rawOut.getStatusCode().value())
                throw new Exception(String.format("Expected %d HttpCode and receive %d HttpCode",
                    statusExpected.value(), rawOut.getStatusCode().value()));
            return rawOut;
        } catch (Exception e) {
            throw new AssertionFailedError(String.format(
                "For %s %s%n%s are occurred with content :%s%nAnd with message :%n%s",
                method, uri, e.getClass().getSimpleName(),
                this.restTemplate.exchange(uri, method, entity, String.class),
                e.getMessage() != null ? e.getMessage() : "")
            );
        }
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param entity HttpEntity represent entity used for this exchange
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @param statusExpected HttpStatus represent Status expected
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull String endpoint, @NonNull HttpMethod method, @NonNull HttpEntity<?> entity,
                                @NonNull Class<T> objectClass, @Nullable HttpStatus statusExpected) {
        return Objects.requireNonNull(baseCustomExchange(endpoint, method, entity, objectClass,
            statusExpected).getBody());
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param headers HttpHeaders represent headers used for this exchange
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @param statusExpected HttpStatus represent Status expected
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull String endpoint, @NonNull HttpMethod method, @NonNull HttpHeaders headers,
                                @NonNull Class<T> objectClass, @Nullable HttpStatus statusExpected) {
        return customExchange(endpoint, method, new HttpEntity<>(headers), objectClass, statusExpected);
    }
    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param entity HttpEntity represent entity used for this exchange
     * @param objectClass Class represent Class of return type
     * @param statusExpected HttpStatus represent Status expected
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull String endpoint, @NonNull HttpEntity<?> entity,
                                @NonNull Class<T> objectClass, @Nullable HttpStatus statusExpected) {
        return customExchange(endpoint, HttpMethod.GET, entity, objectClass, null);
    }

    /**
     * Method for launch Exchange with specific options
     * @param method HttpMethod represent method used for this exchange
     * @param entity HttpEntity represent entity used for this exchange
     * @param objectClass Class represent Class of return type
     * @param statusExpected HttpStatus represent Status expected
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull HttpMethod method, @NonNull HttpEntity<?> entity,
                                @NonNull Class<T> objectClass, @Nullable HttpStatus statusExpected) {
        return customExchange("", method, entity, objectClass, null);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param entity HttpEntity represent entity used for this exchange
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull String endpoint, @NonNull HttpMethod method, @NonNull HttpEntity<?> entity,
                                @NonNull Class<T> objectClass) {
        return customExchange(endpoint, method, entity, objectClass, null);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param headers HttpHeaders represent headers used for this exchange
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull String endpoint, @NonNull HttpMethod method, @NonNull HttpHeaders headers,
                                @NonNull Class<T> objectClass) {
        return customExchange(endpoint, method, new HttpEntity<>(headers), objectClass);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param objectClass Class represent Class of return type
     * @param statusExpected HttpStatus represent Status expected
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull String endpoint, @NonNull HttpMethod method,
                                @NonNull Class<T> objectClass, @Nullable HttpStatus statusExpected) {
        return customExchange(endpoint, method, new HttpHeaders(), objectClass, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param headers HttpHeaders represent headers used for this exchange
     * @param objectClass Class represent Class of return type
     * @param statusExpected HttpStatus represent Status expected
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull String endpoint, @NonNull HttpHeaders headers,
                                @NonNull Class<T> objectClass, @Nullable HttpStatus statusExpected) {
        return customExchange(endpoint, HttpMethod.GET, new HttpEntity<>(headers), objectClass, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param method HttpMethod represent method used for this exchange
     * @param headers HttpHeaders represent headers used for this exchange
     * @param objectClass Class represent Class of return type
     * @param statusExpected HttpStatus represent Status expected
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull HttpMethod method, @NonNull HttpHeaders headers,
                                @NonNull Class<T> objectClass, @Nullable HttpStatus statusExpected) {
        return customExchange("", method, new HttpEntity<>(headers), objectClass, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull String endpoint, @NonNull HttpMethod method,
                                @NonNull Class<T> objectClass) {
        return customExchange(endpoint, method, new HttpHeaders(), objectClass);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param headers HttpHeaders represent headers used for this exchange
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull String endpoint, @NonNull HttpHeaders headers,
                                @NonNull Class<T> objectClass) {
        return customExchange(endpoint, HttpMethod.GET, headers, objectClass);
    }

    /**
     * Method for launch Exchange with specific options
     * @param method HttpMethod represent method used for this exchange
     * @param headers HttpHeaders represent headers used for this exchange
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull HttpMethod method, @NonNull HttpHeaders headers,
                                @NonNull Class<T> objectClass) {
        return customExchange("", method, headers, objectClass);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param objectClass Class represent Class of return type
     * @param statusExpected HttpStatus represent Status expected
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull String endpoint, @NonNull Class<T> objectClass,
                                @Nullable HttpStatus statusExpected) {
        return customExchange(endpoint, HttpMethod.GET, objectClass, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param method HttpMethod represent method used for this exchange
     * @param objectClass Class represent Class of return type
     * @param statusExpected HttpStatus represent Status expected
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull HttpMethod method, @NonNull Class<T> objectClass,
                                @Nullable HttpStatus statusExpected) {
        return customExchange("", method, objectClass, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param headers HttpHeaders represent headers used for this exchange
     * @param objectClass Class represent Class of return type
     * @param statusExpected HttpStatus represent Status expected
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull HttpHeaders headers, @NonNull Class<T> objectClass,
                                @Nullable HttpStatus statusExpected) {
        return customExchange("", headers, objectClass, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param headers HttpHeaders represent headers used for this exchange
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull HttpHeaders headers,
                                @NonNull Class<T> objectClass) {
        return customExchange("", headers, objectClass);
    }

    /**
     * Method for launch Exchange with specific options
     * @param method HttpMethod represent method used for this exchange
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull HttpMethod method, @NonNull Class<T> objectClass) {
        return customExchange("", method, objectClass);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull String endpoint, @NonNull Class<T> objectClass) {
        return customExchange(endpoint, HttpMethod.GET, objectClass);
    }

    /**
     * Method for launch Exchange with specific options
     * @param objectClass Class represent Class of return type
     * @param statusExpected HttpStatus represent Status expected
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull Class<T> objectClass, @Nullable HttpStatus statusExpected) {
        return customExchange(HttpMethod.GET, objectClass, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param objectClass Class represent Class of return type
     * @param <T> Class represent type of return
     * @return exchangeReturn
     */
    @NonNull
    public <T> T customExchange(@NonNull Class<T> objectClass) {
        return customExchange("", objectClass);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param entity HttpEntity represent entity used for this exchange
     * @param statusExpected HttpStatus represent Status expected
     */
    public void customExchange(@NonNull String endpoint, @NonNull HttpMethod method, @NonNull HttpEntity<?> entity,
                                @Nullable HttpStatus statusExpected) {
        baseCustomExchange(endpoint, method, entity, Object.class, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param headers HttpHeaders represent headers used for this exchange
     * @param statusExpected HttpStatus represent Status expected
     */
    public void customExchange(@NonNull String endpoint, @NonNull HttpMethod method, @NonNull HttpHeaders headers,
                               @Nullable HttpStatus statusExpected) {
        customExchange(endpoint, method, new HttpEntity<>(headers), statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param headers HttpHeaders represent headers used for this exchange
     */
    public void customExchange(@NonNull String endpoint, @NonNull HttpMethod method, @NonNull HttpHeaders headers) {
        customExchange(endpoint, method, new HttpEntity<>(headers), (HttpStatus) null);
    }

    /**
     * Method for launch Exchange with specific options
     * @param method HttpMethod represent method used for this exchange
     * @param headers HttpHeaders represent headers used for this exchange
     * @param statusExpected HttpStatus represent Status expected
     */
    public void customExchange(@NonNull HttpMethod method, @NonNull HttpHeaders headers,
                               @Nullable HttpStatus statusExpected) {
        customExchange("", method, new HttpEntity<>(headers), statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param headers HttpHeaders represent headers used for this exchange
     * @param statusExpected HttpStatus represent Status expected
     */
    public void customExchange(@NonNull String endpoint, @NonNull HttpHeaders headers,
                               @Nullable HttpStatus statusExpected) {
        customExchange(endpoint, HttpMethod.GET, new HttpEntity<>(headers), statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param entity HttpEntity represent entity used for this exchange
     */
    public void customExchange(@NonNull String endpoint, @NonNull HttpMethod method, @NonNull HttpEntity<?> entity) {
        customExchange(endpoint, method, entity, (HttpStatus) null);
    }

    /**
     * Method for launch Exchange with specific options
     * @param method HttpMethod represent method used for this exchange
     * @param entity HttpEntity represent entity used for this exchange
     * @param statusExpected HttpStatus represent Status expected
     */
    public void customExchange(@NonNull HttpMethod method, @NonNull HttpEntity<?> entity,
                               @Nullable HttpStatus statusExpected) {
        customExchange("", method, entity, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param entity HttpEntity represent entity used for this exchange
     * @param statusExpected HttpStatus represent Status expected
     */
    public void customExchange(@NonNull String endpoint, @NonNull HttpEntity<?> entity,
                               @Nullable HttpStatus statusExpected) {
        customExchange(endpoint, HttpMethod.GET, entity, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     * @param statusExpected HttpStatus represent Status expected
     */
    public void customExchange(@NonNull String endpoint, @NonNull HttpMethod method, @Nullable HttpStatus statusExpected) {
        customExchange(endpoint, method, new HttpHeaders(), statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param method HttpMethod represent method used for this exchange
     */
    public void customExchange(@NonNull String endpoint, @NonNull HttpMethod method) {
        customExchange(endpoint, method, new HttpHeaders());
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param headers HttpHeaders represent headers used for this exchange
     */
    public void customExchange(@NonNull String endpoint, @NonNull HttpHeaders headers) {
        customExchange(endpoint, HttpMethod.GET, new HttpEntity<>(headers), (HttpStatus) null);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param entity HttpEntity represent entity used for this exchange
     */
    public void customExchange(@NonNull String endpoint, @NonNull HttpEntity<?> entity) {
        customExchange(endpoint, HttpMethod.GET, entity, (HttpStatus) null);
    }

    /**
     * Method for launch Exchange with specific options
     * @param method HttpMethod represent method used for this exchange
     * @param entity HttpEntity represent entity used for this exchange
     */
    public void customExchange(@NonNull HttpMethod method, @NonNull HttpEntity<?> entity) {
        customExchange("", method, entity);
    }

    /**
     * Method for launch Exchange with specific options
     * @param method HttpMethod represent method used for this exchange
     * @param headers HttpHeaders represent headers used for this exchange
     */
    public void customExchange(@NonNull HttpMethod method, @NonNull HttpHeaders headers) {
        customExchange("", method, new HttpEntity<>(headers));
    }

    /**
     * Method for launch Exchange with specific options
     * @param entity HttpEntity represent entity used for this exchange
     * @param statusExpected HttpStatus represent Status expected
     */
    public void customExchange(@NonNull HttpEntity<?> entity, @Nullable HttpStatus statusExpected) {
        customExchange("", entity, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param headers HttpHeaders represent headers used for this exchange
     * @param statusExpected HttpStatus represent Status expected
     */
    public void customExchange(@NonNull HttpHeaders headers, @Nullable HttpStatus statusExpected) {
        customExchange("", new HttpEntity<>(headers), statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     * @param statusExpected HttpStatus represent Status expected
     */
    public void customExchange(@NonNull String endpoint, @Nullable HttpStatus statusExpected) {
        customExchange(endpoint, HttpMethod.GET, statusExpected);
    }

    /**
     * Method for launch Exchange with specific options
     * @param endpoint String represent endpoint of API tested in this Test
     */
    public void customExchange(@NonNull String endpoint) {
        customExchange(endpoint, HttpMethod.GET);
    }

    /**
     * Method for launch Exchange with specific options
     * @param method HttpMethod represent method used for this exchange
     */
    public void customExchange(@NonNull HttpMethod method) {
        customExchange("", method);
    }

    /**
     * Method for launch Exchange with specific options
     * @param headers HttpHeaders represent headers used for this exchange
     */
    public void customExchange(@NonNull HttpHeaders headers) {
        customExchange("", new HttpEntity<>(headers));
    }

    /**
     * Method for launch Exchange with specific options
     * @param entity HttpEntity represent entity used for this exchange
     */
    public void customExchange(@NonNull HttpEntity<?> entity) {
        customExchange("", entity);
    }

    /**
     * Method for launch Exchange with specific options
     * @param statusExpected HttpStatus represent Status expected
     */
    public void customExchange(@Nullable HttpStatus statusExpected) {
        customExchange("", statusExpected);
    }
}
