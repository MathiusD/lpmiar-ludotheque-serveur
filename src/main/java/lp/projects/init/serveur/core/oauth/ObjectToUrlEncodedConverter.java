package lp.projects.init.serveur.core.oauth;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;

/**
 * Class used for convert object to FORM_URL_ENCODED
 * @param <T> Class used for conversion
 */
public class ObjectToUrlEncodedConverter<T> implements HttpMessageConverter<T>
{
    private static final String Encoding = "UTF-8";

    private final ObjectMapper mapper;

    /**
     * Default Constructor
     */
    public ObjectToUrlEncodedConverter()
    {
        this(new ObjectMapper());
    }

    /**
     * Basic Constructor
     * @param mapper ObjectMapper used
     */
    public ObjectToUrlEncodedConverter(@NonNull ObjectMapper mapper)
    {
        this.mapper = mapper;
    }

    /**
     * For know if mediaType is supported for reading
     * @param clazz target Class
     * @param mediaType target MediaType
     * @return canRead
     */
    @Override
    @NonNull
    public boolean canRead(@NonNull Class clazz, @NonNull MediaType mediaType)
    {
        return false;
    }

    /**
     * For know if mediaType is supported for writing
     * @param clazz target Class
     * @param mediaType target MediaType
     * @return canWrite
     */
    @Override
    @NonNull
    public boolean canWrite(@NonNull Class clazz, @NonNull MediaType mediaType)
    {
        return getSupportedMediaTypes().contains(mediaType);
    }

    /**
     * For know all supported mediaType
     * @return mediaTypesSupported
     */
    @Override
    @NonNull
    public List<MediaType> getSupportedMediaTypes()
    {
        return Collections.singletonList(MediaType.APPLICATION_FORM_URLENCODED);
    }

    /**
     * For read from input and convert to class given
     * @param clazz Class of return
     * @param inputMessage InputMessage used
     * @return messageConverted
     * @throws HttpMessageNotReadableException If Message isn't readable
     */
    @Override
    public T read(@NonNull Class clazz,
                  @NonNull HttpInputMessage inputMessage) throws HttpMessageNotReadableException
    {
        throw new NotImplementedException("Extraction for Application_Form_UrlEncoded isn't Supported");
    }

    /**
     * For write to output
     * @param o Object used
     * @param contentType ContentType requested
     * @param outputMessage OutputMessage used for write object
     * @throws HttpMessageNotWritableException If Message isn't writable
     */
    @Override
    public void write(@Nullable T o, @NonNull MediaType contentType,
                      @NonNull HttpOutputMessage outputMessage) throws HttpMessageNotWritableException
    {
        if (o != null)
        {
            String body = mapper.convertValue(o, UrlEncodedWriter.class).toString();
            try
            {
                outputMessage.getBody().write(body.getBytes(Encoding));
            }
            catch (IOException ignored)
            {
                // if UTF-8 is not supporter then I give up
            }
        }
    }

    /**
     * Class represent writer for Application_Form_UrlEncoded
     */
    private static class UrlEncodedWriter
    {
        private final StringBuilder out = new StringBuilder();

        /**
         * Method for write object
         * @param name Name of property used
         * @param property value of property
         * @throws UnsupportedEncodingException If Encoding isn't supported
         */
        @JsonAnySetter
        public void write(@NonNull String name, @Nullable Object property) throws UnsupportedEncodingException
        {
            if (out.length() > 0)
                out.append("&");
            out.append(URLEncoder.encode(name, Encoding)).append("=");
            if (property != null)
                out.append(URLEncoder.encode(property.toString(), Encoding));
        }

        /**
         * Method for Write UrlEncodedWriter
         * @return urlEncodedWriterReadable
         */
        @Override
        @NonNull
        public String toString()
        {
            return out.toString();
        }
    }
}