package lp.projects.init.serveur.core.game;

import org.assertj.core.annotations.NonNull;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test GameSchema
 */
public class GameSchemaTest extends GameTest {

    /**
     * Class for testing exception threw by GameSchema if constructor by copy is missing
     */
    public final static class testingGameInvalidConstructor extends MinimalGame {

        /**
         * Base Constructor for testingGameInvalid
         * @param id String represent id of Game
         * @param name String represent name of Game
         */
        public testingGameInvalidConstructor(@NonNull String id, @NonNull String name) {
            super(id, name);
        }
    }

    public final static GameSchema SchemaForInvalidConstructor = new GameSchema("Schema For Invalid Constructor",
        testingGameInvalidConstructor.class);

    /**
     * For test Instantiation of GameSchema
     */
    @Test
    public void testGameInstantiation() {
        assertEquals(new GameSchema(GameSchema.FULL_DATA.name), GameSchema.FULL_DATA);
        assertEquals(new GameSchema(GameSchema.FULL_DATA.name, GameSchema.FULL_DATA.description),
            GameSchema.FULL_DATA);
        assertThrows(IllegalArgumentException.class, () -> new GameSchema("truc"),
            "This Schema don't match with any existing Schema");
        assertThrows(IllegalArgumentException.class, () -> new GameSchema(GameSchema.FULL_DATA.name, "truc"),
                "This Schema don't match with any existing Schema");

    }
    /**
     * For test method matchingGameWithThisSchema of GameSchema
     */
    @Test
    public void testMatchingGameWithThisSchemaOfGameSchema() {
        Game firstExample = new Game("ABC", "Example", GameState.BORROWED, "Blip",
                new NbPlayer(1, 4), 3, new GameTime(15), "Example",
                "Machin", "Me", "Truc", "Bidule", "here");
        assertGamesAreStrictlyEquals(GameSchema.FULL_DATA.matchingGameWithThisSchema(firstExample), firstExample);
        assertEquals(GameSchema.FULL_DATA.matchingGameWithThisSchema(firstExample).getClass(),
                GameSchema.FULL_DATA.gameClass);
        assertGamesAreStrictlyEquals(GameSchema.SHORT_DATA.matchingGameWithThisSchema(firstExample), firstExample);
        assertEquals(GameSchema.SHORT_DATA.matchingGameWithThisSchema(firstExample).getClass(),
                GameSchema.SHORT_DATA.gameClass);
        assertGamesAreStrictlyEquals(GameSchema.MINIMAL_DATA.matchingGameWithThisSchema(firstExample), firstExample);
        assertEquals(GameSchema.MINIMAL_DATA.matchingGameWithThisSchema(firstExample).getClass(),
                GameSchema.MINIMAL_DATA.gameClass);
        assertThrows(IllegalArgumentException.class, () ->
                SchemaForInvalidConstructor.matchingGameWithThisSchema(firstExample),
                "Class in this object doesn't have constructor match to following signature public <? extends MinimalGame>(MinimalGame)"
        );
    }

}
