# Ludo-Serveur

## Note en cas de Fork/Reprise

La ci est prévue pour déployer à la fois sur le serveur de l'iut de nantes et mon serveur personnel.
En ce sens on a à peu près 3 000 variables de CI que je document ci après.
Dans la ci sauf si vous souhaitez déployer sur un tomcat perso je vous déconseille fortement de conserver la section de déployement `mathius` (Dans la ci et dans le pom dcp).
D'ailleurs les runners gitlab de l'univ n'ont pas accès à central vous devez donc [pluger une ci externe](https://gitlab.com/projects/new#cicd_for_external_repo) ou [mettre en place un miroir](https://gitlab.com/projects/new#import_project) (Pour le dernier choix faut juste penser à cocher la case `Mirror repository`).
Petit détail, les liens sont sur `gitlab.com` mais si vous avez une instance perso vous pouvez aussi.

### Variables CI

#### Deploiement

##### IUT

Pour gérer le déployement automatique sur le serveur de l'iut

* `IUTTomcat_PASS` : Mdp pour vous connecter au tomcat de l'iut
* `IUTTomcat_USER` : User pour vous connecter au tomcat de l'iut

##### VPN

Vous devrez gérer la connection au vpn de l'univ sinon votre runner gitlab n'aura pas accès au serveur de l'iut pour déployer

* `IUT_VPN` : Url du vpn de l'univ dans notre cas : `nomade.etu.univ-nantes.fr`
* `IUT_VPN_ROLE` : Role utilisé pour vous connecter au VPN dans notre cas : `IUT de Nantes`
* `IUT_VPN_USER` : Votre nom d'utilisateur pour vous connecter aux services de l'univ (ex : `E**C***A`)
* `IUT_VPN_MDP` : Votre mdp pour vous connecter aux services de l'univ

##### Perso

Pour gérer le déployement automatique sur un tomcat perso (Si vous l'utilisez je vous conseille de revoir le pom et la ci)

* `MathiusTomcat_PASS` : Mdp pour vous connecter à votre tomcat
* `MathiusTomcat_USER` : User pour vous connecter à votre tomcat

Si vous n'envisager pas de déployer sur un serveur personnel je vous conseille de supprimer les tâches *:mathius dans la ci

#### OAuth

Pour la plupart des OAuth il vous suffira de renseignez des variables sous la forme suivante :

* `providerAppId` : L'id de l'application OAuth sur le service en question
* `providerSecret` : Le secret de l'application OAuth sur le service en question

Cela est donc valable pour :

* [Discord](https://discord.com) (Section pour mettre en place l'OAuth : <https://discord.com/developers/applications>)
* [GitLab](https://gitlab.com) (Section pour mettre en place l'OAuth : <https://gitlab.com/-/profile/applications>)
* [Google](https://google.com) (Section pour mettre en place l'OAuth : <https://console.cloud.google.com/projectselector2/apis/credentials/consent?supportedpurview=project>)
* [Microsoft](https://microsoft.com) (Section pour mettre en place l'OAuth : <https://portal.azure.com/#blade/Microsoft_AAD_RegisteredApps/ApplicationsListBlade>)
* [GitLab Nantes](https://gitlab.univ-nantes.fr) (Section pour mettre en place l'OAuth : <https://gitlab.univ-nantes.fr/-/profile/applications>)
* [GitLab Lille](https://gitlab.univ-lille.fr/) (Section pour mettre en place l'OAuth : <https://gitlab.univ-lille.fr/profile/applications>)

##### Cas spécifiques

Dans le cadre de Zoom et Github, leur oauth est relou et doit être setup pour chaque nom de domaine/ip voir pour chaque path donc en ce sens voici les variables à mettre pour chacun)

###### Github

Voici le portail pour github d'ailleurs :  <https://github.com/settings/applications/new>

* `githubIUTDevId`
* `githubIUTDevSecret`
* `githubIUTReleaseId`
* `githubIUTReleaseSecret`
* `githubMathiusDevId`
* `githubMathiusDevSecret`
* `githubMathiusReleaseId`
* `githubMathiusReleaseSecret`

###### Zoom

Note supplémentaire pour Zoom, Zoom n'accepte que des noms de domaines pas d'ip donc fromage pour l'iut
(Voici le portail pour zoom d'ailleurs : <https://marketplace.zoom.us/develop/create>)

* `zoomMathiusDevId`
* `zoomMathiusDevSecret`
* `zoomMathiusReleaseId`
* `zoomMathiusReleaseSecret`

## Infos diverses

* Pour obtenir le war il vous suffit de faire un `make package` ce qui vous créera un jar dans le répertoire `/target` sous le nom de `serveur.war`.
* Pour lancer le projet en local il vous suffit de faire un `make` et cela vous lancera le projet sur votre [localhost](http://localhost:8080) sur le port 8080.
* Note : désormais ce qui suit n'est plus nécessaire pour les deux serveurs, la CI déployant automatiquement sur les deux serveurs !
* Pour mettre en prod votre version actuelle du serveur il vous suffit de faire un `make deployMathius` ou `make deployIUT` (en fonction du serveur cible) à condition d'avoir les logs du serveur cible dans votre fichier `~/.m2/settings.xml`. Voici donc un fichier d'exemple avec des logs bidons :

```xml
<settings>
    <servers>
        <!--Pour celle de l'IUT-->
        <server>
            <id>IUTNantesTomCat</id>
            <username>Truc</username>
            <password>bidule</password>
        </server>
        <!--Pour ma configuration-->
        <server>
            <id>MathiusTomCat</id>
            <username>Truc</username>
            <password>bidule</password>
        </server>
    </servers>
</settings>
````
