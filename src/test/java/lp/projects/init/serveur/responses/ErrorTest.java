package lp.projects.init.serveur.responses;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

/**
 * Class for test HTTP Error
 */
public class ErrorTest {

    /**
     * For test Instantiation of Error
     */
    @Test
    public void testErrorInstantiation() {
        Error baseError = new Error(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value());
        Error httpError = new Error(HttpStatus.BAD_REQUEST);
        assertEquals(baseError, httpError);
        baseError = new Error(HttpStatus.BAD_REQUEST.getReasonPhrase(), HttpStatus.BAD_REQUEST.value(),
            "truc");
        assertNotEquals(baseError, httpError);
        httpError = new Error(HttpStatus.BAD_REQUEST, "truc");
        assertEquals(baseError, httpError);
    }
}
