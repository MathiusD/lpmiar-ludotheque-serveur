package lp.projects.init.serveur.core.loan;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Class represent Base information of State of Loan
 */
@JsonIgnoreProperties({"permittedStateChange", "accessKey"})
@Schema(description = "Base Information of State of Loan")
public class MinimalState extends StateName {

    protected List<MinimalState> permittedStateChange;
    @Schema(description = "Key for get this State (Request Purpose only)", example = "at_home")
    protected String accessKey;

    /**
     * Shit Constructor for MinimalState Stored in DB
     * Used only by reflexion !
     */
    protected MinimalState() {
        super();
    }

    /**
     * Basic Constructor for MinimalState
     * @param name String represent name of State
     */
    @JsonCreator
    public MinimalState(@NonNull String name) {
        super(name);
        MinimalState foundState = null;
        for (MinimalState stat: State.ALL_STATES)
            if (stat.name.equalsIgnoreCase(name) && foundState == null)
                foundState = stat;
        if (foundState == null)
            throw new IllegalArgumentException("This MinimalState don't match with any existing State");
        this.permittedStateChange = foundState.permittedStateChange;
        this.accessKey = foundState.accessKey;
    }

    /**
     * Basic Constructor for MinimalState
     * @param accessKey String represent name of State
     */
    public static MinimalState fetchMinimalState(@NonNull String accessKey) {
        for (MinimalState stat: State.ALL_STATES)
            if (stat.getAccessKey().equalsIgnoreCase(accessKey))
                return stat;
        throw new IllegalArgumentException("This MinimalState don't match with any existing State");
    }

    /**
     * Basic Constructor for MinimalState
     * @param name String represent name of MinimalState
     * @param states many States represents states where changes are allowed
     */
    public MinimalState(@NonNull String name, @NonNull MinimalState... states) {
        this(name, Arrays.asList(states));
    }

    /**
     * Basic Constructor for MinimalState
     * @param name String represent name of State
     * @param states Collection of State or Child represents states where changes are allowed
     */
    public MinimalState(@NonNull String name, @NonNull Collection<? extends MinimalState> states) {
        this.name = name;
        this.accessKey = name.toLowerCase().trim().replaceAll(" ", "_");
        List<MinimalState> temp = new ArrayList<>();
        for (MinimalState state: states)
            if (state != null)
                temp.add(state.getMinimalState());
        this.permittedStateChange = temp;
    }

    /**
     * Constructor of MinimalState by MinimalState or child
     * @param otherState MinimalState used for extract informations
     */
    public MinimalState(@NonNull MinimalState otherState) {
        this(otherState.name, otherState.permittedStateChange);
    }

    /**
     * Method for know if change this State to new State is allowed
     * @param newState State represent target of change
     * @return arePermitted
     */
    public boolean changeArePermitted(@Nullable MinimalState newState) {
        if (newState == null) return false;
        if (newState.getClass() != MinimalState.class)
            return this.permittedStateChange.contains(new MinimalState(newState));
        else
            return this.permittedStateChange.contains(newState);
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof MinimalState)) return false;
        MinimalState state = (MinimalState) o;
        return state.hashCode() == this.hashCode();
    }

    /**
     * Method for get HashCode of this State
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(accessKey).append(permittedStateChange).toHashCode();
    }

    /**
     * Method for generate MinimalState related to this State
     * @return minimalState
     */
    @JsonIgnore
    @NonNull
    public MinimalState getMinimalState() {
        return new MinimalState(this);
    }

    /**
     * Getter for permittedStateChange
     * @return permittedStateChange
     */
    @NonNull
    public List<MinimalState> getPermittedStateChange() {
        return new ArrayList<>(permittedStateChange);
    }

    /**
     * Getter for get accessKey
     * @return accessKey
     */
    @NonNull
    public String getAccessKey() {
        return accessKey;
    }
}
