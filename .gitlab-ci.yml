image: maven:latest

stages:
  - build
  - test
  - package
  - deploy:develop
  - deploy:release

variables:
  BUILD_TARGET_DEVELOP_IUT: "target/develop/${CI_PIPELINE_ID}/iut"
  BUILD_TARGET_DEVELOP_MATHIUS: "target/develop/${CI_PIPELINE_ID}/mathius"
  BUILD_TARGET_RELEASE_IUT: "target/release/${CI_PIPELINE_ID}/iut"
  BUILD_TARGET_RELEASE_MATHIUS: "target/release/${CI_PIPELINE_ID}/mathius"
  BASE_MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
  MAVEN_OPTS_DEVELOP_IUT: "${BASE_MAVEN_OPTS} -DbuildDirectory=${BUILD_TARGET_DEVELOP_IUT}"
  MAVEN_OPTS_DEVELOP_MATHIUS: "${BASE_MAVEN_OPTS} -DbuildDirectory=${BUILD_TARGET_DEVELOP_MATHIUS}"
  MAVEN_OPTS_RELEASE_IUT: "${BASE_MAVEN_OPTS} -DbuildDirectory=${BUILD_TARGET_RELEASE_IUT}"
  MAVEN_OPTS_RELEASE_MATHIUS: "${BASE_MAVEN_OPTS} -DbuildDirectory=${BUILD_TARGET_RELEASE_MATHIUS}"

cache:
  paths:
    - .m2/repository/
    - target/

resolve:
  stage: .pre
  script:
    - mvn $BASE_MAVEN_OPTS clean dependency:resolve-plugins dependency:resolve

compile:
  stage: build
  script:
    - mvn $BASE_MAVEN_OPTS compile
  needs: ["resolve"]

test:
  stage: test
  script:
    - sh oauthKeys.sh true ${githubMathiusReleaseId} ${githubMathiusReleaseSecret} src/main/resources/oauthKeys.properties
    - mvn $BASE_MAVEN_OPTS compile test
  needs: ["compile"]
  artifacts:
    when: always
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml
        - target/failsafe-reports/TEST-*.xml
  parallel: 50

.package_template: &package
  stage: package
  needs: ["test"]

.package_develop_template: &package_develop
  <<: *package
  only:
    refs:
      - CI
      - CI-rework
      - master

package:develop:iut:
  <<: *package_develop
  script:
    - sed -e '$s/.\//~\//' src/main/resources/catalina.properties >> .temp.properties
    - rm -rf src/main/resources/catalina.properties
    - cat .temp.properties >> src/main/resources/catalina.properties
    - sh oauthKeys.sh false ${githubIUTDevId} ${githubIUTDevSecret} src/main/resources/oauthKeys.properties
    - mvn $MAVEN_OPTS_DEVELOP_IUT package
  artifacts:
    name: "$CI_JOB_NAME"
    expire_in: 24 hours
    paths:
      - ${BUILD_TARGET_DEVELOP_IUT}/serveur.war

package:develop:mathius:
  <<: *package_develop
  script:
    - sh oauthKeys.sh true ${githubMathiusDevId} ${githubMathiusDevSecret} src/main/resources/oauthKeys.properties ${zoomMathiusDevId} ${zoomMathiusDevSecret}
    - mvn $MAVEN_OPTS_DEVELOP_MATHIUS package
  artifacts:
    name: "$CI_JOB_NAME"
    expire_in: 24 hours
    paths:
      - ${BUILD_TARGET_DEVELOP_MATHIUS}/serveur.war

.package_release_template: &package_release
  <<: *package
  before_script:
    - sed -e '$s/testing/prod/' src/main/resources/catalina.properties >> .temp.properties
    - rm -rf src/main/resources/catalina.properties
    - cat .temp.properties >> src/main/resources/catalina.properties
  only:
    refs:
      - master

package:release:iut:
  <<: *package_release
  script:
    - sed -e '$s/.\//~\//' src/main/resources/catalina.properties >> .temp.properties
    - rm -rf src/main/resources/catalina.properties
    - cat .temp.properties >> src/main/resources/catalina.properties
    - sh oauthKeys.sh false ${githubIUTReleaseId} ${githubIUTReleaseSecret} src/main/resources/oauthKeys.properties
    - mvn $MAVEN_OPTS_RELEASE_IUT package
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - ${BUILD_TARGET_RELEASE_IUT}/serveur.war

package:release:mathius:
  <<: *package_release
  script:
    - sh oauthKeys.sh true ${githubMathiusReleaseId} ${githubMathiusReleaseSecret} src/main/resources/oauthKeys.properties ${zoomMathiusReleaseId} ${zoomMathiusReleaseSecret}
    - mvn $MAVEN_OPTS_RELEASE_MATHIUS package
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - ${BUILD_TARGET_RELEASE_MATHIUS}/serveur.war

.deploy_template : &deploy
  before_script:
    - sh createSettings.sh ${MathiusTomcat_USER} ${MathiusTomcat_PASS} ${IUTTomcat_USER} ${IUTTomcat_PASS}
  retry: 2

.deploy_develop_template: &deploy_develop
  <<: *deploy
  stage: deploy:develop
  only:
    refs:
      - CI
      - CI-rework
      - master

.deploy_release_template : &deploy_release
  <<: *deploy
  stage: deploy:release
  only:
    refs:
      - master

deploy:develop:mathius:
  <<: *deploy_develop
  script:
    - mvn $MAVEN_OPTS_DEVELOP_MATHIUS tomcat7:redeploy-only
  needs: [ "package:develop:mathius" ]

deploy:develop:iut:
  <<: *deploy_develop
  image: ubuntu:latest
  script:
    - sh prepareDeployOnIUT.sh
    - mvn $MAVEN_OPTS_DEVELOP_IUT tomcat7:redeploy-only -Dtarget=iut-develop
  needs: [ "package:develop:iut" ]

deploy:release:mathius:
  <<: *deploy_release
  script:
    - mvn $MAVEN_OPTS_RELEASE_MATHIUS tomcat7:redeploy-only -Dtarget=mathius-production
  needs: [ "package:release:mathius" ]

deploy:release:iut:
  <<: *deploy_release
  image: ubuntu:latest
  script:
    - sh prepareDeployOnIUT.sh
    - mvn $MAVEN_OPTS_RELEASE_IUT tomcat7:redeploy-only -Dtarget=iut-production
  needs: ["package:release:iut", "deploy:develop:iut"]

.clean_template: &clean
  stage: .post

.clean_develop_template : &clean_develop
  <<: *clean
  only:
    refs:
      - CI
      - CI-rework
      - master

clean:develop:mathius:
  <<: *clean_develop
  script:
    - mvn $MAVEN_OPTS_DEVELOP_MATHIUS clean
  needs: [ "deploy:develop:mathius" ]

clean:develop:iut:
  <<: *clean_develop
  script:
    - mvn $MAVEN_OPTS_DEVELOP_IUT clean
  needs: [ "deploy:develop:iut" ]

.clean_release_template : &clean_release
  <<: *clean
  only:
    refs:
      - master

clean:release:mathius:
  <<: *clean_release
  script:
    - mvn $MAVEN_OPTS_RELEASE_MATHIUS clean
  needs: [ "deploy:release:mathius" ]

clean:release:iut:
  <<: *clean_release
  script:
    - mvn $MAVEN_OPTS_RELEASE_IUT clean
  needs: [ "deploy:release:iut" ]
