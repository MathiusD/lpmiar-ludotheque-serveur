package lp.projects.init.serveur.core.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test  YearRecommended
 */
public class YearRecommendedTest {

    /**
     * For test Instantiation of  YearRecommended
     */
    @Test
    public void testYearRecommendedInstantiation() {
        YearRecommended baseYearRecommended = new  YearRecommended(1, 2);
        YearRecommended targetYearRecommended = new  YearRecommended(1, 2, false);
        assertEquals(baseYearRecommended, targetYearRecommended);
        targetYearRecommended = new  YearRecommended(1, 2, true);
        assertNotEquals(baseYearRecommended, targetYearRecommended);
        baseYearRecommended = new  YearRecommended(1);
        targetYearRecommended = new  YearRecommended(1, null, true);
        assertEquals(baseYearRecommended, targetYearRecommended);
        targetYearRecommended = new  YearRecommended(1, true);
        assertEquals(baseYearRecommended, targetYearRecommended);
    }

    /**
     * For test Exception are throw if arguments are incorrect
     */
    @Test
    public void testYearRecommendedFailedInstantiation() {
        assertThrows(IllegalArgumentException.class, () -> new  YearRecommended(2, 1),
                "max must be superior or equals to min");
    }
}
