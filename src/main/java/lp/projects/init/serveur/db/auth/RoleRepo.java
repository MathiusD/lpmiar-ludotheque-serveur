package lp.projects.init.serveur.db.auth;

import lp.projects.init.serveur.core.auth.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface represent Repo for Role
 */
public interface RoleRepo extends JpaRepository<Role, String> {

}
