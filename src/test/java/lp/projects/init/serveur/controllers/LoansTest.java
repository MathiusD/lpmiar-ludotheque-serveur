package lp.projects.init.serveur.controllers;

import lp.projects.init.serveur.BaseAPITest;
import lp.projects.init.serveur.core.game.GameState;
import lp.projects.init.serveur.core.loan.Loan;
import lp.projects.init.serveur.core.loan.LoanUpdate;
import lp.projects.init.serveur.core.loan.LoanUpdateResult;
import lp.projects.init.serveur.core.loan.State;
import lp.projects.init.serveur.responses.*;
import org.junit.jupiter.api.Test;
import org.springframework.http.*;
import org.springframework.lang.NonNull;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LoansTest extends BaseAPITest {

    /**
     * Method for get base uri of Loans testing
     * @return baseUri for springTests
     */
    @NonNull
    @Override
    public String baseUri() {
        return String.format("%s/loans", super.baseUri());
    }

    /**
     * Test for route /loans/{id}
     */
    @Test
    public void testLoan() {
        assertEquals(new InvalidTokenException(), customExchange("/truc", InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", "truc");
        assertEquals(customExchange("/truc", headers,  InvalidTokenException.class),
                new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(libraryLoans.get(0),
                customExchange(String.format("/%s", libraryLoans.get(0).getId()), headers, Loan.class));
        assertEquals(new NotFoundException("Loan with the given id (truc) was not found"),
                customExchange("/truc", headers, NotFoundException.class));
        headers = new HttpHeaders();
        headers.add("token", otherToken.getToken());
        assertEquals(customExchange(String.format("/%s", myLoans.get(0).getId()), headers,
                AccessDeniedException.class), new AccessDeniedException(meToo,
                String.format("/loans/%s", myLoans.get(0).getId())));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(customExchange(String.format("/%s", meTooLoans.get(0).getId()), headers,
                Loan.class), meTooLoans.get(0));
    }

    /**
     * Test for route /loans/state
     */
    @Test
    public void testLoanState () {
        assertEquals(State.ALL_STATES, Arrays.asList(customExchange("/states", State[].class)));
    }

    /**
     * Test for route /loans/{id} by POST
     */
    @Test
    public void testUpdateLoan() {
        HttpHeaders headers = new HttpHeaders();
        LoanUpdate update = new LoanUpdate("truc");
        HttpEntity<LoanUpdate> entity = new HttpEntity<>(update, headers);
        assertEquals(new InvalidTokenException(), customExchange("/truc", HttpMethod.POST,
                entity, InvalidTokenException.class));
        headers.add("token", "truc");
        entity = new HttpEntity<>(update, headers);
        assertEquals(customExchange("/truc", HttpMethod.POST, entity, InvalidTokenException.class),
                new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        Loan temp = libraryLoans.get(0);
        update = new LoanUpdate("truc");
        entity = new HttpEntity<>(update, headers);
        assertEquals(new BadRequestException("This MinimalState don't match with any existing State"),
                customExchange(String.format("/%s", temp.getId()), HttpMethod.POST, entity,
                        BadRequestException.class));
        update = new LoanUpdate("waiting");
        entity = new HttpEntity<>(update, headers);
        assertEquals(new LoanUpdateResult(new Loan(temp.getId(), temp.getGame(), temp.getUser(), temp.getStartedAt(),
            temp.getEndAt(), State.WAITING, temp.getGameDataIsMinified()), String.format(
                "Loan is update for state : %s", update.state)), customExchange(String.format(
                    "/%s", temp.getId()), HttpMethod.POST, entity, LoanUpdateResult.class));
        assertEquals(new BadRequestException(
            String.format("Change not allowed ! (Initial State : %s, Target State %s",
                State.WAITING.getAccessKey(), State.WAITING.getAccessKey())),
                customExchange(String.format("/%s", temp.getId()), HttpMethod.POST, entity,
                        BadRequestException.class));
    }

    /**
     * Test for route /loans/{id} by DELETE
     */
    @Test
    public void testDeleteLoan() {
        HttpHeaders headers = new HttpHeaders();
        assertEquals(new InvalidTokenException(), customExchange("/truc", HttpMethod.DELETE,
                headers, InvalidTokenException.class));
        headers.add("token", "truc");
        assertEquals(customExchange("/truc", HttpMethod.DELETE, headers, InvalidTokenException.class),
            new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", otherToken.getToken());
        Loan temp = meTooLoans.get(0);
        customExchange(String.format("/%s", temp.getId()), HttpMethod.DELETE, headers, HttpStatus.NO_CONTENT);
        assertEquals(GameState.AVAILABLE, gameRepository.getOne(temp.getGame().getId()).getGameState());
        temp = meTooLoans.get(1);
        assertEquals(customExchange(String.format("/%s", temp.getId()), HttpMethod.DELETE, headers,
                AccessDeniedException.class),
                new AccessDeniedException(meToo, String.format("/loans/%s", temp.getId())));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        customExchange(String.format("/%s", temp.getId()), HttpMethod.DELETE, headers, HttpStatus.NO_CONTENT);
        assertEquals(GameState.UNAVAILABLE, gameRepository.getOne(temp.getGame().getId()).getGameState());
    }
}
