package lp.projects.init.serveur.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class represent Unauthorized Exception
 */
@CrossOrigin
@ResponseBody
@ResponseStatus(HttpStatus.UNAUTHORIZED)
@Schema(description = "Unauthorized Error")
public class UnauthorizedException extends Error{

    @Hidden
    @NonNull
    public static HttpStatus getBaseHttpStatus() {
        return HttpStatus.UNAUTHORIZED;
    }

    /**
     * Basic Constructor for UnauthorizedException
     * @param raisonProvided String represent specific message for this error
     */
    @JsonCreator
    public UnauthorizedException(@Nullable String raisonProvided) {
        super(getBaseHttpStatus(), raisonProvided);
    }

    /**
     * Basic Constructor for UnauthorizedException
     */
    public UnauthorizedException() {
        this(null);
    }
}