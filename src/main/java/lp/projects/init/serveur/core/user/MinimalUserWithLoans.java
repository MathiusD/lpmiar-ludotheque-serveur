package lp.projects.init.serveur.core.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.auth.MinimalRole;
import lp.projects.init.serveur.core.loan.Loan;
import lp.projects.init.serveur.core.loan.MinimalLoan;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Class represent Base informations of this User and his Loans of this library
 */
@Schema(description = "Base Information of User with this Loans of this Library")
public class MinimalUserWithLoans extends MinimalUser {

    @ArraySchema(uniqueItems = true, schema = @Schema(description = "Loans of this User"))
    protected final List<MinimalLoan> loans;

    /**
     * Basic Constructor for MinimalUserWithLoans
     * @param id String represent id of User
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     * @param role MinimalRole represent role of User
     * @param loans Collection of Loans represent Loans of this User
     */
    @JsonCreator
    public MinimalUserWithLoans(@NonNull String id, @Nullable String lastName, @Nullable String firstName,
                                @NonNull String email, @NonNull String nickName,
                                @NonNull MinimalRole role, @NonNull Collection<? extends MinimalLoan> loans) {
        super(id, lastName, firstName, email, nickName, role);
        List<MinimalLoan> tempLoans = new ArrayList<>();
        for (MinimalLoan loan : loans)
            if(loan != null && !tempLoans.contains(loan))
                    if (loan instanceof Loan) {
                    Loan gLoan = (Loan) loan;
                    if (gLoan.getUser().equals(this))
                        tempLoans.add(loan.getMinimalLoan());
                } else
                    tempLoans.add(loan.getMinimalLoan());
        this.loans = tempLoans;
    }

    /**
     * Basic Constructor for MinimalUserWithLoans
     * @param id String represent id of User
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     * @param loans Collection of Loans represent Loans of this User
     */
    public MinimalUserWithLoans(@NonNull String id, @Nullable String lastName, @Nullable String firstName,
                                @NonNull String email, @NonNull String nickName,
                                @NonNull Collection<? extends MinimalLoan> loans) {
        this(id, lastName, firstName, email, nickName, DEFAULT_ROLE, loans);
    }

    /**
     * Basic Constructor for MinimalUserWithLoans
     * @param id String represent id of User
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     * @param role MinimalRole represent role of User
     * @param loans Many Loans represent Loans of this User
     */
    public MinimalUserWithLoans(@NonNull String id, @Nullable String lastName, @Nullable String firstName,
                                @NonNull String email, @NonNull String nickName,
                                @NonNull MinimalRole role, @NonNull MinimalLoan... loans) {
        this(id, lastName, firstName, email, nickName, role, Arrays.asList(loans));
    }

    /**
     * Basic Constructor for MinimalUserWithLoans
     * @param id String represent id of User
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     * @param loans Many Loans represent Loans of this User
     */
    public MinimalUserWithLoans(@NonNull String id, @Nullable String lastName, @Nullable String firstName,
                                @NonNull String email, @NonNull String nickName,
                                @NonNull MinimalLoan... loans) {
        this(id, lastName, firstName, email, nickName, Arrays.asList(loans));
    }

    /**
     * Basic Constructor for MinimalUserWithLoans
     * @param id String represent id of User
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     */
    public MinimalUserWithLoans(@NonNull String id, @Nullable String lastName, @Nullable String firstName,
                                @NonNull String email, @NonNull String nickName) {
        this(id, lastName, firstName, email, nickName, DEFAULT_ROLE, new ArrayList<>());
    }

    /**
     * Constructor of MinimalUser by MinimalUserWithLoans or child
     * @param baseUser MinimalUser used for extract informations
     * @param loans Collection of Loans represent Loans of this User
     */
    public MinimalUserWithLoans(@NonNull MinimalUser baseUser, @NonNull Collection<? extends MinimalLoan> loans) {
        this(baseUser.getId(), baseUser.getLastName(), baseUser.getFirstName(),
                baseUser.getEmail(), baseUser.getNickName(), baseUser.getRole(), loans);
    }

    /**
     * Constructor of MinimalUser by MinimalUserWithLoans or child
     * @param baseUser MinimalUser used for extract informations
     * @param loans Many Loans represent Loans of this User
     */
    public MinimalUserWithLoans(@NonNull MinimalUser baseUser, @NonNull MinimalLoan... loans) {
        this(baseUser.getId(), baseUser.getLastName(), baseUser.getFirstName(),
                baseUser.getEmail(), baseUser.getNickName(), baseUser.getRole(), loans);
    }

    /**
     * Constructor of MinimalUserWithLoans by MinimalUserWithLoans or child
     * @param baseUser MinimalUserWithLoans used for extract informations
     */
    public MinimalUserWithLoans(@NonNull MinimalUserWithLoans baseUser) {
        super(baseUser.getId(), baseUser.getLastName(), baseUser.getFirstName(),
                baseUser.getEmail(), baseUser.getNickName(), baseUser.getRole());
        this.loans = baseUser.loans;
    }

    /**
     * Constructor of MinimalUserWithLoans by MinimalUser or child
     * @param baseUser MinimalUser used for extract informations
     */
    public MinimalUserWithLoans(@NonNull MinimalUser baseUser) {
        this(baseUser.getId(), baseUser.getLastName(), baseUser.getFirstName(),
                baseUser.getEmail(), baseUser.getNickName(), baseUser.getRole());
    }

    /**
     * Constructor for MinimalUserWithLoans by Collection of Loans only
     * @param loans Collection of Loans represent Loans of this User
     */
    public MinimalUserWithLoans(@NonNull Collection<? extends Loan> loans) {
        this(MinimalUser.extractMinimalUserFrom(loans), loans);
    }

    /**
     * Constructor for MinimalUserWithLoans by Many Loans only
     * @param loans Many Loans represent Loans of this User
     */
    public MinimalUserWithLoans(@NonNull Loan... loans) {
        this(MinimalUser.extractMinimalUserFrom(loans), loans);
    }

    /**
     * Getter for loans
     * @return loans
     */
    public List<MinimalLoan> getLoans() {
        return new ArrayList<>(loans);
    }
}
