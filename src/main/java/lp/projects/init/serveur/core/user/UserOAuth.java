package lp.projects.init.serveur.core.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.oauth.OAuthProvider;
import lp.projects.init.serveur.db.BaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.UUID;

/**
 * Class represent Link between User and OAuthProvider
 */
@Entity
@Schema(description = "Represent OAuth related to User")
public class UserOAuth extends BaseEntity {

    @Id
    @Column(name = "user_oauth_id")
    protected String id;
    @ManyToOne(optional = false, targetEntity = MinimalUser.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_oauth_userRelated", nullable = false)
    protected MinimalUser userRelated;
    @Column(name = "user_oauth_provider_name")
    @Schema(description = "Name of provider related", example = "gitlab")
    protected String providerNameRelated;
    @Column(name = "user_oauth_provider_userId")
    @Schema(description = "Id of this user for this provider")
    protected String userIdOfProvider;

    /**
     * Shit Constructor for UserOAuth Stored in DB
     * Used only by reflexion !
     */
    protected UserOAuth() {
        super();
    }

    /**
     * Basic Constructor of UserOAuth
     * @param id String represent id of this object
     * @param userRelated MinimalUser represent userRelated
     * @param providerNameRelated String represent name of Provider
     * @param userIdOfProvider String represent id of user in this provider
     */
    @JsonCreator
    @PersistenceConstructor
    public UserOAuth(@NonNull String id, @NonNull MinimalUser userRelated,
                     @NonNull String providerNameRelated, @NonNull String userIdOfProvider) {
        super();
        this.id = id;
        this.userRelated = userRelated;
        this.providerNameRelated = providerNameRelated;
        this.userIdOfProvider = userIdOfProvider;
    }

    /**
     * Basic Constructor of UserOAuth
     * @param userRelated MinimalUser represent userRelated
     * @param providerNameRelated String represent name of Provider
     * @param userIdOfProvider String represent id of user in this provider
     */
    public UserOAuth(@NonNull MinimalUser userRelated, @NonNull String providerNameRelated,
                     @NonNull String userIdOfProvider) {
        this(UUID.randomUUID().toString(), userRelated, providerNameRelated, userIdOfProvider);
    }

    /**
     * Basic Constructor of UserOAuth
     * @param id String represent id of this object
     * @param userRelated MinimalUser represent userRelated
     * @param providerRelated OAuthProvider represent provider
     * @param userIdOfProvider String represent id of user in this provider
     */
    public UserOAuth(@NonNull String id, @NonNull MinimalUser userRelated,
                     @NonNull OAuthProvider providerRelated, @NonNull String userIdOfProvider) {
        this(id, userRelated, providerRelated.nameProvider, userIdOfProvider);
    }

    /**
     * Basic Constructor of UserOAuth
     * @param userRelated MinimalUser represent userRelated
     * @param providerRelated OAuthProvider represent provider
     * @param userIdOfProvider String represent id of user in this provider
     */
    public UserOAuth(@NonNull MinimalUser userRelated, @NonNull OAuthProvider providerRelated,
                     @NonNull String userIdOfProvider) {
        this(UUID.randomUUID().toString(), userRelated, providerRelated, userIdOfProvider);
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserOAuth)) return false;
        UserOAuth userOAuth = (UserOAuth) o;
        return userOAuth.hashCode() == hashCode();
    }

    /**
     * Method for get HashCode of this UserOAuth
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).toHashCode();
    }

    /**
     * Method for Write UserOAuth
     * @return userOAuthReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("UserOAuth for provider %s with providerId : %s", providerNameRelated, userIdOfProvider);
    }

    /**
     * Getter for get id
     * @return id
     */
    @NonNull
    @JsonIgnore
    public String getId() {
        return id;
    }

    /**
     * Getter for get userRelated
     * @return userRelated
     */
    @NonNull
    @JsonIgnore
    public MinimalUser getUserRelated() {
        return userRelated;
    }

    /**
     * Setter for get userRelated
     */
    @NonNull
    public void setUserRelated(MinimalUser user) {
        userRelated = user;
    }

    /**
     * Getter for get providerNameRelated
     * @return providerNameRelated
     */
    @NonNull
    public String getProviderNameRelated() {
        return providerNameRelated;
    }

    /**
     * Getter for get userIdOfProvider
     * @return userIdOfProvider
     */
    @NonNull
    public String getUserIdOfProvider() {
        return userIdOfProvider;
    }
}
