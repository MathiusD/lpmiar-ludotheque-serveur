package lp.projects.init.serveur.core.game;

import org.junit.jupiter.api.Test;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test ShortGame
 */
public class ShortGameTest extends MinimalGameTest {

    /**
     * Assert ShortGame are exactly same attributes value
     * @param first ShortGame represent first game
     * @param second ShortGame represent second game
     */
    public static void assertGamesAreStrictlyEquals(@NonNull ShortGame first, @NonNull ShortGame second) {
        assertGamesAreStrictlyEquals(first, second, null);
    }

    /**
     * Assert ShortGame are exactly same attributes value
     * @param first ShortGame represent first game
     * @param second ShortGame represent second game
     * @param message String represent message send if assertion are false
     */
    public static void assertGamesAreStrictlyEquals(@NonNull ShortGame first, @NonNull ShortGame second, @Nullable String message) {
        assertGamesAreStrictlyEquals(first.getMinimalGame(), second.getMinimalGame(), message);
        assertEquals(first.nbPlayer, second.nbPlayer, message);
        assertEquals(first.yearRecommended, second.yearRecommended, message);
        assertEquals(first.time, second.time, message);
        assertEquals(first.type, second.type, message);
    }

    /**
     * For test Instantiation of MinimalGame
     */
    @Test
    public void testMinimalGameInstantiation() {
        ShortGame firstMinGame = new ShortGame("ABC", "Example", 3, 4, (YearRecommended) null,
            null, null);
        ShortGame tempMinGame = new ShortGame("ABC", "Example", new NbPlayer(3, 4), null,
                null, null);
        assertGamesAreStrictlyEquals(firstMinGame, tempMinGame);
        firstMinGame = new ShortGame("ABC", "Example", 5, null,
                null, null);
        tempMinGame = new ShortGame("ABC", "Example", new NbPlayer(5), null,
                null, null);
        assertGamesAreStrictlyEquals(firstMinGame, tempMinGame);
        tempMinGame = new ShortGame(firstMinGame);
        assertGamesAreStrictlyEquals(firstMinGame, tempMinGame);
        firstMinGame = new ShortGame("ABC", "Example", GameState.BORROWED, null,
            null, null, null);
        tempMinGame = new ShortGame(firstMinGame);
        assertGamesAreStrictlyEquals(firstMinGame, tempMinGame);
    }
}
