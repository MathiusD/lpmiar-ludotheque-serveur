package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.db.BaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.Embeddable;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for represent State of Game
 */
@Embeddable
@Schema(description = "State of Game")
public class GameState extends BaseEntity {

    @Schema(description = "Name of this State", example = "Available")
    protected String name;
    @Schema(description = "More details of this State (Or why Game in this State)",
            example = "This Game are available for played")
    protected String note;
    @Schema(description = "Boolean represent if Game are available in summary", example = "true")
    protected Boolean isAvailable;

    /**
     * Shit Constructor for Game Stored in DB
     * Used only by reflexion !
     */
    protected GameState() {
        super();
    }

    /**
     * Basic Constructor for GameState
     * @param name String represent name of state
     * @param isAvailable Boolean represent if Game are available to Loan or play with it
     * @param note String represent more detail of this state (Or why)
     */
    @JsonCreator
    public GameState(@NonNull String name, @NonNull Boolean isAvailable, @Nullable String note) {
        this.name = name;
        if (note == null)
            note = String.format("This game is %s", name.toLowerCase());
        this.note = note;
        this.isAvailable = isAvailable;
    }

    /**
     * Basic Constructor for GameState
     * @param name String represent name of state
     * @param isAvailable Boolean represent if Game are available to Loan or play with it
     */
    public GameState(@NonNull String name, @NonNull Boolean isAvailable) {
        this(name, isAvailable, null);
    }

    /**
     * Method for fork current State for create a new based to attribute not redefined
     * @param name String represent name of state
     * @param note String represent more detail of this state (Or why)
     */
    public GameState fork(@NonNull String name, @Nullable String note) {
        return new GameState(name, isAvailable, note);
    }

    /**
     * Method for fork current State for create a new based to attribute not redefined
     * @param name String represent name of state
     * @param isAvailable Boolean represent if Game are available to Loan or play with it
     */
    public GameState fork(@NonNull String name, @NonNull Boolean isAvailable) {
        return new GameState(name, isAvailable, note);
    }

    /**
     * Method for fork current State for create a new based to attribute not redefined
     * @param isAvailable Boolean represent if Game are available to Loan or play with it
     * @param note String represent more detail of this state (Or why)
     */
    public GameState fork(@NonNull Boolean isAvailable, @Nullable String note) {
        return new GameState(name, isAvailable, note);
    }

    /**
     * Method for fork current State for create a new based to attribute not redefined
     * @param isAvailable Boolean represent if Game are available to Loan or play with it
     */
    public GameState fork(@NonNull Boolean isAvailable) {
        return fork(isAvailable, note);
    }

    /**
     * Method for fork current State for create a new based to attribute not redefined
     * @param note String represent more detail of this state (Or why)
     */
    public GameState fork(@NonNull String note) {
        return fork(name, note);
    }

    /**
     * Method for fork current State for create a new based to attribute not redefined
     * (In this case, we take only isAvailable in new State)
     * @param name String represent name of state
     */
    public GameState forkOnlyIsAvailable(@NonNull String name) {
        return new GameState(name, isAvailable);
    }

    public final static GameState AVAILABLE = new GameState("Available", true);
    public final static GameState UNAVAILABLE = new GameState("Unavailable", false);
    public final static GameState REQUESTED = UNAVAILABLE.forkOnlyIsAvailable("Requested");
    public final static GameState BORROWED = UNAVAILABLE.forkOnlyIsAvailable("Borrowed");
    public final static GameState BROKEN = UNAVAILABLE.forkOnlyIsAvailable("Broken");
    public final static List<GameState> ALL_STATES;
    static {
        List<GameState> temp = new ArrayList<>();
        temp.add(AVAILABLE);
        temp.add(UNAVAILABLE);
        temp.add(REQUESTED);
        temp.add(BORROWED);
        temp.add(BROKEN);
        ALL_STATES = temp;
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof GameState)) return false;
        GameState gameState = (GameState) o;
        return hashCode() == gameState.hashCode();
    }

    /**
     * Method for get HashCode of this GameState
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name).append(note).append(isAvailable).toHashCode();
    }

    /**
     * Getter for name of GameState
     * @return name
     */
    @NonNull
    public String getName() {
        return name;
    }

    /**
     * Getter for isAvailable of GameState
     * @return isAvailable
     */
    @NonNull
    public Boolean getIsAvailable() {
        return isAvailable;
    }

    /**
     * Getter for note of GameState
     * @return note
     */
    @NonNull
    public String getNote() {
        return note;
    }

    /**
     * Method for Write GameState
     * @return gameStateReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("GameState %s and game is%s available.", name, !isAvailable ? " not" : "");
    }
}
