package lp.projects.init.serveur;

import lp.projects.init.serveur.responses.NotFoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class for test App
 */
public class AppTest extends BaseAPITest{

    /**
     * Test for base route in API
     */
    @Test
    public void testBaseAPI() {
        assertEquals(customExchange(NotFoundException.class), new NotFoundException());
    }
}
