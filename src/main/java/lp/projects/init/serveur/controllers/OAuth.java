package lp.projects.init.serveur.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lp.projects.init.serveur.DBRepo;
import lp.projects.init.serveur.core.auth.Token;
import lp.projects.init.serveur.core.oauth.*;
import lp.projects.init.serveur.core.user.MinimalUser;
import lp.projects.init.serveur.core.user.User;
import lp.projects.init.serveur.core.user.UserOAuth;
import lp.projects.init.serveur.responses.*;
import lp.projects.init.serveur.responses.Error;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * Class represent Controller of OAuth
 */
@RestController
@Configuration
@PropertySource(value= {"classpath:oauthConfig.properties", "classpath:oauthKeys.properties"})
public class OAuth extends DBRepo implements EnvironmentAware {

    private Environment environment;

    @Override
    public void setEnvironment(@Autowired final Environment environment) {
        this.environment = environment;
    }

    private final RestTemplate restTemplate = new RestTemplate();

    /**
     * Basic Constructor of OAuthController
     */
    public OAuth() {
        restTemplate.getMessageConverters().add(new ObjectToUrlEncodedConverter<>());
        MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonHttpMessageConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.APPLICATION_JSON));
        restTemplate.getMessageConverters().add(jsonHttpMessageConverter);
    }

    private final Map<String, OAuthState> stateMap = new HashMap<>();
    private final Map<String, OAuthProvider> providerData = new HashMap<>();
    @Value("${oauth.providers}")
    private List<String> providerList;

    /**
     * Method for get baseUri for handler related to this provider
     * @param request Request send to this serveur
     * @param provider Provider related to this request
     * @return baseUri
     */
    @NonNull
    private String baseUri(@NonNull HttpServletRequest request, @NonNull OAuthProvider provider) {
        return String.format(
            "%s%s", request.getRequestURL().toString()
                .replace(provider.getOAuthPath(), "")
                .replace(provider.getLoginPath(), "")
                .replace(provider.getLinkPath(), ""),
            provider.getOAuthPath()
        );
    }

    /**
     * Method for get provider with given name
     * @param providerName String represent name of this provider
     * @return provider
     * @throws NotFoundException if Provider isn't supported
     */
    @NonNull
    private OAuthProvider getProvider(@NonNull String providerName) throws NotFoundException {
        if (!providerList.contains(providerName))
            throw new NotFoundException("Provider isn't supported");
        if (providerData.get(providerName) == null)
            providerData.put(providerName, new OAuthProvider(providerName, environment));
        return providerData.get(providerName);
    }

    /**
     * For get all providers supported
     * @return providers
     */
    @CrossOrigin
    @Operation(summary = "Get providers supported this library")
    @ApiResponse(responseCode = "200", description = "All providers",
        content = @Content(mediaType = "application/json",
            array = @ArraySchema(uniqueItems = true, schema = @Schema(implementation = OAuthProvider.class)
            )
        )
    )
    @RequestMapping(value = "/oauth/providers", method = RequestMethod.GET)
    @NonNull
    public List<OAuthProvider> getProviders() {
        List<OAuthProvider> providers = new ArrayList<>();
        for (String providerName : providerList) {
            try {
                providers.add(getProvider(providerName));
            } catch (NotFoundException ignored) {
            }
        }
        return providers;
    }

    /**
     * For build request for provider and redirect to them
     * @param provider Provider used for this request
     * @param request Request received
     * @param response Response send for connect provider
     * @param userRelated User related to this request
     * @param requestKey String represent requestKey used for this request
     * @param requestPass String represent pass used for this request
     * @return response
     * @throws NotFoundException NotFoundException are thrown if provider is unknown
     * @throws BadRequestException BadRequestException are thrown if requestKey is already used
     */
    public HttpServletResponse redirectResponse(@NonNull String provider,
                                                @NonNull HttpServletRequest request,
                                                @NonNull HttpServletResponse response,
                                                @Nullable MinimalUser userRelated,
                                                @Nullable String requestKey,
                                                @Nullable String requestPass) throws
                                                    NotFoundException, BadRequestException {
        try {
            OAuthProvider providerData = getProvider(provider);
            OAuthState oAuthState = new OAuthState(request.getServletPath(), userRelated,
                providerData, requestKey, requestPass);
            stateMap.put(oAuthState.key, oAuthState);
            String targetUri = String.format(
                "%s?client_id=%s&redirect_uri=%s&response_type=code&scope=%s&state=%s%s%s",
                providerData.getAuthorizeEndpoint(), providerData.getAppId(), baseUri(request, providerData),
                providerData.getScope(), oAuthState.key, providerData.getWithCodeVerification() ?
                    String.format("&code_verifier=%s%s", oAuthState.codeVerifier,
                        oAuthState.getHashOfCodeVerifier() != null ?
                            String.format("&code_challenge=%s&code_challenge_method=S256",
                                oAuthState.getHashOfCodeVerifier()) : ""
                    ) : "",
                providerData.getAuthOpts() != null ? String.format("&%s", providerData.getAuthOpts()) : ""
            );
            response.setHeader("Location", targetUri);
            response.setStatus(HttpStatus.FOUND.value());
            return response;
        } catch (IllegalArgumentException exception) {
            throw new BadRequestException(exception.getMessage());
        }
    }

    /**
     * For build login request for provider and redirect to them
     * @param provider Provider used for this request
     * @param request Request received
     * @param response Response send for connect provider
     * @param key String represent requestKey used for this request
     * @param pass String represent pass used for this request
     * @throws NotFoundException NotFoundException are thrown if provider is unknown
     * @throws BadRequestException BadRequestException are thrown if requestKey is already used
     */
    @ApiResponses({
        @ApiResponse(responseCode = "400", description = "requestKey is already used",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = BadRequestException.class)
            )
        ),
        @ApiResponse(responseCode = "404", description = "Provider is Unknown",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = NotFoundException.class)
            )
        )
    })
    @Operation(summary = "For login with specific provider")
    @RequestMapping(value = "/login/{provider}", method = RequestMethod.GET)
    public void login(@Parameter(description = "Provider used for this request",
                        example = "gitlab") @NonNull @PathVariable String provider,
                      @Parameter(description = "Random key for identify this request",
                          example = "ABCD") @Nullable @RequestParam String key,
                      @Parameter(description = "Pass for protect result if request is identify",
                          example = "ABCD") @Nullable @RequestHeader(required = false) String pass,
                      @NonNull HttpServletRequest request,
                      @NonNull HttpServletResponse response) throws NotFoundException,
                                                                    BadRequestException {
        redirectResponse(provider, request, response, null, key, pass);
    }

    /**
     * For build connect request for provider and redirect to them
     * @param provider Provider used for this request
     * @param request Request received
     * @param response Response send for connect provider
     * @param key String represent requestKey used for this request
     * @param pass String represent pass used for this request
     * @throws NotFoundException NotFoundException are thrown if provider is unknown
     * @throws BadRequestException BadRequestException are thrown if requestKey is already used
     */
    @ApiResponses({
        @ApiResponse(responseCode = "400", description = "requestKey is already used",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = BadRequestException.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class)
            )
        ),
        @ApiResponse(responseCode = "404", description = "Provider is Unknown",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = NotFoundException.class)
            )
        )
    })
    @Operation(summary = "For link your account with specific provider")
    @RequestMapping(value = "/link/{provider}", method = RequestMethod.GET)
    public void connect(@Parameter(description = "Provider used for this request", example = "gitlab")
                        @NonNull @PathVariable String provider,
                        @Parameter(description = "Token for identify your account",
                            example = "abcEFG145") @RequestHeader(required = false) @Nullable
                        String token, @Parameter(
                            description = "Random key for identify this request",
                            example = "ABCD") @Nullable @RequestParam String key,
                        @Parameter(description = "Pass for protect result if request is identify",
                            example = "ABCD") @Nullable @RequestHeader(required = false) String pass,
                        @NonNull HttpServletRequest request,
                        @NonNull HttpServletResponse response) throws AccessDeniedException,
                            InvalidTokenException, NotFoundException, BadRequestException {
        redirectResponse(provider, request, response, identifyToken(token).getUser(), key, pass);
    }

    /**
     * For get Token related to connected user or link a OAuth with current account after connection to provider
     * @param provider Provider used for this request
     * @param code Code given by provider used for this request
     * @param state String represent state if provider support that
     * @param error String represent potentially error thrown by provider
     * @param error_description String represent Error description thrown by provider
     * @param request Request received
     * @return response
     * @throws BadRequestException BadRequestException are thrown if provider userData is insuffisant,
     *                             email has already used by other user or this OAuth has already linked
     *                             with other user
     * @throws UnauthorizedException UnauthorizedException are thrown if error are reporting by provider or
     *                               this request doesn't throw by this serveur (State unknown) or
     *                               provider doesn't allow the connection
     * @throws NotFoundException NotFoundException are thrown if provider is unknown
     */
    @CrossOrigin
    @Operation(summary = "For get Token related to connected user after connection to provider or for link OAuth connection to your account")
    @ApiResponses({
        @ApiResponse(responseCode = "200", description = "Token Given",
           content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Token.class)
           )
        ),
        @ApiResponse(responseCode = "201", description = "OAuth linked successfully",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = OAuthLinkResult.class)
            )
        ),
        @ApiResponse(responseCode = "400", description = "Data provided by provider is insuffisant, email is already used or this oauth link already exist",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = BadRequestException.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Connection are aborted by provider",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = UnauthorizedException.class)
            )
        ),
        @ApiResponse(responseCode = "404", description = "Provider is Unknown",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = NotFoundException.class)
            )
        )
    })
    @RequestMapping(value = "/oauth/{provider}", method = RequestMethod.GET)
    @NonNull
    public ResponseEntity<?> handler(@Parameter(description = "Provider used for this request",
                                        example = "gitlab") @NonNull @PathVariable String provider,
                                     @Parameter(
                                         description = "Code given by provider used for this request",
                                         example = "abcDEF") @Nullable @RequestParam String code,
                                     @Parameter(description = "State if provider support that",
                                         example = "abcDEF") @Nullable @RequestParam String state,
                                     @Parameter(description = "Error thrown by provider",
                                         example = "Aborted") @Nullable @RequestParam String error,
                                     @Parameter(description = "Error description thrown by provider",
                                         example = "aborted by user") @Nullable
                                     @RequestParam String error_description,
                                     @NonNull HttpServletRequest request) throws BadRequestException,
                                                            UnauthorizedException, NotFoundException {
        OAuthState oAuthState = null;
        if (state != null) {
            try {
                oAuthState = stateMap.get(state);
            } catch (NullPointerException exception) {
                throw new BadRequestException("This request doesn't throw by this server...");
            }
        }
        boolean dontSendRequest = oAuthState != null && oAuthState.requestKey != null;
        ResponseEntity<?> result = null;
        try {
            OAuthProvider providerData = getProvider(provider);
            TokenResponse response = extractTokenResponse(providerData, Objects.requireNonNull(code),
                oAuthState != null ? oAuthState.codeVerifier : null, request);
            UserOAuth baseUserOAuth = null;
            try {
                baseUserOAuth = providerData.getUserData().createUserOAuthFrom(
                    fetchUserData(providerData, response));
            } catch (IllegalArgumentException exception) {
                BadRequestException badReq = new BadRequestException(exception.getMessage());
                if (!dontSendRequest)
                    throw badReq;
                else
                    oAuthState.setRequestResult(new ResponseEntity<>(badReq, HttpStatus.BAD_REQUEST));
            }
            if (baseUserOAuth != null) {
                if (oAuthState != null && oAuthState.resourceReached.startsWith("/connect"))
                    result = new ResponseEntity<>(linkHandler(baseUserOAuth, oAuthState), HttpStatus.CREATED);
                else
                    result = new ResponseEntity<>(loginHandler(baseUserOAuth), HttpStatus.OK);
                if (dontSendRequest)
                    oAuthState.setRequestResult(result);
            }
        } catch (NullPointerException ignored) {
            UnauthorizedException unauthorized;
            if (error == null && error_description == null)
                unauthorized = new UnauthorizedException("User abort the connection");
            else
                unauthorized = new UnauthorizedException(String.format("%s : %s", error, error_description));
            if (!dontSendRequest)
                throw unauthorized;
            else
                oAuthState.setRequestResult(new ResponseEntity<>(unauthorized,
                    HttpStatus.UNAUTHORIZED));
        } finally {
            if (oAuthState != null && oAuthState.requestKey == null) {
                oAuthState.invalidate();
                stateMap.remove(state);
            }
        }
        if (dontSendRequest) {
            Map<String, String> mapSend = new HashMap<>();
            mapSend.put("message", "Request are submit please continue...");
            result = new ResponseEntity<>(mapSend, HttpStatus.ACCEPTED);
        }
        return result;
    }

    /**
     * For get Token related to connected user or link a OAuth with current account after connection to provider with key
     * @param key String represent requestKey used for this request
     * @param pass String represent pass used for this request
     * @return response
     * @throws UnauthorizedException UnauthorizedException are thrown if error are reporting by provider or
     *                               this request doesn't throw by this serveur (State unknown) or
     *                               provider doesn't allow the connection or
     *                               pass doesn't match with first pass given
     * @throws NotFoundException NotFoundException are thrown if this request doesn't found
     */
    @CrossOrigin
    @Operation(summary = "For get Token related to connected user after connection to provider or for link OAuth connection to your account with key")
    @ApiResponses({
        @ApiResponse(responseCode = "200", description = "Token Given",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Token.class)
            )
        ),
        @ApiResponse(responseCode = "201", description = "OAuth linked successfully",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = OAuthLinkResult.class)
            )
        ),
        @ApiResponse(responseCode = "400", description = "Data provided by provider is insuffisant, email is already used or this oauth link already exist",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = BadRequestException.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Connection are aborted by provider or pass doesn't match with first pass given",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = UnauthorizedException.class)
            )
        ),
        @ApiResponse(responseCode = "404", description = "Request not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = NotFoundException.class)
            )
        ),
        @ApiResponse(responseCode = "409", description = "Auth request was not successful.",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = ConflictException.class)
            )
        )
    })
    @RequestMapping(value = "/oauth/return/{key}", method = RequestMethod.GET)
    @NonNull
    public ResponseEntity<?> returnOAuth(@Parameter(
                                            description = "Random key for identify this request",
                                            example = "ABCD") @NonNull @PathVariable String key,
                                         @Parameter(
                                            description = "Pass for protect result if request is identify",
                                            example = "ABCD") @Nullable @RequestHeader(required = false)
                                         String pass) throws NotFoundException,
                                                UnauthorizedException, ConflictException {
        OAuthState oAuthState = null;
        for (Map.Entry<String, OAuthState> setState : stateMap.entrySet())
            if (key.equals(setState.getValue().requestKey)) {
                oAuthState = setState.getValue();
            }
        if (oAuthState == null)
            throw new NotFoundException(String.format(
                    "OAuth result with the given key (%s) was not found", key));
        if (!Objects.equals(oAuthState.headerRelated, pass))
            throw new UnauthorizedException("Wrong pass has given... Get out !");
        try {
            return Objects.requireNonNull(oAuthState.getRequestResult());
        } catch (NullPointerException ignored) {
            throw new ConflictException("This OAuth request was not successful.");
        } finally {
            oAuthState.invalidate();
            stateMap.remove(oAuthState.key);
        }
    }

    /**
     * For get Token related to connected user after connection to provider
     * @param baseUserOAuth Base UserOAuth extracted from provider data
     * @return token
     * @throws BadRequestException BadRequestException are thrown if email has already used by other user
     */
    public Token loginHandler(@NonNull UserOAuth baseUserOAuth) throws BadRequestException {
        UserOAuth userOAuth = null;
        for (UserOAuth otherOAuth : userOAuthRepo.findAll())
            if (otherOAuth.getProviderNameRelated().equals(baseUserOAuth.getProviderNameRelated()) &&
                    otherOAuth.getUserIdOfProvider().equals(baseUserOAuth.getUserIdOfProvider()))
                userOAuth = otherOAuth;
        if (userOAuth == null) {
            User lastUser = null;
            for (User user : userRepository.findAll())
                if (user.getEmail().equals(baseUserOAuth.getUserRelated().getEmail()))
                    lastUser = user;
            if (lastUser != null)
                throw new BadRequestException(
                    "This email is already taken by other user please connect this oauth to your account");
            insertUser(baseUserOAuth.getUserRelated());
            insertUserOAuth(baseUserOAuth);
            userOAuth = baseUserOAuth;
        }
        Token lastToken = null;
        for(Token token : tokenRepository.findAll())
            if (token.getUser().equals(userOAuth.getUserRelated()))
                lastToken = token;
        if (lastToken == null) {
            lastToken = new Token(userOAuth.getUserRelated());
            insertToken(lastToken);
        }
        return lastToken;
    }

    /**
     * For link a OAuth with current account after connection to provider
     * @param baseUserOAuth Base UserOAuth extracted from provider data
     * @param oAuthState State related to this link request
     * @return token
     * @throws BadRequestException BadRequestException are thrown if email has already used by other user
     */
    public OAuthLinkResult linkHandler(@NonNull UserOAuth baseUserOAuth,
                                       @NonNull OAuthState oAuthState) throws BadRequestException,
                                                                             UnauthorizedException {
        UserOAuth userOAuth = null;
        for (UserOAuth otherOAuth : userOAuthRepo.findAll())
            if (otherOAuth.getProviderNameRelated().equals(baseUserOAuth.getProviderNameRelated()) &&
                    otherOAuth.getUserIdOfProvider().equals(baseUserOAuth.getUserIdOfProvider()))
                userOAuth = otherOAuth;
        if (userOAuth != null)
            throw new BadRequestException("This OAuth has already linked");
        baseUserOAuth.setUserRelated(oAuthState.userRelated);
        insertUserOAuth(baseUserOAuth);
        return new OAuthLinkResult(oAuthState.provider, oAuthState.userRelated);
    }

    /**
     * Method for get Token from code
     * @param provider Provider used for this request
     * @param code Code given by provider used for this request
     * @param verificationCode Code verification used for base request
     * @param request Request received
     * @return tokenResponse
     * @throws UnauthorizedException If Provider doesn't allow connection...
     */
    @NonNull
    public TokenResponse extractTokenResponse(@NonNull OAuthProvider provider, @NonNull String code,
                                              @Nullable String verificationCode,
                                              @NonNull HttpServletRequest request) throws UnauthorizedException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<TokenRequest> entity = new HttpEntity<>(new TokenRequest(provider,
            "authorization_code", code, baseUri(request, provider), verificationCode), headers);
        try {
            ResponseEntity<TokenResponse> result = restTemplate.exchange(
                provider.getTokenEndpoint(), HttpMethod.POST, entity, TokenResponse.class
            );
            return Objects.requireNonNull(result.getBody());
        } catch (HttpClientErrorException ignored) {
            throw new UnauthorizedException("Provider doesn't allow connection...");
        }
    }

    /**
     * Method for fetch UserData with Token
     * @param provider Provider used for this request
     * @param response Token response used for reach ressource
     * @return userData
     * @throws UnauthorizedException If Provider doesn't allow connection...
     */
    @NonNull
    public HashMap fetchUserData(@NonNull OAuthProvider provider,
                                 @NonNull TokenResponse response) throws UnauthorizedException {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", String.format("%s %s", response.tokenType, response.accessToken));
        ResponseEntity<HashMap> result;
        try {
            result = restTemplate.exchange(
                    provider.getUserEndpoint(), HttpMethod.GET, new HttpEntity<>(headers), HashMap.class
            );
        } catch (HttpClientErrorException ignored) {
            throw new UnauthorizedException("Provider doesn't allow connection...");
        }
        return Objects.requireNonNull(result.getBody());
    }
}