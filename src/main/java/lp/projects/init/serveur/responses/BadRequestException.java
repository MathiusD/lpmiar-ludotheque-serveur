package lp.projects.init.serveur.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class represent Bad Request Exception
 */
@CrossOrigin
@ResponseBody
@ResponseStatus(HttpStatus.BAD_REQUEST)
@Schema(description = "Bad Request Error")
public class BadRequestException extends Error{

    @Hidden
    @NonNull
    public static HttpStatus getBaseHttpStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    /**
     * Basic Constructor for BadRequestException
     * @param raisonProvided String represent specific message for this error
     */
    @JsonCreator
    public BadRequestException(@Nullable String raisonProvided) {
        super(getBaseHttpStatus(), raisonProvided);
    }

    /**
     * Basic Constructor for ForbiddenException
     */
    public BadRequestException() {
        this(null);
    }
}