package lp.projects.init.serveur.core.oauth;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.lang.reflect.Field;

/**
 * Class represent token request for OAuth provider
 */
public class TokenRequest {

    public final String client_id;
    public final String client_secret;
    public final String grant_type;
    public final String code;
    public final String redirect_uri;
    public final String code_verifier;
    public final String scope;

    /**
     * Basic Constructor for TokenRequest
     * @param client_id Id of client
     * @param client_secret Secret of client
     * @param grant_type Grant Type request
     * @param code Code received
     * @param redirect_uri Handler URI
     * @param code_verifier Verification code send in first request
     * @param scope Scope requested
     */
    @JsonCreator
    public TokenRequest(@NonNull String client_id, @NonNull String client_secret,
                        @NonNull String grant_type, @NonNull String code, @NonNull String redirect_uri,
                        @Nullable String code_verifier, @NonNull String scope) {
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.grant_type = grant_type;
        this.code = code;
        this.redirect_uri = redirect_uri;
        this.code_verifier = code_verifier;
        this.scope = scope;
    }

    /**
     * Basic Constructor for TokenRequest
     * @param provider OAuthProvider used for this request
     * @param grant_type Grant Type request
     * @param code Code received
     * @param redirect_uri Handler URI
     * @param code_verifier Verification code send in first request
     */
    public TokenRequest(@NonNull OAuthProvider provider, @NonNull String grant_type,
                        @NonNull String code, @NonNull String redirect_uri,
                        @Nullable String code_verifier) {
        this(provider.getAppId(), provider.getSecret(), grant_type, code, redirect_uri,
                provider.getWithCodeVerification() ? code_verifier : null, provider.getScope());
    }

    /**
     * Method for Write TokenRequest
     * @return tokenRequestReadable
     */
    @Override
    @NonNull
    public String toString() {
        String toString = String.format("TokenRequest with :%n");
        for(Field field : this.getClass().getDeclaredFields()) {
            try {
                toString = String.format("%s%s=%s%n", toString, field.getName() ,field.get(this));
            } catch (IllegalAccessException ignored) {}
        }
        return toString.substring(0, toString.length() - 1);
    }
}
