package lp.projects.init.serveur.db.auth;

import lp.projects.init.serveur.core.auth.MinimalRole;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface represent Repo for MinimalRole
 */
public interface MinimalRoleRepo extends JpaRepository<MinimalRole, String>{
}
