package lp.projects.init.serveur.core.game;

import org.junit.jupiter.api.Test;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test Game
 */
public class GameTest extends ShortGameTest {

    /**
     * Assert Game are exactly same attributes value
     * @param first Game represent first game
     * @param second Game represent second game
     */
    public static void assertGamesAreStrictlyEquals(@NonNull Game first, @NonNull Game second) {
        assertGamesAreStrictlyEquals(first, second, null);
    }

    /**
     * Assert Game are exactly same attributes value
     * @param first Game represent first game
     * @param second Game represent second game
     * @param message String represent message send if assertion are false
     */
    public static void assertGamesAreStrictlyEquals(@NonNull Game first, @NonNull Game second, @Nullable String message) {
        assertGamesAreStrictlyEquals(first.getShortGame(), second.getShortGame(), message);
        assertEquals(first.companyName, second.companyName, message);
        assertEquals(first.description, second.description, message);
        assertEquals(first.author, second.author, message);
        assertEquals(first.illustrator, second.illustrator, message);
        assertEquals(first.demoLink, second.demoLink, message);
        assertEquals(first.getLocation(), second.getLocation(), message);
    }

    /**
     * For test Instantiation of Game
     */
    @Test
    public void testGameInstantiation() {
        Game firstMinGame = new Game("ABC", "Example", null, 3, 4,
                (YearRecommended) null, null, null, null, null, null, null, null);
        Game tempMinGame = new Game("ABC", "Example", null, new NbPlayer(3, 4), null,
                null, null, null, null, null, null, null);
        assertGamesAreStrictlyEquals(firstMinGame, tempMinGame);
        firstMinGame = new Game("ABC", "Example", null, 5, null,
                null, null, null, null, null, null, null);
        tempMinGame = new Game("ABC", "Example", null, new NbPlayer(5), null,
                null, null,  null, null, null, null, null);
        assertGamesAreStrictlyEquals(firstMinGame, tempMinGame);
        tempMinGame = new Game(firstMinGame);
        assertGamesAreStrictlyEquals(firstMinGame, tempMinGame);
        firstMinGame = new Game("ABC", "Example", GameState.BORROWED, null,
                null, null, null, null, null, null, null,
                null, null);
        tempMinGame = new Game(firstMinGame);
        assertGamesAreStrictlyEquals(firstMinGame, tempMinGame);
    }

    /**
     * For test extract Games from CSV
     */
    @Test
    public void testExtractGameFromCSV() throws IOException {
        List<Game> gamesExpected = new ArrayList<>();
        gamesExpected.add(new Game("ABC", "Example", null, null, null,
            null, null, null, null, null, null, null));
        List<Game> gamesRelated = Game.extractGamesFromCSV(Objects.requireNonNull(getClass().getClassLoader()
            .getResource("test.csv")).getPath());
        assertEquals(gamesExpected.size(), gamesRelated.size());
        for (Game gameExpected : gamesExpected) {
            assertTrue(gamesRelated.contains(gameExpected));
            assertGamesAreStrictlyEquals(gameExpected, gamesRelated.get(gamesRelated.indexOf(gameExpected)));
        }
    }
}
