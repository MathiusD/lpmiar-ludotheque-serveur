package lp.projects.init.serveur.db.game;

import lp.projects.init.serveur.BaseAPITest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Class for test repo of MinimalGame
 */
public class MinimalGameRepoTest extends BaseAPITest {

    /**
     * Method for test import
     */
    @Test
    public void testImportMinimalGame() {
        cleanRepo();
        Assertions.assertEquals(minimalGameRepository.count(), 0);
        minimalGameRepository.save(games.get(0));
        Assertions.assertEquals(minimalGameRepository.count(), 1);
        Assertions.assertEquals(minimalGameRepository.findAll().get(0), games.get(0));
    }

    /**
     * Method for test import by own method
     */
    @Test
    public void testMethodOfDBRepo() {
        cleanRepo();
        Assertions.assertEquals(minimalGameRepository.count(), 0);
        Assertions.assertTrue(insertGame(games.get(0)));
        Assertions.assertFalse(insertGame(games.get(0)));
        Assertions.assertEquals(minimalGameRepository.count(), 1);
        Assertions.assertEquals(minimalGameRepository.findAll().get(0), games.get(0));
        Assertions.assertTrue(removeGame(games.get(0)));
        Assertions.assertFalse(removeGame(games.get(0)));
        Assertions.assertEquals(minimalGameRepository.count(), 0);
    }
}
