package lp.projects.init.serveur.core.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test MinMaxInteger
 */
public class MinMaxIntegerTest {

    /**
     * For test Instantiation of MinMaxInteger
     */
    @Test
    public void testMinMaxIntegerInstantiation() {
        MinMaxInteger baseMinMax = new MinMaxInteger(1, 2);
        MinMaxInteger targetMinMax = new MinMaxInteger(1, 2, false);
        assertEquals(baseMinMax, targetMinMax);
        targetMinMax = new MinMaxInteger(1, 2, true);
        assertNotEquals(baseMinMax, targetMinMax);
        baseMinMax = new NbPlayer(1);
        targetMinMax = new MinMaxInteger(1, null, true);
        assertEquals(baseMinMax, targetMinMax);
        targetMinMax = new MinMaxInteger(1, true);
        assertEquals(baseMinMax, targetMinMax);
    }

    /**
     * For test Exception are throw if arguments are incorrect
     */
    @Test
    public void testMinMaxIntegerFailedInstantiation() {
        assertThrows(IllegalArgumentException.class, () -> new MinMaxInteger(2, 1),
                "max must be superior or equals to min");
    }

    /**
     * For test extract MinMaxInteger from String
     */
    @Test
    public void testExtractFrom() {
        assertEquals(new MinMaxInteger(1, 2), MinMaxInteger.extractFrom("1à2"));
        assertEquals(new MinMaxInteger(1, 2), MinMaxInteger.extractFrom("1à 2"));
        assertEquals(new MinMaxInteger(1, 2), MinMaxInteger.extractFrom("1A 2"));
        assertEquals(new MinMaxInteger(1, 2), MinMaxInteger.extractFrom("1à    2"));
        assertEquals(new MinMaxInteger(1, 2), MinMaxInteger.extractFrom("1 à    2"));
        assertEquals(new MinMaxInteger(1, 2), MinMaxInteger.extractFrom("1 -    2"));
        assertNull(MinMaxInteger.extractFrom("1 -    "));
        assertEquals(new MinMaxInteger(1), MinMaxInteger.extractFrom("1 +"));
        assertEquals(new MinMaxInteger(1, false), MinMaxInteger.extractFrom("1"));
    }
}
