package lp.projects.init.serveur.controllers;

import lp.projects.init.serveur.BaseAPITest;
import lp.projects.init.serveur.core.loan.MinimalLoan;
import lp.projects.init.serveur.core.loan.State;
import lp.projects.init.serveur.core.user.User;
import lp.projects.init.serveur.responses.InvalidTokenException;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyTest extends BaseAPITest {

    /**
     * Method for get base uri of My testing
     * @return baseUri for springTests
     */
    @NonNull
    @Override
    public String baseUri() {
        return String.format("%s/my", super.baseUri());
    }

    /**
     * Test for route /my
     */
    @Test
    public void testMyUser() {
        assertEquals(new InvalidTokenException(), customExchange(InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", "truc");
        assertEquals(customExchange(headers,  InvalidTokenException.class),
                new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(my, customExchange(headers,  User.class));
    }

    /**
     * Test for route /my/loans without args
     */
    @Test
    public void testMyLoansWithoutArgs() {
        assertEquals(new InvalidTokenException(), customExchange("/loans", InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", "truc");
        assertEquals(customExchange("/loans", headers,  InvalidTokenException.class),
                new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(Arrays.asList(customExchange("/loans", headers,
                MinimalLoan[].class)), myMinimalLoans);
    }

    /**
     * Test for route /my/loans with state arg only
     */
    @Test
    public void testMyLoansWithStateOnly() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(Arrays.asList(
                customExchange("/loans?state=truc", headers,
                        MinimalLoan[].class)), new ArrayList<>());
        for (State state : State.ALL_STATES) {
            MinimalLoan[] loans = customExchange(String.format("/loans?state=%s", state.getAccessKey()),
                    headers, MinimalLoan[].class);
            List<MinimalLoan> dataLoans = new ArrayList<>();
            for (MinimalLoan loan: myLoans)
                if (loan.getState().equals(state))
                    dataLoans.add(loan);
            assertEquals(Arrays.asList(loans), dataLoans);
        }
    }
}
