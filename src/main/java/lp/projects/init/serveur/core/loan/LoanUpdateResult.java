package lp.projects.init.serveur.core.loan;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Class represent result of update Loan
 */
@Schema(description = "Result of Update Loan")
public class LoanUpdateResult {

    @Schema(description = "Message related for update", example = "Successful updated")
    public final String message;
    @Schema(description = "Loan associated to request")
    public final Loan loanRelated;

    /**
     * Basic Constructor of LoanUpdateResult
     * @param loanRelated Loan related to this update
     * @param message String represent message of this update
     */
    @JsonCreator
    public LoanUpdateResult(@NonNull Loan loanRelated, @Nullable String message) {
        this.message = message;
        this.loanRelated = loanRelated;
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof LoanUpdateResult)) return false;
        LoanUpdateResult loanUpdateResult = (LoanUpdateResult) o;
        return hashCode() == loanUpdateResult.hashCode();
    }

    /**
     * Method for get HashCode of this LoanUpdateResult
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(message).append(loanRelated).toHashCode();
    }

    /**
     * Method for Write LoanUpdateResult
     * @return loanUpdateResultReadable
     */
    @Override
    @NonNull
    public String toString(){
        return String.format("LoanUpdateResult for loan %s (With message : %s)", loanRelated.toString(), message);
    }
}
