package lp.projects.init.serveur.core.auth;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class represent role of User in this Library
 */
@Entity
@Table(name = "role")
@DiscriminatorValue("ROLE")
@PrimaryKeyJoinColumn(name = "role_name")
@Schema(description = "Role of User in this Library")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
public class Role extends MinimalRole {

    @Column(name = "role_description")
    @Schema(description = "Description of this role", example = "Role for any customers")
    protected String description;

    /**
     * Shit Constructor for Role Stored in DB
     * Used only by reflexion !
     */
    protected Role() {
        super();
    }

    /**
     * Basic constructor of Role
     * @param name String represent name of this role
     * @param isAdmin Boolean represent if this role are permission to execute sudo
     * @param description String represent description of this role (Why, How ..)
     */
    @JsonCreator
    @PersistenceConstructor
    public Role(@NonNull String name, boolean isAdmin, @Nullable String description) {
        super(name, isAdmin);
        this.description = description;
    }

    /**
     * Basic constructor of Role
     * @param name String represent name of this role
     * @param description String represent description of this role (Why, How ..)
     */
    public Role(@NonNull String name, @Nullable String description) {
        this(name, false, description);
    }

    /**
     * Basic constructor of Role
     * @param name String represent name of this role
     * @param isAdmin Boolean represent if this role are permission to execute sudo
     */
    public Role(@NonNull String name, boolean isAdmin) {
        this(name, isAdmin, null);
    }

    /**
     * Basic constructor of Role
     * @param name String represent name of this role
     */
    public Role(@NonNull String name) {
        this(name, null);
    }

    /**
     * Basic constructor of Role by other role
     * @param otherRole MinimalRole represent other role for extract base information
     */
    public Role(@NonNull MinimalRole otherRole) {
        this(otherRole.getName(), otherRole.getIsAdmin());
    }

    /**
     * Basic constructor of Role by other role
     * @param otherRole Role represent other role for extract base information
     */
    public Role(@NonNull Role otherRole) {
        this(otherRole.getName(), otherRole.getIsAdmin(), otherRole.getDescription());
    }

    public static final Role USER = new Role("User", "User of this library");
    public static final Role MANAGER = new Role("Manager", true, "Manager of this library");
    public static final List<Role> ALL_ROLES;
    static {
        List<Role> temp = new ArrayList<>();
        temp.add(USER);
        temp.add(MANAGER);
        ALL_ROLES = temp;
    }

    /**
     * Getter for get description
     * @return description
     */
    public String getDescription() {
        return description;
    }
}
