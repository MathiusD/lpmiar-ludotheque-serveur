package lp.projects.init.serveur.db.loan;

import lp.projects.init.serveur.core.loan.Loan;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface represent Repo for Loan
 */
public interface LoanRepo extends JpaRepository<Loan, String> {
}
