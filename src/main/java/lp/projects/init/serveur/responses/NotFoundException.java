package lp.projects.init.serveur.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class represent NotFound Exception
 */
@CrossOrigin
@ResponseBody
@ResponseStatus(HttpStatus.NOT_FOUND)
@Schema(description = "Not Found Error")
public class NotFoundException extends Error{

    @Hidden
    @NonNull
    public static HttpStatus getBaseHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }
    /**
     * Basic Constructor for NotFoundException
     * @param raisonProvided String represent specific message for this error
     */
    @JsonCreator
    public NotFoundException(@Nullable String raisonProvided) {
        super(getBaseHttpStatus(), raisonProvided);
    }

    /**
     * Basic Constructor for NotFoundException
     */
    public NotFoundException() {
        this(null);
    }
}