package lp.projects.init.serveur.core.oauth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.core.env.Environment;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Objects;

/**
 * Class represent base Class used for fetch data dynamically inside properties
 */
public abstract class PropertyData {

    protected final Environment environment;
    protected PropertyData parent;

    /**
     * Default Constructor for create property
     * @param environment Environnement used for fetch properties
     */
    protected PropertyData(@NonNull Environment environment) {
        this.environment = environment;
    }

    /**
     * Method return base name for get properties related to this object
     * @return baseProperty
     */
    @NonNull
    @JsonIgnore
    public abstract String getBaseProperty();

    /**
     * Method for get specific property in this property or this parent without default value
     * @param nameProperty property research
     * @return property
     */
    @Nullable
    public String getSpecificPropertyWithoutDefaultValue(@NonNull String nameProperty) {
        return getSpecificPropertyWithoutDefaultValue(nameProperty, true);
    }

    /**
     * Method for get specific property in this property without default value
     * @param nameProperty property research
     * @param isMightInherit boolean represent if research use parent or not
     * @return property
     */
    @Nullable
    public String getSpecificPropertyWithoutDefaultValue(@NonNull String nameProperty, boolean isMightInherit) {
        return getSpecificProperty(nameProperty, isMightInherit, null);
    }

    /**
     * Method for get specific property in this property or this parent
     * @param nameProperty property research
     * @return property
     */
    @NonNull
    public String getSpecificProperty(@NonNull String nameProperty) {
        return getSpecificProperty(nameProperty, true);
    }

    /**
     * Method for get specific property in this property
     * @param nameProperty property research
     * @param isMightInherit boolean represent if research use parent or not
     * @return property
     */
    @NonNull
    public String getSpecificProperty(@NonNull String nameProperty, boolean isMightInherit) {
        return Objects.requireNonNull(getSpecificProperty(nameProperty, isMightInherit, nameProperty));
    }

    /**
     * Method for get specific property in this property
     * @param nameProperty property research
     * @param defaultValue String represent defaultValue
     * @return property
     */
    @Nullable
    public String getSpecificProperty(@NonNull String nameProperty, @Nullable String defaultValue) {
        return getSpecificProperty(nameProperty, true, defaultValue);
    }

    /**
     * Method for get specific property in this property
     * @param nameProperty property research
     * @param isMightInherit boolean represent if research use parent or not
     * @param defaultValue String represent defaultValue
     * @return property
     */
    @Nullable
    public String getSpecificProperty(@NonNull String nameProperty, boolean isMightInherit,
                                      @Nullable String defaultValue) {
        String targetProperty = String.format("%s.%s", getBaseProperty(), nameProperty);
        String property = environment.getProperty(targetProperty);
        if (isMightInherit && !environment.containsProperty(targetProperty) && getParent() != null)
            property = getParent().getSpecificProperty(nameProperty);
        return property != null ? property : defaultValue;
    }

    /**
     * For get parent related to this PropertyData
     * @return parent
     */
    @Nullable
    @Schema(description = "Parent of this Data")
    public abstract PropertyData getParent();
}
