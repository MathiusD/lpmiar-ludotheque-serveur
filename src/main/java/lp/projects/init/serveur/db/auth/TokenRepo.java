package lp.projects.init.serveur.db.auth;

import lp.projects.init.serveur.core.auth.Token;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface represent Repo for Token
 */
public interface TokenRepo  extends JpaRepository<Token, String> {

}
