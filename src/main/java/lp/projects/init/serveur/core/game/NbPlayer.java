package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.db.BaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class represent indication of number Player
 */
@Embeddable
@Schema(description = "Object for represent indication of Number Player recommended to play Game")
public class NbPlayer extends MinMaxInteger {

    /**
     * Shit Constructor for NbPlayer Stored in DB
     * Used only by reflexion !
     */
    protected NbPlayer() {
        super();
    }

    /**
     * Basic Constructor for NbPlayer
     * @param minNbPlayer Integer represent minimum player for this Game
     * @param maxNbPlayer Integer represent maximum player for this Game
     * @param andMorePlayer Boolean represent if Game are played for minimum player and more (If true maximum is null)
     */
    @JsonCreator
    @PersistenceConstructor
    public NbPlayer(@NonNull @JsonProperty("min") Integer minNbPlayer,
                    @Nullable @JsonProperty("max") Integer maxNbPlayer,
                    @NonNull @JsonProperty("andMore") Boolean andMorePlayer) {
        super(minNbPlayer, maxNbPlayer, andMorePlayer);
    }

    /**
     * Basic Constructor for NbPlayer
     * @param minNbPlayer Integer represent minimum player for this Game
     * @param maxNbPlayer Integer represent maximum player for this Game
     */
    public NbPlayer(@NonNull Integer minNbPlayer, @Nullable Integer maxNbPlayer) {
        this(minNbPlayer, maxNbPlayer, false);
    }

    /**
     * Basic Constructor for NbPlayer
     * @param actualNbPlayer Integer used for create NbUser with actual number of player
     * @param andMorePlayer Boolean represent if Game are played for minimum player and more (If true maximum is null
     *                      else minimum and maximum are equal to actualNbPlayer)
     */
    public NbPlayer(@NonNull Integer actualNbPlayer, @NonNull Boolean andMorePlayer) {
        this(actualNbPlayer, actualNbPlayer, andMorePlayer);
    }

    /**
     * Basic Constructor for NbPlayer
     * @param minNbPlayer Integer represent minimum player for this Game
     */
    public NbPlayer(@NonNull Integer minNbPlayer) {
        this(minNbPlayer, true);
    }

    /**
     * Copy Constructor for NbPlayer
     * @param minMaxInteger NbPlayer represent instance used for copy this
     */
    public NbPlayer(@NonNull MinMaxInteger minMaxInteger) {
        this(minMaxInteger.min, minMaxInteger.max, minMaxInteger.andMore);
    }

    /**
     * Getter for andMore of NbPlayer
     * @return andMore
     */
    @Schema(description = "Boolean represent if Game are played for minimum player and more (If true maximum is null)",
            example = "false")
    @Override
    @NonNull
    @Access(AccessType.PROPERTY)
    public Boolean getAndMore() {
        return super.getAndMore();
    }

    /**
     * Getter for max of NbPlayer
     * @return max
     */
    @Schema(description = "Max Number of Player for play this Game", example = "42")
    @Override
    @Nullable
    @Access(AccessType.PROPERTY)
    public Integer getMax() {
        return super.getMax();
    }

    /**
     * Getter for min of NbPlayer
     * @return min
     */
    @Schema(description = "Minimum Number of Player for play this Game", example = "3")
    @Override
    @NonNull
    @Access(AccessType.PROPERTY)
    public Integer getMin() {
        return super.getMin();
    }

    @Nullable
    public static NbPlayer extractFrom(@Nullable String dataUsed) {
        MinMaxInteger baseData = MinMaxInteger.extractFrom(dataUsed);
        return baseData != null ? new NbPlayer(baseData) : null;
    }
}
