package lp.projects.init.serveur.db.auth;

import lp.projects.init.serveur.BaseAPITest;
import lp.projects.init.serveur.core.auth.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Class for test repo of Role
 */
public class RoleRepoTest extends BaseAPITest {

    /**
     * Method for test import
     */
    @Test
    public void testImportRole() {
        cleanRepo();
        Assertions.assertEquals(roleRepository.count(), 0);
        roleRepository.save(Role.MANAGER);
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), Role.MANAGER);
    }

    /**
     * Method for test import by own method
     */
    @Test
    public void testMethodOfDBRepo() {
        cleanRepo();
        Assertions.assertEquals(roleRepository.count(), 0);
        Assertions.assertTrue(insertRole(Role.MANAGER));
        Assertions.assertFalse(insertRole(Role.MANAGER));
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), Role.MANAGER);
        Assertions.assertTrue(removeRole(Role.MANAGER));
        Assertions.assertFalse(removeRole(Role.MANAGER));
        Assertions.assertEquals(roleRepository.count(), 0);
    }
}
