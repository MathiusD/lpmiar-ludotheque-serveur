package lp.projects.init.serveur.db.game;

import lp.projects.init.serveur.core.game.Game;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface represent Repo for Game
 */
public interface GameRepo extends JpaRepository<Game, String> {
}
