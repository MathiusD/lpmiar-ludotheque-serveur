package lp.projects.init.serveur.db.user;

import lp.projects.init.serveur.core.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface represent Repo for User
 */
public interface UserRepo extends JpaRepository<User, String> { }
