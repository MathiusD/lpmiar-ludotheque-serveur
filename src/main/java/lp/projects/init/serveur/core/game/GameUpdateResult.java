package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Class represent result of update Game
 */
@Schema(description = "Result of Update Game")
public class GameUpdateResult {

    @Schema(description = "Message related for update", example = "Successful updated")
    public final String message;
    @Schema(description = "Game associated to request")
    public final Game gameRelated;

    /**
     * Basic Constructor of GameUpdateResult
     * @param gameRelated Game related to this update
     * @param message String represent message of this update
     */
    @JsonCreator
    public GameUpdateResult(@NonNull Game gameRelated, @Nullable String message) {
        this.message = message;
        this.gameRelated = gameRelated;
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof GameUpdateResult)) return false;
        GameUpdateResult gameUpdateResult = (GameUpdateResult) o;
        return hashCode() == gameUpdateResult.hashCode();
    }

    /**
     * Method for get HashCode of this GameUpdateResult
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(message).append(gameRelated).toHashCode();
    }

    /**
     * Method for Write GameUpdateResult
     * @return gameUpdateResultReadable
     */
    @Override
    @NonNull
    public String toString(){
        return String.format("GameUpdateResult for game %s (With message : %s)", gameRelated.toString(), message);
    }
}
