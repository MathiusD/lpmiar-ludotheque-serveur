package lp.projects.init.serveur.core.loan;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.game.Game;
import lp.projects.init.serveur.core.game.GameSchema;
import lp.projects.init.serveur.core.game.MinimalGame;
import lp.projects.init.serveur.core.user.MinimalUser;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Class represent Loan in this library
 */
@Entity
@Table(name = "loan")
@DiscriminatorValue("LOAN")
@PrimaryKeyJoinColumn(name = "loan_id")
@Schema(description = "Loan of this Library")
public class Loan extends MinimalLoan {

    @ManyToOne(optional = false, targetEntity = MinimalUser.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    @Schema(description = "User related to this Loan")
    protected MinimalUser user;

    /**
     * Shit Constructor for Loan Stored in DB
     * Used only by reflexion !
     */
    protected Loan() {
        super();
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param loan Loan represent Loan used for extract information
     * @param game Game represent Game related of this Loan
     * @param gameSchema GameSchema represent schema used for save Game
     */
    public Loan(@NonNull Loan loan, @NonNull Game game, @NonNull GameSchema gameSchema) {
        super(loan.getId(), game, loan.getStartedAt(), loan.getEndAt(), loan.getState(), gameSchema);
        this.user = loan.user;
    }

    /**
     * Basic Constructor for Loan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     * @param gameDataIsMinified GameSchema represent schema used for save Game
     */
    @JsonCreator
    @PersistenceConstructor
    public Loan(@NonNull String id, @NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Long startedAt, @NonNull Long endAt, @NonNull MinimalState state,
                boolean gameDataIsMinified) {
        super(id, game, startedAt, endAt, state, gameDataIsMinified);
        this.user = user.getMinimalUser();
    }

    /**
     * Basic Constructor for Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     * @param gameDataIsMinified GameSchema represent schema used for save Game
     */
    public Loan(@NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Long startedAt, @NonNull Long endAt, @NonNull MinimalState state,
                boolean gameDataIsMinified) {
        this(UUID.randomUUID().toString(), game, user, startedAt, endAt, state, gameDataIsMinified);
    }

    /**
     * Basic Constructor for Loan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     */
    public Loan(@NonNull String id, @NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Long startedAt, @NonNull Long endAt, @NonNull MinimalState state) {
        this(id, game, user, startedAt, endAt, state, true);
    }

    /**
     * Basic Constructor for Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     */
    public Loan(@NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Long startedAt, @NonNull Long endAt, @NonNull MinimalState state) {
        this(game, user, startedAt, endAt, state, true);
    }

    /**
     * Basic Constructor for Loan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     */
    public Loan(@NonNull String id, @NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Long endAt, @NonNull MinimalState state) {
        this(id, game, user, new Date().getTime(), endAt, state);
    }

    /**
     * Basic Constructor for Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     */
    public Loan(@NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Long endAt, @NonNull MinimalState state) {
        this(game, user, new Date().getTime(), endAt, state);
    }

    /**
     * Basic Constructor for Loan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     * @param gameDataIsMinified GameSchema represent schema used for save Game
     */
    public Loan(@NonNull String id, @NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Long endAt, @NonNull MinimalState state, boolean gameDataIsMinified) {
        this(id, game, user, new Date().getTime(), endAt, state, gameDataIsMinified);
    }

    /**
     * Basic Constructor for Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     * @param gameDataIsMinified GameSchema represent schema used for save Game
     */
    public Loan(@NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Long endAt, @NonNull MinimalState state, boolean gameDataIsMinified) {
        this(game, user, new Date().getTime(), endAt, state, gameDataIsMinified);
    }

    /**
     * Basic Constructor for Loan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param endAt Long represent timestamp of start of this Loan
     */
    public Loan(@NonNull String id, @NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Long endAt) {
        this(id, game, user, endAt, DEFAULT_STATE);
    }

    /**
     * Basic Constructor for Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param endAt Long represent timestamp of start of this Loan
     */
    public Loan(@NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Long endAt) {
        this(game, user, endAt, DEFAULT_STATE);
    }

    /**
     * Basic Constructor for Loan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param unit TimeUnit represent unit used for loanDuration
     * @param loanDuration Integer represent duration of Loan
     */
    public Loan(@NonNull String id, @NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull TimeUnit unit, @NonNull Integer loanDuration) {
        super(id, game, unit, loanDuration);
        this.user = user.getMinimalUser();
    }

    /**
     * Basic Constructor for Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param unit TimeUnit represent unit used for loanDuration
     * @param loanDuration Integer represent duration of Loan
     */
    public Loan(@NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull TimeUnit unit, @NonNull Integer loanDuration) {
        this(UUID.randomUUID().toString(), game, user, unit, loanDuration);
    }

    /**
     * Basic Constructor for Loan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param loanDuration Integer represent duration of Loan (In Seconds)
     */
    public Loan(@NonNull String id, @NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Integer loanDuration) {
        this(id, game, user, TimeUnit.SECONDS, loanDuration);
    }

    /**
     * Basic Constructor for Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param user MinimalUser represent who had this Loan
     * @param loanDuration Integer represent duration of Loan (In Seconds)
     */
    public Loan(@NonNull MinimalGame game, @NonNull MinimalUser user,
                @NonNull Integer loanDuration) {
        this(game, user, TimeUnit.SECONDS, loanDuration);
    }

    /**
     * Constructor of Loan by Loan or child
     * @param otherLoan Loan used for extract informations
     */
    public Loan(@NonNull Loan otherLoan) {
        this(otherLoan.getId(), otherLoan.getGame(), otherLoan.getUser(), otherLoan.getStartedAt(),
            otherLoan.getEndAt(), otherLoan.getState(), otherLoan.getGameDataIsMinified());
    }

    /**
     * Getter for get user
     * @return user
     */
    @NonNull
    public MinimalUser getUser() {
        return new MinimalUser(user);
    }
}
