package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.db.BaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.Embeddable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class represent average Time of Game
 */
@Embeddable
@Schema(description = "Object represent average Time of Game")
public class GameTime extends BaseEntity {

    @Schema(description = "TimeUnit exploited for each Time value", example = "MINUTES")
    protected TimeUnit timeUnit;
    @Schema(description = "Minimum Time for played this Game", example = "20")
    protected Integer minTime;
    @Schema(description = "Maximum Time for played this Game", example = "30")
    protected Integer maxTime;
    @Schema(description = "Average Time for played this Game", example = "25")
    protected Integer averageTime;
    @Schema(description = " Boolean represent if Game duration is up to Minimum Time (If true maximum is null)",
            example = "false")
    protected Boolean upToMin;

    /**
     * Shit Constructor for GameTime Stored in DB
     * Used only by reflexion !
     */
    protected GameTime() {
        super();
    }

    /**
     * Basic Constructor for GameTime
     * @param timeUnit TimeUnit represent unit used for different time values provided
     * @param minTime Integer represent minimum Time of Game
     * @param maxTime Integer represent maximum Time of Game
     * @param averageTime Integer represent average Time of Game
     * @param upToMin Boolean represent if Game duration is up to Minimum Time (If true maximum is null)
     */
    @JsonCreator
    @PersistenceConstructor
    public GameTime(@NonNull TimeUnit timeUnit, @Nullable Integer minTime, @Nullable Integer maxTime,
                    @Nullable Integer averageTime, @NonNull Boolean upToMin) {
        if (minTime == null && averageTime == null && maxTime != null) {
            minTime = maxTime;
            maxTime = null;
        }
        if (minTime != null && maxTime != null) {
            if (minTime > maxTime)
                throw new IllegalArgumentException(
                    "maxTime must be superior or equals to minTime");
            if (averageTime != null) {
                if (minTime > averageTime || averageTime > maxTime)
                    throw new IllegalArgumentException(
                        "averageTime must be between minTime and maxTime");
            } else
                averageTime = (minTime + maxTime) / 2;
        }
        if (averageTime == null && minTime == null && maxTime == null)
            throw new IllegalArgumentException("A time-related argument must be defined");
        this.timeUnit = timeUnit;
        this.minTime = minTime;
        this.averageTime = averageTime;
        this.upToMin = upToMin;
        this.maxTime = !upToMin ? maxTime : null;
    }

    /**
     * Basic Constructor for GameTime
     * @param minTime Integer represent minimum Time of Game (In minutes)
     * @param maxTime Integer represent maximum Time of Game (In minutes)
     * @param averageTime Integer represent average Time of Game (In minutes)
     * @param upToMin Boolean represent if Game duration is up to Minimum Time (If true maximum is null)
     */
    public GameTime(@Nullable Integer minTime, @Nullable Integer maxTime, @Nullable Integer averageTime,
                    @NonNull Boolean upToMin) {
        this(TimeUnit.MINUTES, minTime, maxTime, averageTime, upToMin);
    }

    /**
     * Basic Constructor for GameTime
     * @param timeUnit TimeUnit represent unit used for different time values provided
     * @param minTime Integer represent minimum Time of Game
     * @param maxTime Integer represent maximum Time of Game
     * @param averageTime Integer represent average Time of Game
     */
    public GameTime(@NonNull TimeUnit timeUnit, @Nullable Integer minTime, @Nullable Integer maxTime,
                    @Nullable Integer averageTime) {
        this(timeUnit, minTime, maxTime, averageTime, false);
    }

    /**
     * Basic Constructor for GameTime
     * @param minTime Integer represent minimum Time of Game (In minutes)
     * @param maxTime Integer represent maximum Time of Game (In minutes)
     * @param averageTime Integer represent average Time of Game (In minutes)
     */
    public GameTime(@Nullable Integer minTime, @Nullable Integer maxTime, @Nullable Integer averageTime) {
        this(minTime, maxTime, averageTime, false);
    }

    /**
     * Basic Constructor for GameTime
     * @param timeUnit TimeUnit represent unit used for different time values provided
     * @param minTime Integer represent minimum Time of Game
     * @param maxTime Integer represent maximum Time of Game
     */
    public GameTime(@NonNull TimeUnit timeUnit, @Nullable Integer minTime, @Nullable Integer maxTime) {
        this(timeUnit, minTime, maxTime, null);
    }

    /**
     * Basic Constructor for GameTime
     * @param minTime Integer represent minimum Time of Game (In minutes)
     * @param maxTime Integer represent maximum Time of Game (In minutes)
     */
    public GameTime(@Nullable Integer minTime, @Nullable Integer maxTime) {
        this(minTime, maxTime, null);
    }

    /**
     * Basic Constructor for GameTime
     * @param timeUnit TimeUnit represent unit used for different time values provided
     * @param minTime Integer represent minimum Time of Game
     * @param upToMin Boolean represent if Game duration is up to Minimum Time (If true maximum is null)
     */
    public GameTime(@NonNull TimeUnit timeUnit, @NonNull Integer minTime, @NonNull Boolean upToMin) {
        this(timeUnit, minTime, null, null, upToMin);
    }

    /**
     * Basic Constructor for GameTime
     * @param minTime Integer represent minimum Time of Game (In minutes)
     * @param upToMin Boolean represent if Game duration is up to Minimum Time (If true maximum is null)
     */
    public GameTime(@NonNull Integer minTime, @NonNull Boolean upToMin) {
        this(minTime, null, null, upToMin);
    }

    /**
     * Basic Constructor for GameTime
     * @param timeUnit TimeUnit represent unit used for different time values provided
     * @param averageTime Integer represent average Time of Game
     */
    public GameTime(@NonNull TimeUnit timeUnit, @NonNull Integer averageTime) {
        this(timeUnit, null, null, averageTime);
    }

    /**
     * Basic Constructor for GameTime
     * @param averageTime Integer represent average Time of Game (In minutes)
     */
    public GameTime(@NonNull Integer averageTime) {
        this((Integer) null, null, averageTime);
    }

    /**
     * Method for format GameTime in String
     * @return representation
     */
    @Schema(description = "Format of GameTime in String", example = "20-30 (25) minutes")
    @NonNull
    public String getTimeRepresentation() {
        String represention = null;
        if (upToMin)
            represention = String.format("<= %d", minTime);
        else {
            if (minTime != null)
                represention = minTime.toString();
            if (maxTime != null)
                represention = String.format("%s-%d", represention, maxTime);
        }
        if (averageTime != null)
            if (represention != null)
                represention = String.format("%s (%d)", represention, averageTime);
            else
                represention = averageTime.toString();
        return String.format("%s %s", represention, timeUnit.toString().toLowerCase());
    }

    /**
     * Method for Write GameTime
     * @return representation
     */
    @Override
    public String toString() {
        return getTimeRepresentation();
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof GameTime)) return false;
        GameTime gameTime = (GameTime) o;
        return hashCode() == gameTime.hashCode();
    }

    /**
     * Method for get HashCode of this GameTime
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(timeUnit).append(minTime).append(maxTime)
            .append(averageTime).append(upToMin).toHashCode();
    }

    /**
     * Getter for upToMin of GameTime
     * @return upToMin
     */
    @NonNull
    public Boolean getUpToMin() {
        return upToMin;
    }

    /**
     * Getter for averageTime of GameTime
     * @return averageTime
     */
    @Nullable
    public Integer getAverageTime() {
        return averageTime;
    }

    /**
     * Getter for maxTime of GameTime
     * @return maxTime
     */
    @Nullable
    public Integer getMaxTime() {
        return maxTime;
    }

    /**
     * Getter for minTime of GameTime
     * @return minTime
     */
    @Nullable
    public Integer getMinTime() {
        return minTime;
    }

    /**
     * Getter for timeUnit of GameTime
     * @return timeUnit
     */
    @NonNull
    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    protected final static Map<String, TimeUnit> timeUnitMap;

    static {
        Map<String, TimeUnit> tempTimeUnitMap = new HashMap<>();
        tempTimeUnitMap.put("secondes", TimeUnit.SECONDS);
        tempTimeUnitMap.put("sec", TimeUnit.SECONDS);
        tempTimeUnitMap.put("s", TimeUnit.SECONDS);
        tempTimeUnitMap.put("minutes", TimeUnit.MINUTES);
        tempTimeUnitMap.put("min", TimeUnit.MINUTES);
        tempTimeUnitMap.put("mn", TimeUnit.MINUTES);
        tempTimeUnitMap.put("heures", TimeUnit.HOURS);
        tempTimeUnitMap.put("h", TimeUnit.HOURS);
        tempTimeUnitMap.put("jours", TimeUnit.DAYS);
        tempTimeUnitMap.put("j", TimeUnit.DAYS);
        timeUnitMap = tempTimeUnitMap;
    }

    @Nullable
    protected static TimeUnit extractTimeUnitFrom(@NonNull Matcher matcherUsed, int groupIndex) {
        return (matcherUsed.group(groupIndex) != null &&
            !matcherUsed.group(groupIndex).equalsIgnoreCase("")) &&
            timeUnitMap.containsKey(matcherUsed.group(groupIndex)) ?
            timeUnitMap.get(matcherUsed.group(groupIndex)) : null;
    }

    @Nullable
    public static GameTime extractFrom(@Nullable String dataUsed) {
        if (dataUsed != null) {
            String timeUnitDetection = " *((secondes|sec|s)|(minutes|min|mn)|(heures|h)|(jours|jour|j))?";
            Pattern patternRange = Pattern.compile(
                    String.format("(\\d+)%s *[-àa] *(\\d+)%s", timeUnitDetection, timeUnitDetection),
                    Pattern.CASE_INSENSITIVE);
            Matcher matchRange = patternRange.matcher(dataUsed);
            if (matchRange.matches()) {
                TimeUnit firstTimeUnitUsed = extractTimeUnitFrom(matchRange, 2);
                TimeUnit secondTimeUnitUsed = extractTimeUnitFrom(matchRange, 8);
                if (firstTimeUnitUsed != null || secondTimeUnitUsed != null) {
                    if (firstTimeUnitUsed != null && secondTimeUnitUsed != null &&
                            firstTimeUnitUsed != secondTimeUnitUsed)
                        return new GameTime(firstTimeUnitUsed, Integer.valueOf(matchRange.group(1)),
                            Long.valueOf(firstTimeUnitUsed.convert(Integer.parseInt(matchRange.group(7)),
                                secondTimeUnitUsed)).intValue());
                    TimeUnit defaultTimeUnitUsed = firstTimeUnitUsed != null ? firstTimeUnitUsed : secondTimeUnitUsed;
                    return new GameTime(defaultTimeUnitUsed, Integer.valueOf(matchRange.group(1)),
                        Integer.valueOf(matchRange.group(7)));
                }
                return new GameTime(Integer.valueOf(matchRange.group(1)), Integer.valueOf(matchRange.group(7)));
            }
            Pattern patternMin = Pattern.compile(String.format("(\\d+)%s", timeUnitDetection),
                    Pattern.CASE_INSENSITIVE);
            Matcher matchMin = patternMin.matcher(dataUsed);
            if (matchMin.matches()) {
                TimeUnit timeUnitUsed = extractTimeUnitFrom(matchMin, 2);
                if (timeUnitUsed != null)
                    return new GameTime(timeUnitUsed, Integer.valueOf(matchMin.group(1)));
                return new GameTime(Integer.valueOf(matchMin.group(1)));
            }
            Pattern patternUpToMin = Pattern.compile(
                    String.format("(<=|moins de) *(\\d+)%s", timeUnitDetection),
                    Pattern.CASE_INSENSITIVE);
            Matcher matchUpToMin = patternUpToMin.matcher(dataUsed);
            if (matchUpToMin.matches()) {
                TimeUnit timeUnitUsed = extractTimeUnitFrom(matchUpToMin, 3);
                if (timeUnitUsed != null)
                    return new GameTime(timeUnitUsed, Integer.valueOf(matchUpToMin.group(2)), true);
                return new GameTime(Integer.valueOf(matchUpToMin.group(2)), true);
            }
        }
        return null;
    }
}
