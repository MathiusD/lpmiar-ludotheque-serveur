package lp.projects.init.serveur.db.auth;

import lp.projects.init.serveur.BaseAPITest;
import lp.projects.init.serveur.core.auth.Role;
import lp.projects.init.serveur.core.auth.Token;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Class for test repo of Token
 */
public class TokenRepoTest extends BaseAPITest {

    /**
     * Method for test import
     */
    @Test
    public void testImportToken() {
        cleanRepo();
        Assertions.assertEquals(tokenRepository.count(), 0);
        Assertions.assertEquals(userRepository.count(), 0);
        Assertions.assertEquals(roleRepository.count(), 0);
        roleRepository.save(Role.MANAGER);
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), Role.MANAGER);
        userRepository.save(my);
        Assertions.assertEquals(userRepository.count(), 1);
        Assertions.assertEquals(userRepository.findAll().get(0), my);
        Token tokenTemp = new Token(my, "ABCSDES");
        tokenRepository.save(tokenTemp);
        Assertions.assertEquals(tokenRepository.count(), 1);
        Assertions.assertEquals(tokenRepository.findAll().get(0), tokenTemp);
    }

    /**
     * Method for test import by own method
     */
    @Test
    public void testMethodOfDBRepo() {
        cleanRepo();
        Token tokenTemp = new Token(my, "ABCSDEES");
        Assertions.assertEquals(tokenRepository.count(), 0);
        Assertions.assertEquals(userRepository.count(), 0);
        Assertions.assertEquals(roleRepository.count(), 0);
        Assertions.assertTrue(insertToken(tokenTemp));
        Assertions.assertFalse(insertToken(tokenTemp));
        Assertions.assertEquals(tokenRepository.count(), 1);
        Assertions.assertEquals(tokenRepository.findAll().get(0), tokenTemp);
        Assertions.assertEquals(userRepository.count(), 1);
        Assertions.assertEquals(userRepository.findAll().get(0), my);
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), Role.MANAGER);
        Assertions.assertTrue(removeToken(tokenTemp));
        Assertions.assertFalse(removeToken(tokenTemp));
        Assertions.assertEquals(tokenRepository.count(), 0);
        Assertions.assertEquals(userRepository.count(), 1);
        Assertions.assertEquals(userRepository.findAll().get(0), my);
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), Role.MANAGER);
    }
}
