package lp.projects.init.serveur.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lp.projects.init.serveur.DBRepo;
import lp.projects.init.serveur.core.auth.Token;
import lp.projects.init.serveur.core.game.Game;
import lp.projects.init.serveur.core.game.GameState;
import lp.projects.init.serveur.core.loan.*;
import lp.projects.init.serveur.responses.*;
import lp.projects.init.serveur.responses.Error;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static lp.projects.init.serveur.core.loan.State.ALL_STATES;

/**
 * Class represent Controller of Loans
 */
@RestController
public class Loans extends DBRepo {

    /**
     * For get Loan by id
     * @param token String represent Token for identify your account
     * @param id String represent id of Loan research
     * @return loan
     */
    @CrossOrigin
    @Operation(summary = "Get a Loan by its id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found the Loan",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Loan.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        ),
        @ApiResponse(responseCode = "404", description = "Loan not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = NotFoundException.class)
            )
        )
    })
    @RequestMapping(value = "/loans/{id}", method = RequestMethod.GET)
    @NonNull
    public Loan loan(@Parameter(description = "Token for identify your account", example = "abcEFG145")
                                @RequestHeader(required = false) @Nullable String token,
                            @Parameter(description = "Id of loan you are looking for", example = "ABC")
                            @PathVariable(value="id") @NonNull String id) throws Error {
        Token tokenGiven = identifyToken(token);
        Loan loan = loanRepository.findById(id).orElseThrow(
            new NotFoundException(String.format("Loan with the given id (%s) was not found", id)));
        grantAccess(tokenGiven, String.format("/loans/%s", id), loan.getUser());
        return new Loan(loan);
    }

    /**
     * For update Loan by id
     * @param token String represent Token for identify your account
     * @param id String represent id of Loan research
     * @param update LoanUpdate represent instructions for update this loan
     * @return loanUpdate
     */
    @CrossOrigin
    @Operation(summary = "For update Loan by its id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Result of Loan update",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = LoanUpdateResult.class)
            )
        ),
        @ApiResponse(responseCode = "400", description = "Bad Request",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = BadRequestException.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class)
            )
        ),
        @ApiResponse(responseCode = "404", description = "Loan not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = NotFoundException.class)
            )
        )
    })
    @RequestMapping(value = "/loans/{id}", method = RequestMethod.POST)
    @NonNull
    public LoanUpdateResult updateLoan(@Parameter(description = "Token for identify your account",
                                       example = "abcEFG145") @RequestHeader(required = false) @Nullable
                                       String token, @Parameter(description = "Id of loan you are looking for",
                                       example = "ABC") @PathVariable(value="id") @NonNull String id,
                                       @Parameter(description = "Object contain update instruction")
                                       @RequestBody LoanUpdate update) throws Error {
        Token tokenRelated = identifyToken(token);
        Loan loan = loanRepository.findById(id).orElseThrow(
            new NotFoundException(String.format("Loan with the given id (%s) was not found", id)));
        grantAccess(tokenRelated, String.format("/loans/%s", loan.getId()));
        try {
            loan.setState(MinimalState.fetchMinimalState(update.state));
            updateLoan(loan);
            Game game = gameRepository.getOne(loan.getGame().getId());
            if (loan.getState().equals(State.WAITING))
                game.setGameState(GameState.BORROWED);
            if (loan.getState().equals(State.RENDERED))
                game.setGameState(GameState.AVAILABLE);
            updateGame(game);
            return new LoanUpdateResult(loan(token , loan.getId()),
                String.format("Loan is update for state : %s", update.state));
        } catch (IllegalArgumentException exception) {
            throw new BadRequestException(exception.getMessage());
        }
    }

    /**
     * For delete Loan by id
     * @param token String represent Token for identify your account
     * @param id String represent id of Loan research
     */
    @CrossOrigin
    @Operation(summary = "For delete Loan by its id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "204", description = "Loan Deleted"),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class)
            )
        ),
        @ApiResponse(responseCode = "404", description = "Loan not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = NotFoundException.class)
            )
        )
    })
    @RequestMapping(value = "/loans/{id}", method = RequestMethod.DELETE)
    @NonNull
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteLoan(@Parameter(description = "Token for identify your account",
                                       example = "abcEFG145") @RequestHeader(required = false) @Nullable String token,
                                       @Parameter(description = "Id of loan you are looking for", example = "ABC")
                                       @PathVariable(value="id") @NonNull String id) throws Error {
        Token tokenGiven = identifyToken(token);
        Loan loan = loanRepository.findById(id).orElseThrow(
            new NotFoundException(String.format("Loan with the given id (%s) was not found", id)));
        grantAccess(tokenGiven, String.format("/loans/%s", loan.getId()),
            loan.getState().equals(State.REQUESTED) ? loan.getUser() : null);
        removeLoan(loan);
        Game gameRelated = gameRepository.getOne(loan.getGame().getId());
        gameRelated.setGameState(loan.getState().equals(State.REQUESTED) || loan.getState().equals(State.RENDERED) ?
            GameState.AVAILABLE : GameState.UNAVAILABLE);
        updateGame(gameRelated);
    }

    /**
     * For get states related to Loan
     * @return states
     */
    @CrossOrigin
    @Operation(summary = "Get states related to Loan")
    @ApiResponse(responseCode = "200", description = "View All State related to Loan",
        content = @Content(mediaType = "application/json",
            array = @ArraySchema(uniqueItems = true, schema = @Schema(implementation = State.class))
        )
    )
    @RequestMapping(value = "/loans/states", method = RequestMethod.GET)
    @NonNull
    public List<State> schemas() {
        return ALL_STATES;
    }
}
