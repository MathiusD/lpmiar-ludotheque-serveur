package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class represent indication of number Player
 */
@Embeddable
@Access(AccessType.FIELD)
public class MinMaxInteger extends MinMax<Integer> {

    /**
     * Shit Constructor for MinMaxInteger Stored in DB
     * Used only by reflexion !
     */
    protected MinMaxInteger() {
        super();
    }

    /**
     * Basic Constructor for MinMaxInteger
     * @param min Integer represent minimum
     * @param max Integer represent maximum
     * @param andMore Boolean represent if used with minimum and more (If true maximum is null)
     */
    @JsonCreator
    @PersistenceConstructor
    public MinMaxInteger(@NonNull Integer min, @Nullable Integer max,
                  @NonNull Boolean andMore) {
        super(min, max, andMore);
    }

    /**
     * Basic Constructor for MinMaxInteger
     * @param min Integer represent minimum
     * @param max Integer represent maximum
     */
    public MinMaxInteger(@NonNull Integer min, @Nullable Integer max) {
        this(min, max, false);
    }

    /**
     * Basic Constructor for MinMaxInteger
     * @param current Integer used for create MinMax with current value
     * @param andMore Boolean represent if used with minimum and more (If true maximum is null
     *                else minimum and maximum are equal to current)
     */
    public MinMaxInteger(@NonNull Integer current, @NonNull Boolean andMore) {
        this(current, current, andMore);
    }

    /**
     * Basic Constructor for MinMaxInteger
     * @param min Integer represent minimum
     */
    public MinMaxInteger(@NonNull Integer min) {
        this(min, true);
    }

    /**
     * Copy Constructor for MinMaxInteger
     * @param minMaxInteger MinMaxInteger represent instance used for copy this
     */
    public MinMaxInteger(@NonNull MinMaxInteger minMaxInteger) {
        this(minMaxInteger.min, minMaxInteger.max, minMaxInteger.andMore);
    }

    /**
     * Getter for max of MinMaxInteger
     * @return max
     */
    @Nullable
    @Override
    @Access(AccessType.PROPERTY)
    public Integer getMax() {
        return max;
    }

    /**
     * Getter for min of MinMaxInteger
     * @return min
     */
    @NonNull
    @Override
    @Access(AccessType.PROPERTY)
    public Integer getMin() {
        return min;
    }

    /**
     * Setter for min of MinMaxInteger
     * @param min Min of MinMax
     */
    private void setMin(Integer min) {
        this.min = min;
    }

    /**
     * Setter for max of MinMaxInteger
     * @param max Max of MinMax
     */
    private void setMax(Integer max) {
        this.max = max;
    }

    @Nullable
    public static MinMaxInteger extractFrom(@Nullable String dataUsed) {
        if (dataUsed != null) {
            Pattern patternRange = Pattern.compile("(\\d+) *[-àa] *(\\d+)",
                    Pattern.CASE_INSENSITIVE);
            Matcher matchRange = patternRange.matcher(dataUsed);
            if (matchRange.matches())
                return new MinMaxInteger(Integer.valueOf(matchRange.group(1)), Integer.valueOf(matchRange.group(2)));
            Pattern patternUp = Pattern.compile("(\\d+) *\\+",
                    Pattern.CASE_INSENSITIVE);
            Matcher matchUp = patternUp.matcher(dataUsed);
            if (matchUp.matches())
                return new MinMaxInteger(Integer.valueOf(matchUp.group(1)));
            Pattern patternMin = Pattern.compile("(\\d+)",
                    Pattern.CASE_INSENSITIVE);
            Matcher matchMin = patternMin.matcher(dataUsed);
            if (matchMin.matches())
                return new MinMaxInteger(Integer.valueOf(matchMin.group(1)), false);
        }
        return null;
    }
}
