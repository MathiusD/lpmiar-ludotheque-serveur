package lp.projects.init.serveur.core.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test GameState
 */
public class GameStateTest {

    /**
     * For test method fork of GameState
     */
    @Test
    public void testForkOfGameState() {
        assertNotEquals(GameState.AVAILABLE.name, GameState.AVAILABLE.forkOnlyIsAvailable("blap").name);
        assertNotEquals(GameState.AVAILABLE.note, GameState.AVAILABLE.forkOnlyIsAvailable("blap").note);
        assertEquals(GameState.AVAILABLE.isAvailable, GameState.AVAILABLE.forkOnlyIsAvailable("blap").isAvailable);
        assertEquals(GameState.AVAILABLE.name, GameState.AVAILABLE.fork("blap").name);
        assertNotEquals(GameState.AVAILABLE.note, GameState.AVAILABLE.fork("blap").note);
        assertEquals(GameState.AVAILABLE.isAvailable, GameState.AVAILABLE.fork("blap").isAvailable);
        assertEquals(GameState.AVAILABLE.name, GameState.AVAILABLE.fork(false).name);
        assertEquals(GameState.AVAILABLE.note, GameState.AVAILABLE.fork(false).note);
        assertNotEquals(GameState.AVAILABLE.isAvailable, GameState.AVAILABLE.fork(false).isAvailable);
        assertNotEquals(GameState.AVAILABLE.name, GameState.AVAILABLE.fork("blap", false).name);
        assertEquals(GameState.AVAILABLE.note, GameState.AVAILABLE.fork("blap", false).note);
        assertNotEquals(GameState.AVAILABLE.isAvailable,
            GameState.AVAILABLE.fork("blap", false).isAvailable);
        assertEquals(GameState.AVAILABLE.name, GameState.AVAILABLE.fork(false, "blap").name);
        assertNotEquals(GameState.AVAILABLE.note, GameState.AVAILABLE.fork(false, "blap").note);
        assertNotEquals(GameState.AVAILABLE.isAvailable,
                GameState.AVAILABLE.fork(false, "blap").isAvailable);
        assertNotEquals(GameState.AVAILABLE.name, GameState.AVAILABLE.fork("blap", "blap").name);
        assertNotEquals(GameState.AVAILABLE.note, GameState.AVAILABLE.fork("blap", "blap").note);
        assertEquals(GameState.AVAILABLE.isAvailable, GameState.AVAILABLE.fork("blap", "blap").isAvailable);
    }
}
