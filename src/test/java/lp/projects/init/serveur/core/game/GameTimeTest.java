package lp.projects.init.serveur.core.game;

import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test GameTime
 */
public class GameTimeTest {

    /**
     * For test Instantiation of GameTime
     */
    @Test
    public void testGameTimeInstantiation() {
        GameTime baseGameTime = new GameTime(15);
        GameTime otherGameTime = new GameTime(TimeUnit.MINUTES, 15);
        assertEquals(baseGameTime, otherGameTime);
        otherGameTime = new GameTime(TimeUnit.MICROSECONDS, 15);
        assertNotEquals(baseGameTime, otherGameTime);
        baseGameTime = new GameTime(20, 30, 25);
        otherGameTime = new GameTime(TimeUnit.MINUTES, 20, 30);
        assertEquals(baseGameTime, otherGameTime);
        baseGameTime = new GameTime(30, true);
        otherGameTime = new GameTime(TimeUnit.MINUTES, 30, true);
        assertEquals(baseGameTime, otherGameTime);
    }

    /**
     * For test Exception are throw if arguments are incorrect
     */
    @Test
    public void testNbPlayerFailedInstantiation() {
        assertThrows(IllegalArgumentException.class, () -> new GameTime(2, 1),
                "maxTime must be superior or equals to minTime");
        assertThrows(IllegalArgumentException.class, () -> new GameTime((Integer) null, (Integer) null),
                "A time-related argument must be defined");
        assertThrows(IllegalArgumentException.class, () -> new GameTime(1, 2, 3),
                "averageTime must be between minTime and maxTime");
    }

    /**
     * For test timeRepresentation Method
     */
    @Test
    public void testTimeRepresentation() {
        assertEquals(new GameTime(15).getTimeRepresentation(), "15 minutes");
        assertEquals(new GameTime(15, 20).getTimeRepresentation(), "15-20 (17) minutes");
        assertEquals(new GameTime(15, true).getTimeRepresentation(), "<= 15 minutes");
        assertEquals(new GameTime(TimeUnit.HOURS, 1).getTimeRepresentation(), "1 hours");
    }

    /**
     * For test extract GameTime from String
     */
    @Test
    public void testExtractFrom() {
        assertEquals(new GameTime(1, 2), GameTime.extractFrom("1à2"));
        assertEquals(new GameTime(1, 2), GameTime.extractFrom("1à 2"));
        assertEquals(new GameTime(1, 2), GameTime.extractFrom("1à    2"));
        assertEquals(new GameTime(1, 2), GameTime.extractFrom("1 à    2"));
        assertEquals(new GameTime(1, 2), GameTime.extractFrom("1 -    2"));
        assertNull(GameTime.extractFrom("1 -    "));
        assertEquals(new GameTime(1), GameTime.extractFrom("1"));
        assertEquals(new GameTime(1, true), GameTime.extractFrom("<= 1"));
        assertEquals(new GameTime(TimeUnit.HOURS, 1), GameTime.extractFrom("1 h"));
        assertEquals(new GameTime(TimeUnit.SECONDS,1, true), GameTime.extractFrom("<= 1 s"));
        assertEquals(new GameTime(TimeUnit.SECONDS, 1, 2), GameTime.extractFrom("1 sec - 2 secondes"));
        assertEquals(new GameTime(TimeUnit.MINUTES, 1, 120), GameTime.extractFrom("1 minutes - 2 heures"));
    }
}
