package lp.projects.init.serveur.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.user.MinimalUser;
import org.springframework.lang.NonNull;

/**
 * Class represent AccessDenied Exception
 */
public class AccessDeniedException extends ForbiddenException {
    @Schema(description = "User related to this exception")
    public final MinimalUser userRelated;
    @Schema(description = "Resource requested", example = "/admin")
    public final String sectionReached;

    /**
     * Basic Constructor for AccessDeniedException
     * @param userRelated MinimalUser represent User related to this exception
     * @param sectionReached String represent Resource requested
     */
    @JsonCreator
    public AccessDeniedException(@NonNull MinimalUser userRelated, @NonNull String sectionReached) {
        super(String.format("User %s does not have access rights to resource `%s`.", userRelated.getId(),
            sectionReached));
        this.userRelated = userRelated.getMinimalUser();
        this.sectionReached = sectionReached;
    }
}
