httpsSupported=$1;
githubAppId=$2;
githubSecret=$3;
outputFile=$4;
zoomAppId=$5;
zoomSecret=$6;
baseProvider="oauth.providers=gitlab,gitlabNantes,gitlabLille,github,discord";

{
  if [ "$httpsSupported" = true ] ; then
    echo $baseProvider,google,zoom,microsoft;
  else
    echo $baseProvider;
  fi
  echo oauth.gitlab.appId="$gitlabAppId";
  echo oauth.gitlab.secret="$gitlabSecret";

  echo oauth.gitlabNantes.appId="$gitlabNantesAppId";
  echo oauth.gitlabNantes.secret="$gitlabNantesSecret";

  echo oauth.gitlabLille.appId="$gitlabLilleAppId";
  echo oauth.gitlabLille.secret="$gitlabLilleSecret";

  echo oauth.discord.appId="$discordAppId";
  echo oauth.discord.secret="$discordSecret";

  if [ "$httpsSupported" = true ] ; then
    echo oauth.google.appId="$googleAppId";
    echo oauth.google.secret="$googleSecret";

    echo oauth.zoom.appId="$zoomAppId";
    echo oauth.zoom.secret="$zoomSecret";

    echo oauth.microsoft.appId="$microsoftAppId";
    echo oauth.microsoft.secret="$microsoftSecret";
  fi

  echo oauth.github.appId="$githubAppId";
  echo oauth.github.secret="$githubSecret";
}  >> "$outputFile";