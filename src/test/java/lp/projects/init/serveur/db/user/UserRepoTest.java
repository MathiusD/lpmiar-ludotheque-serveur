package lp.projects.init.serveur.db.user;

import lp.projects.init.serveur.BaseAPITest;
import lp.projects.init.serveur.core.auth.Role;
import lp.projects.init.serveur.core.auth.Token;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Class for test repo of User
 */
public class UserRepoTest extends BaseAPITest {

    /**
     * Method for test import
     */
    @Test
    public void testImportUser() {
        cleanRepo();
        Assertions.assertEquals(userRepository.count(), 0);
        Assertions.assertEquals(roleRepository.count(), 0);
        roleRepository.save(Role.MANAGER);
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), Role.MANAGER);
        userRepository.save(my);
        Assertions.assertEquals(userRepository.count(), 1);
        Assertions.assertEquals(userRepository.findAll().get(0), my);
    }

    /**
     * Method for test import by own method
     */
    @Test
    public void testMethodOfDBRepo() {
        cleanRepo();
        Assertions.assertEquals(userRepository.count(), 0);
        Assertions.assertEquals(roleRepository.count(), 0);
        Assertions.assertTrue(insertUser(my));
        Assertions.assertFalse(insertUser(my));
        Assertions.assertEquals(userRepository.count(), 1);
        Assertions.assertEquals(userRepository.findAll().get(0), my);
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), Role.MANAGER);
        Assertions.assertTrue(removeUser(my));
        Assertions.assertFalse(removeUser(my));
        Assertions.assertEquals(userRepository.count(), 0);
        Assertions.assertEquals(roleRepository.count(), 1);
        Assertions.assertEquals(roleRepository.findAll().get(0), Role.MANAGER);
    }
}
