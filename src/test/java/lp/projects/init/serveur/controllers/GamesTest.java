package lp.projects.init.serveur.controllers;

import lp.projects.init.serveur.BaseAPITest;
import lp.projects.init.serveur.core.game.*;
import lp.projects.init.serveur.core.loan.Loan;
import lp.projects.init.serveur.core.loan.LoanRelated;
import lp.projects.init.serveur.core.loan.State;
import lp.projects.init.serveur.responses.AccessDeniedException;
import lp.projects.init.serveur.responses.InvalidTokenException;
import lp.projects.init.serveur.responses.NotFoundException;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

import static lp.projects.init.serveur.core.game.GameTest.assertGamesAreStrictlyEquals;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test Games Controller
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GamesTest extends BaseAPITest {

    /**
     * Method for get base uri of Games testing
     * @return baseUri for springTests
     */
    @NonNull
    @Override
    public String baseUri() {
        return String.format("%s/games", super.baseUri());
    }

    /**
     * Test for route /games
     * @throws ClassNotFoundException if class for schema array isn't found
     */
    @ParameterizedTest
    @MethodSource("testGamesProvider")
    public void testGames(GameSchema schema, String type, GameState state, String company,
                          String name, String author, String illustrator, Integer start,
                          Integer max, Integer minYear, String location,
                          Integer minNbPlayer) throws ClassNotFoundException {
        MinimalGame[] games;
        Class<? extends MinimalGame[]> temp;
        List<Game> dataOut;
        boolean schemaIsSet = schema != null;
        boolean typeIsSet = type != null;
        boolean stateIsSet = state != null;
        boolean companyIsSet = company != null;
        boolean nameIsSet = name != null;
        boolean authorIsSet = author != null;
        boolean illustratorIsSet = illustrator != null;
        boolean startIsSet = start != null;
        boolean maxIsSet = max != null;
        boolean minYearIsSet = minYear != null;
        boolean locationIsSet = location != null;
        boolean minNbPlayerIsSet = minNbPlayer != null;
        boolean schemaOrTypeIsSet = schemaIsSet || typeIsSet;
        boolean schemaTypeOrStateIsSet = schemaOrTypeIsSet || stateIsSet;
        boolean schemaTypeStateOrCompanyIsSet = schemaTypeOrStateIsSet || companyIsSet;
        boolean schemaTypeStateCompanyOrNameIsSet = schemaTypeStateOrCompanyIsSet || nameIsSet;
        boolean schemaTypeStateCompanyNameOrAuthorIsSet = schemaTypeStateCompanyOrNameIsSet || authorIsSet;
        boolean schemaTypeStateCompanyNameOrAuthorOrIllustratorIsSet = schemaTypeStateCompanyNameOrAuthorIsSet ||
            illustratorIsSet;
        boolean anyoneIsSetExceptMaxAndMinYearAndLocationAndMinNbPlayer =
            schemaTypeStateCompanyNameOrAuthorOrIllustratorIsSet || startIsSet;
        boolean anyoneIsSetExceptMinYearAndLocationAndMinNbPlayer =
            anyoneIsSetExceptMaxAndMinYearAndLocationAndMinNbPlayer || maxIsSet;
        boolean anyoneIsSetExceptLocationAndMinNbPlayer = anyoneIsSetExceptMinYearAndLocationAndMinNbPlayer ||
            minYearIsSet;
        boolean anyoneIsSetExceptMinNbPlayer = anyoneIsSetExceptLocationAndMinNbPlayer || locationIsSet;
        boolean anyoneIsSet = anyoneIsSetExceptMinNbPlayer || minNbPlayerIsSet;
        String urlArgs = anyoneIsSet ? String.format(
            "?%s%s%s%s%s%s%s%s%s%s%s%s",
            schemaIsSet ? String.format("schema=%s", schema.name) : "",
            typeIsSet ? String.format(
                    "%stype=%s", schemaIsSet ? "&": "", type
            ) : "",
            stateIsSet ? String.format(
                    "%sstate=%s", schemaOrTypeIsSet ? "&": "", state.getName()
            ) : "",
            companyIsSet ? String.format(
                    "%scompanyName=%s", schemaTypeOrStateIsSet ? "&": "", company
            ) : "",
            nameIsSet ? String.format(
                    "%snameGame=%s", schemaTypeStateOrCompanyIsSet ? "&": "", name
            ) : "",
            authorIsSet ? String.format(
                    "%sauthor=%s", schemaTypeStateCompanyOrNameIsSet ? "&" : "", author
            ) : "",
            illustratorIsSet ? String.format(
                    "%sillustrator=%s", schemaTypeStateCompanyNameOrAuthorIsSet ? "&" : "", illustrator
            ) : "",
            startIsSet ? String.format(
                    "%sstart=%d", schemaTypeStateCompanyNameOrAuthorOrIllustratorIsSet ? "&" : "", start
            ) : "",
            maxIsSet ? String.format(
                    "%smax=%d", anyoneIsSetExceptMaxAndMinYearAndLocationAndMinNbPlayer ? "&" : "", max
            ) : "",
            minYearIsSet ? String.format(
                    "%sminYear=%d", anyoneIsSetExceptMinYearAndLocationAndMinNbPlayer ? "&" : "", minYear
            ) : "",
            locationIsSet ? String.format(
                    "%slocation=%s", anyoneIsSetExceptLocationAndMinNbPlayer ? "&" : "", location
            ) : "",
            minNbPlayerIsSet ? String.format(
                    "%sminNbPlayer=%d", anyoneIsSetExceptMinNbPlayer ? "&" : "", minNbPlayer
            ) : ""
        ) : "";
        String uri = String.format("%s%s", baseUri(), urlArgs);
        String errorMsg = String.format("Results aren't same : %s", uri);
        temp = schema != null ?
            (Class<? extends MinimalGame[]>) Class.forName("[L" + schema.gameClass.getName() + ";") : MinimalGame[].class;
        games = customExchange(urlArgs, temp);
        dataOut = new ArrayList<>();
        for (Game game : this.games)
            if (game != null && (type == null ||
                    game.getType() != null && game.getType().equalsIgnoreCase(type)
                ) && (state == null || game.getGameState().equals(state)) && (
                    company == null || game.getCompanyName() != null &&
                    game.getCompanyName().equalsIgnoreCase(company)
                ) && (name == null || game.getName().equalsIgnoreCase(name)) && (
                    illustrator == null || game.getIllustrator() != null &&
                    game.getIllustrator().equalsIgnoreCase(illustrator)
                ) && (author == null || game.getAuthor() != null &&
                    game.getAuthor().equalsIgnoreCase(author)
                ) && (minYear == null || game.getYearRecommended() != null &&
                    game.getYearRecommended().getMin() <= minYear
                ) && (location == null || game.getLocation() != null &&
                    game.getLocation().equalsIgnoreCase(location)
                ) && (minNbPlayer == null || game.getNbPlayer() != null &&
                    game.getNbPlayer().getMin() <= minNbPlayer)
                )
                dataOut.add(game);
        if (maxIsSet)
            assertTrue(Arrays.asList(games).size() <= max, errorMsg);
        int sizeExpected = Math.max(dataOut.size() - (startIsSet ? start : 0), 0);
        sizeExpected = maxIsSet ? (sizeExpected > max ? max : sizeExpected) : sizeExpected;
        assertEquals(sizeExpected, Arrays.asList(games).size(), errorMsg);
        for (MinimalGame game: games) {
            assertTrue(dataOut.contains(game), errorMsg);
            assertGamesAreStrictlyEquals(dataOut.get(dataOut.indexOf(game)), game, errorMsg);
        }
    }

    public Stream<Arguments> testGamesProvider() {
        List<Arguments> argsUsed = new ArrayList<>();
        List<GameSchema> schemas = extractSample(GameSchema.ALL_SCHEMA);
        List<String> localTypes = extractSample(types, 1);
        List<GameState> states = new ArrayList<>(GameState.ALL_STATES);
        List<String> localCompanyNames = extractSample(companyNames, 1);
        List<String> localNameGame = extractSample(nameGame, 1);
        List<String> localIllustrators = extractSample(illustrators, 1);
        List<String> localAuthors = extractSample(authors, 1);
        List<Integer> size = new ArrayList<>();
        for (int index = 0; index < this.games.size(); index++)
            size.add(index);
        size = extractSample(size, 1);
        List<Integer> age = new ArrayList<>();
        for (int index = 0; index < 21; index++)
            age.add(index);
        age = extractSample(age, 1);
        List<String> localLocations = extractSample(localAuthors, 1);
        List<Integer> nbPlayers = new ArrayList<>();
        nbPlayers.add(new Random().nextInt(nbPlayerMinMax.getMax() != null ?
            nbPlayerMinMax.getMax() : Integer.MAX_VALUE));
        nbPlayers.add(null);
        if (nbPlayerMinMax.getMin() > Integer.MIN_VALUE)
            nbPlayers.add(nbPlayerMinMax.getMin() - 1);
        if (nbPlayerMinMax.getMax() != null && nbPlayerMinMax.getMax() < Integer.MAX_VALUE)
            nbPlayers.add(nbPlayerMinMax.getMax() + 1);
        for (GameSchema schema : schemas) {
            for (String type : localTypes) {
                for (GameState state : states) {
                    for (String company : localCompanyNames) {
                        for (String name : localNameGame) {
                            for (String illustrator : localIllustrators) {
                                for (String author : localAuthors) {
                                    for (Integer start : size) {
                                        for (Integer max : size) {
                                            for (Integer minYear : age) {
                                                for (String location : localLocations) {
                                                    for (Integer nbPlayer : nbPlayers)
                                                        argsUsed.add(Arguments.of(
                                                            schema, type, state, company, name,
                                                            author, illustrator, start, max, minYear,
                                                            location, nbPlayer
                                                        ));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return extractSample(argsUsed, maxDataForParameterizedTest,false).stream();
    }

    /**
     * Test for route /games/{id}
     */
    @ParameterizedTest
    @MethodSource("testGameProvider")
    public void testGame(Game game) {
        assertGamesAreStrictlyEquals(game, customExchange(String.format("/%s", game.getId()), Game.class));
    }

    /**
     * Test for route /games/{id} with wrong id
     */
    @Test
    public void testGameNotFound() {
        assertEquals(new NotFoundException("Game with the given id (truc) was not found"),
                customExchange("/truc", NotFoundException.class));
    }

    public Stream<Arguments> testGameProvider() {
        List<Arguments> args = new ArrayList<>();
        for (Game game : games)
            args.add(Arguments.of(game));
        return extractSample(args, maxDataForParameterizedTest,false).stream();
    }

    /**
     * Test for route /games/schema
     */
    @Test
    public void testGameSchemas () {
        assertEquals(GameSchema.ALL_SCHEMA, Arrays.asList(customExchange("/schemas",
            GameSchema[].class)));
    }

    /**
     * Test for route /games/types
     */
    @Test
    public void testGameTypes () {
        assertEquals(types, Arrays.asList(customExchange("/types", String[].class)));
    }

    /**
     * Test for route /games/states
     */
    @Test
    public void testGameStates () {
        assertEquals(GameState.ALL_STATES, Arrays.asList(customExchange("/states", GameState[].class)));
    }

    /**
     * Test for route /games/{id}/take
     */
    @ParameterizedTest
    @MethodSource("testGameProvider")
    public void testGameTake(Game game) {
        assertEquals(new InvalidTokenException(), customExchange(String.format("/%s/take", game.getId()),
                InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        BorrowResult result = customExchange(String.format("/%s/take", game.getId()), headers,
            BorrowResult.class);
        if (game.getGameState().getIsAvailable()) {
            assertNotNull(result.loanRelated);
            assertEquals(result.loanRelated.getState(), State.REQUESTED);
            assertEquals(result.message, String.format(
                "The request was successful concluded. (Loan related : %s)", result.loanRelated.getId()));
            assertEquals(new BorrowResult(String.format("Game isn't available (Reason : %s)",
                GameState.REQUESTED.getNote())), customExchange(String.format("/%s/take", game.getId()),
                    headers, BorrowResult.class));
        } else
            assertEquals(new BorrowResult(String.format("Game isn't available (Reason : %s)",
                game.getGameState().getNote())), result);
    }

    /**
     * Test for route /games/{id}/take with wrong id
     */
    @Test
    public void testGameTakeNotFound() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(new NotFoundException("Game with the given id (truc) was not found"),
                customExchange("/truc/take", headers, NotFoundException.class));
    }

    /**
     * Test for route /games/{id}/loanRelated
     */
    @ParameterizedTest
    @MethodSource("testGameSchemaProvider")
    public void testGameLoanRelated(Game game, GameSchema schema) {
        String baseUri = String.format("/%s/loanRelated", game.getId());
        String currentUri = String.format("%s?%s", baseUri, schema != null ?
            String.format("schema=%s", schema.name) : "");
        assertEquals(new InvalidTokenException(), customExchange(currentUri,
                InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", otherToken.getToken());
        assertEquals(new AccessDeniedException(otherToken.getUser(),
                String.format("/games%s", baseUri)),
            customExchange(currentUri, headers, AccessDeniedException.class));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        List<Loan> loansRelated = new ArrayList<>();
        for (Loan loan : loanRepository.findAll())
            if (loan.getGame().equals(game))
                loansRelated.add(loan);
        LoanRelated expected = new LoanRelated(loansRelated, schema == null ? game.getMinimalGame() :
                schema.matchingGameWithThisSchema(game));
        LoanRelated received = customExchange(currentUri, headers, LoanRelated.class);
        assertEquals(expected, received);
        assertGamesAreStrictlyEquals(expected.getGameRelated(), received.getGameRelated());
    }

    /**
     * Test for route /games/{id}/loanRelated with wrong id
     */
    @Test
    public void testGameLoanRelatedNotFound() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(new NotFoundException("Game with the given id (truc) was not found"),
                customExchange("/truc/loanRelated", headers, NotFoundException.class));
    }

    public Stream<Arguments> testGameSchemaProvider() {
        List<Arguments> args = new ArrayList<>();
        List<GameSchema> schemas = new ArrayList<>(GameSchema.ALL_SCHEMA);
        schemas.add(null);
        for (GameSchema schema : schemas)
            for (Game game : games)
                args.add(Arguments.of(game, schema));
            return extractSample(args, maxDataForParameterizedTest,false).stream();
    }

    /**
     * Test for route /games/{id} by POST
     */
    @ParameterizedTest
    @MethodSource("testGameProvider")
    public void testGameUpdate(Game game) throws JSONException {
        HttpEntity<GameUpdate> entity = new HttpEntity<>(new GameUpdate(null, null, null));
        assertEquals(new InvalidTokenException(), customExchange(String.format("/%s", game.getId()),
            HttpMethod.POST, entity, InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", otherToken.getToken());
        entity = new HttpEntity<>(new GameUpdate(null, null, null), headers);
        assertEquals(new AccessDeniedException(otherToken.getUser(),
                String.format("/games/%s", game.getId())),
            customExchange(String.format("/%s", game.getId()), HttpMethod.POST, entity, AccessDeniedException.class));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        headers.add("Content-type", "application/json");
        entity = new HttpEntity<>(new GameUpdate(null,
            game.getDescription(), game.getLocation()), headers);
        assertEquals(new GameUpdateResult(game, "Game doesn't updated because any update are provided"),
            customExchange(String.format("/%s", game.getId()), HttpMethod.POST, entity, GameUpdateResult.class));
        entity = new HttpEntity<>(new GameUpdate(game.getGameState(),
                game.getDescription(), game.getLocation()), headers);
        assertEquals(new GameUpdateResult(game, "Game doesn't updated because any update are provided"),
                customExchange(String.format("/%s", game.getId()), HttpMethod.POST, entity, GameUpdateResult.class));
        GameState state = GameState.BROKEN.fork("This Game are Broken by XXX");
        if (game.getLocation() != null || game.getDescription() != null) {
            entity = new HttpEntity<>(new GameUpdate(null, null, null), headers);
            assertEquals(new GameUpdateResult(game, "Game doesn't updated because any update are provided"),
                customExchange(String.format("/%s", game.getId()), HttpMethod.POST, entity,
                    GameUpdateResult.class));
            assertEquals(new GameUpdateResult(game, "Game doesn't updated because any update are provided"),
                customExchange(String.format("/%s", game.getId()), HttpMethod.POST,
                    new HttpEntity<>(new JSONObject().toString(), headers), GameUpdateResult.class));
            assertEquals(new GameUpdateResult(game, "Game doesn't updated because any update are provided"),
                customExchange(String.format("/%s", game.getId()), HttpMethod.POST,
                    new HttpEntity<>(new JSONObject()
                        .put(game.getType() != null ? "location" : "description", null).toString(),
                            headers), GameUpdateResult.class));
        }
        entity = new HttpEntity<>(new GameUpdate(state,
                game.getDescription(), game.getLocation()), headers);
        if (customExchange(String.format("/%s/loanRelated", game.getId()), headers, LoanRelated.class)
            .getCurrentLoanRelated() == null) {
            game.setGameState(state);
            assertEquals(new GameUpdateResult(game, String.format(
                "Game are successfully updated.%n\tState are updated.")),
                customExchange(String.format("/%s", game.getId()), HttpMethod.POST, entity, GameUpdateResult.class));
        } else
            assertEquals(new GameUpdateResult(game, String.format(
                "Game are updated with errors.%n\tState aren't updated (Reason : Loan are linked to this game close this loan before perform this action).")),
                customExchange(String.format("/%s", game.getId()), HttpMethod.POST, entity, GameUpdateResult.class));
        game.setLocation("Test Location");
        game.setDescription("Test Description");
        entity = new HttpEntity<>(new GameUpdate(null, game.getDescription(), game.getLocation()), headers);
        assertEquals(new GameUpdateResult(game, String.format(
            "Game are successfully updated.%n\tDescription are updated.%n\tLocation are updated.")),
            customExchange(String.format("/%s", game.getId()), HttpMethod.POST, entity, GameUpdateResult.class));
    }

    /**
     * Test for route /games/{id}/ by POST with wrong id
     */
    @Test
    public void testGameUpdateNotFound() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        HttpEntity<GameUpdate> entity = new HttpEntity<>(new GameUpdate(null, null, null), headers);
        assertEquals(new NotFoundException("Game with the given id (truc) was not found"),
                customExchange("/truc", HttpMethod.POST, entity, NotFoundException.class));
    }

    /**
     * Test for route /games/import by POST
     */
    @Test
    public void testGamesUpload() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(Paths.get(
                Objects.requireNonNull(getClass().getClassLoader().getResource("test.csv")).getPath()
        )));
        HttpEntity<MultiValueMap<String, Object>> entity = new HttpEntity<>(body, headers);
        assertEquals(new InvalidTokenException(), customExchange("/import",
                HttpMethod.POST, entity, InvalidTokenException.class));
        headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("token", otherToken.getToken());
        entity = new HttpEntity<>(body, headers);
        assertEquals(new AccessDeniedException(otherToken.getUser(), "/games/import"),
                customExchange("/import", HttpMethod.POST, entity, AccessDeniedException.class));
        headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.add("token", myToken.getToken());
        entity = new HttpEntity<>(body, headers);
        List<Game> csvGames = new ArrayList<>();
        csvGames.add(new Game("ABC", "Example", null, null, null,
                null, null, null, null, null, null, null));
        assertEquals(new GameUploadSummary(csvGames, new ArrayList<>(), "1 games are successfully uploaded."),
            customExchange("/import", HttpMethod.POST, entity, GameUploadSummary.class));
        assertEquals(new GameUploadSummary(new ArrayList<>(), csvGames,
                "No games are successfully uploaded and 1 games aren't uploaded."),
                customExchange("/import", HttpMethod.POST, entity, GameUploadSummary.class));
    }
}
