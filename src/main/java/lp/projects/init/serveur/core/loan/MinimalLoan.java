package lp.projects.init.serveur.core.loan;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.game.Game;
import lp.projects.init.serveur.core.game.GameSchema;
import lp.projects.init.serveur.core.game.MinimalGame;
import lp.projects.init.serveur.db.BaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static javax.persistence.InheritanceType.JOINED;

/**
 * Class represent Minimal Loan in this library
 */
@Entity
@Inheritance(strategy = JOINED)
@DiscriminatorValue("MIN_LOAN")
@Schema(description = "Base Information of Loan of this Library")
public class MinimalLoan extends BaseEntity {

    public final static MinimalState DEFAULT_STATE = State.REQUESTED;

    @Id
    @Column(name = "loan_id")
    @Schema(description = "Id for this Loan", example = "ABF")
    protected String id;
    @ManyToOne(optional = false, targetEntity = MinimalGame.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id", nullable = false)
    @Schema(description = "Game related to this Loan")
    protected MinimalGame game;
    @Column(name = "loan_started_at")
    @Schema(description = "Timestamp for start Loan", example = "946684800")
    protected Long startedAt;
    @Column(name = "loan_end_at")
    @Schema(description = "Timestamp for end Loan", example = "1577836800")
    protected Long endAt;
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "name", column = @Column(name = "loan_state_name"))
    })
    @Schema(description = "State related to this Loan", implementation = MinimalState.class)
    protected StateName state;
    @Column(name = "loan_game_data_is_minified")
    @Schema(description = "Boolean for specify if gameData are minified")
    protected boolean gameDataIsMinified;

    /**
     * Shit Constructor for MinimalLoan Stored in DB
     * Used only by reflexion !
     */
    protected MinimalLoan() {
        super();
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param id String represent id of Loan
     * @param game Game represent Game related of this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     * @param gameSchema GameSchema represent schema used for save Game
     */
    public MinimalLoan(@NonNull String id, @NonNull Game game, @NonNull Long startedAt,
                       @NonNull Long endAt, @NonNull MinimalState state, @NonNull GameSchema gameSchema) {
        this(id, gameSchema.matchingGameWithThisSchema(game), startedAt, endAt, state,
                gameSchema == GameSchema.MINIMAL_DATA);
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param game Game represent Game related of this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     * @param gameSchema GameSchema represent schema used for save Game
     */
    public MinimalLoan(@NonNull Game game, @NonNull Long startedAt,
                       @NonNull Long endAt, @NonNull MinimalState state, @NonNull GameSchema gameSchema) {
        this(gameSchema.matchingGameWithThisSchema(game), startedAt, endAt, state,
                gameSchema == GameSchema.MINIMAL_DATA);
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     * @param gameDataIsMinified GameSchema represent schema used for save Game
     */
    @JsonCreator
    @PersistenceConstructor
    public MinimalLoan(@NonNull String id, @NonNull MinimalGame game, @NonNull Long startedAt,
                       @NonNull Long endAt, @NonNull MinimalState state, boolean gameDataIsMinified) {
        super();
        this.id = id;
        this.game = gameDataIsMinified ? game.getMinimalGame() : game;
        this.startedAt = startedAt;
        this.endAt = endAt;
        this.state = state.getMinimalState();
        this.gameDataIsMinified = gameDataIsMinified;
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param game MinimalGame represent Game related of this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     * @param gameDataIsMinified GameSchema represent schema used for save Game
     */
    public MinimalLoan(@NonNull MinimalGame game, @NonNull Long startedAt,
                       @NonNull Long endAt, @NonNull MinimalState state, boolean gameDataIsMinified) {
        this(UUID.randomUUID().toString(), game, startedAt, endAt, state, gameDataIsMinified);
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param loan MinimalLoan represent Loan used for extract information
     * @param game Game represent Game related of this Loan
     * @param gameSchema GameSchema represent schema used for save Game
     */
    public MinimalLoan(@NonNull MinimalLoan loan, @NonNull Game game, @NonNull GameSchema gameSchema) {
        this(loan.getId(), game, loan.getStartedAt(), loan.getEndAt(), loan.getState(), gameSchema);
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     */
    public MinimalLoan(@NonNull String id, @NonNull MinimalGame game, @NonNull Long startedAt,
                       @NonNull Long endAt, @NonNull MinimalState state) {
        super();
        this.id = id;
        this.game = game.getMinimalGame();
        this.startedAt = startedAt;
        this.endAt = endAt;
        this.state = state.getMinimalState();
        this.gameDataIsMinified = true;
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param game MinimalGame represent Game related of this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     */
    public MinimalLoan(@NonNull MinimalGame game, @NonNull Long startedAt,
                       @NonNull Long endAt, @NonNull MinimalState state) {
        this(UUID.randomUUID().toString(), game, startedAt, endAt, state);
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param id String represent id of Loan
     * @param game Game represent Game related of this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state State represent State of this Loan
     */
    public MinimalLoan(@NonNull String id, @NonNull Game game, @NonNull Long startedAt,
                       @NonNull Long endAt, @NonNull State state) {
        this(id, game.getMinimalGame(), startedAt, endAt, state.getMinimalState());
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     */
    public MinimalLoan(@NonNull String id, @NonNull MinimalGame game, @NonNull Long endAt,
                       @NonNull MinimalState state) {
        this(id, game, new Date().getTime(), endAt, state);
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param game MinimalGame represent Game related of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     */
    public MinimalLoan(@NonNull MinimalGame game, @NonNull Long endAt,
                       @NonNull MinimalState state) {
        this(game, new Date().getTime(), endAt, state);
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     */
    public MinimalLoan(@NonNull String id, @NonNull MinimalGame game, @NonNull Long endAt) {
        this(id, game, endAt, DEFAULT_STATE);
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param game MinimalGame represent Game related of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     */
    public MinimalLoan(@NonNull MinimalGame game, @NonNull Long endAt) {
        this(game, endAt, DEFAULT_STATE);
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param unit TimeUnit represent unit used for loanDuration
     * @param loanDuration Integer represent duration of Loan
     */
    public MinimalLoan(@NonNull String id, @NonNull MinimalGame game, @NonNull TimeUnit unit,
                       @NonNull Integer loanDuration) {
        super();
        this.id = id;
        this.game = game.getMinimalGame();
        this.startedAt = new Date().getTime();
        this.endAt = startedAt + TimeUnit.SECONDS.convert(loanDuration, unit);
        this.state = DEFAULT_STATE.getMinimalState();
        this.gameDataIsMinified = true;
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param game MinimalGame represent Game related of this Loan
     * @param unit TimeUnit represent unit used for loanDuration
     * @param loanDuration Integer represent duration of Loan
     */
    public MinimalLoan(@NonNull MinimalGame game, @NonNull TimeUnit unit,
                       @NonNull Integer loanDuration) {
        this(UUID.randomUUID().toString(), game, unit, loanDuration);
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param id String represent id of Loan
     * @param game MinimalGame represent Game related of this Loan
     * @param loanDuration Integer represent duration of Loan (In Seconds)
     */
    public MinimalLoan(@NonNull String id, @NonNull MinimalGame game, @NonNull Integer loanDuration) {
        this(id, game, TimeUnit.SECONDS, loanDuration);
    }

    /**
     * Basic Constructor for MinimalLoan
     * @param game MinimalGame represent Game related of this Loan
     * @param loanDuration Integer represent duration of Loan (In Seconds)
     */
    public MinimalLoan(@NonNull MinimalGame game, @NonNull Integer loanDuration) {
        this(game, TimeUnit.SECONDS, loanDuration);
    }

    /**
     * Constructor of MinimalLoan by MinimalLoan or child
     * @param otherLoan MinimalLoan used for extract informations
     */
    public MinimalLoan(@NonNull MinimalLoan otherLoan) {
        this(otherLoan.getId(), otherLoan.getGame(), otherLoan.getStartedAt(), otherLoan.getEndAt(),
            otherLoan.getState(), otherLoan.getGameDataIsMinified());
    }


    /**
     * Method for change State of MinimalLoan
     * @param newState State represent new State of this MinimalLoan
     * @return auth
     */
    public boolean changeState(@NonNull MinimalState newState) {
        boolean auth = this.getState().changeArePermitted(newState);
        if (auth)
            this.state = newState.getMinimalState();
        return auth;
    }

    /**
     * Getter for get end of Loan
     * @return endAt
     */
    @NonNull
    public Long getEndAt() {
        return endAt;
    }

    /**
     * Getter for get State of Loan
     * @return state
     */
    @NonNull
    public MinimalState getState() {
        if (state.getClass() == StateName.class)
            state = new MinimalState(state.name);
        return (MinimalState) state;
    }

    /**
     * Getter for set State of Loan
     */
    public void setState(MinimalState state) {
        if (this.getState().changeArePermitted(state))
            this.state = state;
        else
            throw new IllegalArgumentException(String.format(
                "Change not allowed ! (Initial State : %s, Target State %s", getState().getAccessKey(),
                    state.getAccessKey()));
    }

    /**
     * Method for generate MinimalLoan related to this Loan
     * @return minimalLaon
     */
    @JsonIgnore
    @NonNull
    public MinimalLoan getMinimalLoan() {
        return new MinimalLoan(this);
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MinimalLoan)) return false;
        MinimalLoan loan = (MinimalLoan) o;
        return loan.hashCode() == hashCode();
    }

    /**
     * Method for get HashCode of this MinimalLoan
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).toHashCode();
    }

    /**
     * Getter for get id
     * @return id
     */
    @NonNull
    public String getId() {
        return id;
    }

    /**
     * Getter for get startedAt
     * @return startedAt
     */
    @NonNull
    public Long getStartedAt() {
        return startedAt;
    }

    /**
     * Getter for get gameDataIsMinified
     * @return gameDataIsMinified
     */
    @NonNull
    public boolean getGameDataIsMinified() {
        return gameDataIsMinified;
    }

    /**
     * Getter for get game
     * @return game
     */
    @NonNull
    public MinimalGame getGame() {
        return game;
    }

    /**
     * Method for Write MinimalLoan
     * @return minimalLoanReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("Loan related to this Game %s with id %s", game.toString(), id);
    }
}
