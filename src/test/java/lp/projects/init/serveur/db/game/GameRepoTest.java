package lp.projects.init.serveur.db.game;

import lp.projects.init.serveur.BaseAPITest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Class for test repo of Game
 */
public class GameRepoTest extends BaseAPITest {

    /**
     * Method for test import
     */
    @Test
    public void testImportGame() {
        cleanRepo();
        Assertions.assertEquals(gameRepository.count(), 0);
        gameRepository.save(games.get(0));
        Assertions.assertEquals(gameRepository.count(), 1);
        Assertions.assertEquals(gameRepository.findAll().get(0), games.get(0));
    }

    /**
     * Method for test import by own method
     */
    @Test
    public void testMethodOfDBRepo() {
        cleanRepo();
        Assertions.assertEquals(gameRepository.count(), 0);
        Assertions.assertTrue(insertGame(games.get(0)));
        Assertions.assertFalse(insertGame(games.get(0)));
        Assertions.assertEquals(gameRepository.count(), 1);
        Assertions.assertEquals(gameRepository.findAll().get(0), games.get(0));
        Assertions.assertTrue(removeGame(games.get(0)));
        Assertions.assertFalse(removeGame(games.get(0)));
        Assertions.assertEquals(gameRepository.count(), 0);
    }
}
