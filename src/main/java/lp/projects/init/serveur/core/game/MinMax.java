package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.db.BaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Transient;

/**
 * Class used for store object with minimum, maximum and eventually more
 * @param <T> Type of Object Stored
 */
public class MinMax<T extends Comparable<T>> extends BaseEntity {

    @Transient
    protected T min;
    @Transient
    protected T max;
    @Transient
    protected Boolean andMore;

    /**
     * Shit Constructor for MinMax Stored in DB
     * Used only by reflexion !
     */
    protected MinMax() {
        super();
    }

    /**
     * Basic Constructor for MinMax
     * @param min T represent minimum
     * @param max T represent maximum
     * @param andMore Boolean represent if used with minimum and more (If true maximum is null)
     */
    @JsonCreator
    @PersistenceConstructor
    public MinMax(@NonNull T min, @Nullable T max,
                    @NonNull Boolean andMore) {
        if (max != null && min.compareTo(max) > 0)
            throw new IllegalArgumentException(
                    "max must be superior or equals to min");
        this.min = min;
        this.max = !andMore ? max : null;
        this.andMore = andMore;
    }

    /**
     * Basic Constructor for MinMax
     * @param min T represent minimum
     * @param max T represent maximum
     */
    public MinMax(@NonNull T min, @Nullable T max) {
        this(min, max, false);
    }

    /**
     * Basic Constructor for MinMax
     * @param current T used for create MinMax with current value
     * @param andMore Boolean represent if used with minimum and more (If true maximum is null
     *                else minimum and maximum are equal to current)
     */
    public MinMax(@NonNull T current, @NonNull Boolean andMore) {
        this(current, current, andMore);
    }

    /**
     * Basic Constructor for MinMax
     * @param min T represent minimum
     */
    public MinMax(@NonNull T min) {
        this(min, true);
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof MinMax)) return false;
        MinMax minMax = (MinMax) o;
        return hashCode() == minMax.hashCode();
    }

    /**
     * Method for get HashCode of this MinMax
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(min).append(max).append(andMore).toHashCode();
    }

    /**
     * Getter for andMore of MinMax
     * @return andMore
     */
    @NonNull
    @Access(AccessType.PROPERTY)
    public Boolean getAndMore() {
        return andMore;
    }

    /**
     * Getter for max of MinMax
     * @return max
     */
    @Nullable
    public T getMax() {
        return max;
    }

    /**
     * Getter for min of MinMax
     * @return min
     */
    @NonNull
    public T getMin() {
        return min;
    }

    /**
     * Method for get representation of this MinMax
     * @return representation
     */
    @Schema(description = "Representation of this Object in simple String")
    @NonNull
    public String getRepresentation() {
        String represention = min.toString();
        if (max != null)
            represention = String.format("%s-%s", represention, max.toString());
        if (andMore)
            represention = String.format("%s +", represention);
        return represention;
    }

    /**
     * Method for Write MinMax
     * @return minMaxReadable
     */
    @Override
    @NonNull
    public String toString() {
        return getRepresentation();
    }

    /**
     * Setter for andMore of MinMaxInteger
     * @param andMore andMore of MinMax
     */
    private void setAndMore(Boolean andMore) {
        this.andMore = andMore;
    }
}
