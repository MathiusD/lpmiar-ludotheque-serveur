package lp.projects.init.serveur.core.oauth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Class represent Response of TokenRequest
 */
public class TokenResponse {

    public final String accessToken;
    public final String tokenType;
    public final Integer expiresIn;
    public final String refreshToken;
    public final Float createdAt;

    /**
     * Basic Constructor of TokenResponse
     * @param access_token String represent access granted
     * @param token_type String represent type of token
     * @param expires_in Integer represent end life of this token
     * @param refresh_token String represent refresh token
     * @param created_at Integer represent TimeStamp of creation of this token
     */
    @JsonCreator
    public TokenResponse(@JsonProperty("access_token") @NonNull String access_token,
                         @JsonProperty("token_type") @NonNull String token_type,
                         @JsonProperty("expires_in") @NonNull Integer expires_in,
                         @JsonProperty("refresh_token") @Nullable String refresh_token,
                         @JsonProperty("created_at") @Nullable Float created_at) {
        accessToken = access_token;
        tokenType = token_type;
        expiresIn = expires_in;
        refreshToken = refresh_token;
        createdAt = created_at;
    }
}
