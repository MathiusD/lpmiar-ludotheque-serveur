package lp.projects.init.serveur.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.lang.Nullable;

/**
 * Class represent InvalidToken Exception
 */
@Schema(description = "Error may occurred if given token is invalid")
public class InvalidTokenException extends UnauthorizedException {

    @Schema(description = "Token given for throw this Exception", example = "abcDEF478")
    public final String tokenGiven;

    /**
     * Basic Constructor for InvalidTokenException
     */
    public InvalidTokenException() {
        this(null);
    }

    /**
     * Basic Constructor for UnauthorizedException
     * @param tokenGiven String represent tokenGiven
     */
    @JsonCreator
    public InvalidTokenException(@Nullable String tokenGiven) {
        super("You must give a valid token in order to access this resource.");
        this.tokenGiven = tokenGiven;
    }
}
