package lp.projects.init.serveur.core.oauth;

import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.user.MinimalUser;
import org.springframework.lang.NonNull;

/**
 * Class represent result of OAuth Link to user
 */
@Schema(description = "Result of OAuth Link")
public class OAuthLinkResult {

    @Schema(description = "OAuth provider related")
    public final OAuthProvider providerRelated;
    @Schema(description = "User related")
    public final MinimalUser userRelated;

    /**
     * Basic Constructor of OAuthLinkResult
     * @param provider provider related
     * @param user user related
     */
    public OAuthLinkResult(@NonNull OAuthProvider provider, @NonNull MinimalUser user) {
        userRelated = user;
        providerRelated = provider;
    }
}
