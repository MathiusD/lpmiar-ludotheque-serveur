package lp.projects.init.serveur.db.game;

import lp.projects.init.serveur.core.game.MinimalGame;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface represent Repo for MinimalGame
 */
public interface MinimalGameRepo extends JpaRepository<MinimalGame, String> {
}
