package lp.projects.init.serveur.core.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.auth.MinimalRole;
import lp.projects.init.serveur.core.auth.Role;
import lp.projects.init.serveur.core.loan.Loan;
import lp.projects.init.serveur.db.BaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import static javax.persistence.InheritanceType.JOINED;

/**
 * Class represent Base informations of this User of this library
 */
@Entity
@Inheritance(strategy = JOINED)
@DiscriminatorValue("MIN_USER")
@Schema(description = "Base Information of User of this Library")
public class MinimalUser extends BaseEntity {

    @Id
    @Column(name = "user_id")
    @Schema(description = "Id for identify this User", example = "TRUCK")
    protected String id;
    @Column(name = "user_lastName")
    @Schema(description = "Last Name of this User", example = "Lanoix")
    protected String lastName;
    @Column(name = "user_firstName")
    @Schema(description = "First Name of this User", example = "Arnaud")
    protected String firstName;
    @Column(name = "user_email")
    @Schema(description = "Email of this User", example = "gandalf.le.vert@mail.org")
    protected String email;
    @Column(name = "user_nickname")
    @Schema(description = "NickName of this User", example = "DarkSasuke")
    protected String nickName;
    @ManyToOne(optional = false, targetEntity = MinimalRole.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "name_role", nullable = false)
    @Schema(description = "Role of this User")
    protected MinimalRole role;

    public static final MinimalRole DEFAULT_ROLE = Role.USER;

    /**
     * Basic Constructor of MinimalUser
     * @param id String represent id of User
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     * @param role MinimalRole represent role of User
     */
    @JsonCreator
    @PersistenceConstructor
    public MinimalUser(@NonNull String id, @Nullable String lastName, @Nullable String firstName,
                       @NonNull String email, @NonNull String nickName, @NonNull MinimalRole role) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.nickName = nickName;
        this.role = role.getMinimalRole();
    }

    /**
     * Basic Constructor of MinimalUser
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     * @param role MinimalRole represent role of User
     */
    public MinimalUser(@Nullable String lastName, @Nullable String firstName,
                       @NonNull String email, @NonNull String nickName, @NonNull MinimalRole role) {
        this(UUID.randomUUID().toString(), lastName, firstName, email, nickName, role);
    }

    /**
     * Basic Constructor of MinimalUser
     * @param id String represent id of User
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     */
    public MinimalUser(@NonNull String id, @Nullable String lastName, @Nullable String firstName,
                       @NonNull String email, @NonNull String nickName) {
        this(id, lastName, firstName, email, nickName, DEFAULT_ROLE);
    }

    /**
     * Basic Constructor of MinimalUser
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     */
    public MinimalUser(@Nullable String lastName, @Nullable String firstName,
                       @NonNull String email, @NonNull String nickName) {
        this(lastName, firstName, email, nickName, DEFAULT_ROLE);
    }

    /**
     * Constructor of MinimalUser by MinimalUser or child
     * @param otherUser MinimalUser used for extract informations
     */
    public MinimalUser(@NonNull MinimalUser otherUser) {
        this(otherUser.getId(), otherUser.getLastName(), otherUser.getFirstName(), otherUser.getEmail(),
                otherUser.getNickName(), otherUser.getRole());
    }

    /**
     * Constructor of MinimalUser by Loan or child
     * @param loan Loan used for extract informations
     */
    public MinimalUser(@NonNull Loan loan) {
        this(loan.getUser());
    }

    /**
     * Shit Constructor for MinimalUser Stored in DB
     * Used only by reflexion !
     */
    protected MinimalUser() {
        super();
    }

    /**
     * Method for extract MinimalUser from Many Loans
     * @param loans Many Loans used for extract informations of MinimalUser
     */
    @NonNull
    public static MinimalUser extractMinimalUserFrom(@NonNull Collection<? extends Loan> loans) {
        for (Loan loan : loans)
            if(loan != null)
                return loan.getUser();
        throw new IllegalArgumentException("This method required at least one Loan");
    }

    /**
     * Method for extract MinimalUser from Collection of Loans
     * @param loans Collection of Loans represent Loans of this User
     */
    @NonNull
    public static MinimalUser extractMinimalUserFrom(@NonNull Loan... loans) {
        return extractMinimalUserFrom(Arrays.asList(loans));
    }

    /**
     * Method for Write User
     * @return userReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("User %s %s (%s) with id : %s", firstName, lastName, nickName, id);
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MinimalUser)) return false;
        MinimalUser user = (MinimalUser) o;
        return user.hashCode() == hashCode();
    }

    /**
     * Method for get HashCode of this MinimalUser
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).toHashCode();
    }

    /**
     * Method for generate MinimalUser related to this User
     * @return minimalUser
     */
    @JsonIgnore
    @NonNull
    public MinimalUser getMinimalUser() {
        return new MinimalUser(this);
    }

    /**
     * Getter for get id
     * @return id
     */
    @NonNull
    public String getId() {
        return id;
    }

    /**
     * Getter for get lastName
     * @return lastName
     */
    @Nullable
    public String getLastName() {
        return lastName;
    }

    /**
     * Getter for get firstName
     * @return firstName
     */
    @Nullable
    public String getFirstName() {
        return firstName;
    }

    /**
     * Getter for get email
     * @return email
     */
    @NonNull
    public String getEmail() {
        return email;
    }

    /**
     * Getter for get nickName
     * @return nickName
     */
    @NonNull
    public String getNickName() {
        return nickName;
    }

    /**
     * Getter for get role
     * @return role
     */
    @NonNull
    public MinimalRole getRole() {
        return new Role(role);
    }
}
