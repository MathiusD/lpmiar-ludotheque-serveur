package lp.projects.init.serveur.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lp.projects.init.serveur.DBRepo;
import lp.projects.init.serveur.core.auth.Token;
import lp.projects.init.serveur.core.game.GameSchema;
import lp.projects.init.serveur.core.loan.*;
import lp.projects.init.serveur.core.user.*;
import lp.projects.init.serveur.responses.*;
import lp.projects.init.serveur.responses.Error;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static lp.projects.init.serveur.core.game.GameSchema.ALL_SCHEMA;

/**
 * Class represent Controller of Library
 */
@RestController
public class Library extends DBRepo {

    @Autowired
    protected Users users;
    @Autowired
    protected Loans loans;

    /**
     * For get loans of all users
     * @param token String represent Token for identify your account
     * @param state String represent State of loans you are looking for
     * @param gameSchema GameSchema represent Game Schema for manage amount of data received
     * @param endAt Long represent Timestamp of endAt of a loan you are looking for
     * @param startedAt Long represent Timestamp of startedAt end of a loan you are looking for
     * @param gameId Integer represent Id related to Game of loans you are looking for
     * @param since Long represent Timestamp for extract only loan created since this timestamp
     * @param before Long represent Timestamp for extract only loan created before this timestamp
     * @return libraryData
     */
    @CrossOrigin
    @Operation(summary = "Get all Loans of User")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Loans of Users",
            content = @Content(mediaType = "application/json",
                array = @ArraySchema(uniqueItems = true,
                    schema = @Schema(implementation = MinimalUserWithLoans.class)
                )
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        )
    })
    @RequestMapping(value = "/library/loans", method = RequestMethod.GET)
    @NonNull
    public List<MinimalUserWithLoans> libraryLoans(@Parameter(description = "Token for identify your account",
                                                        example = "abcEFG145") @RequestHeader(required = false)
                                                   @Nullable String token, @Parameter(
                                                        description = "State of loans you are looking for",
                                                        example = "requested",
                                                        examples = {
                                                            @ExampleObject(value = "requested",
                                                                description = "Game related to this Loan are rendered"),
                                                            @ExampleObject(value = "at_home",
                                                                description = "Game related to this Loan are at home"),
                                                            @ExampleObject(value = "waiting",
                                                                description = "Game related to this Loan are at library and user can pick her game"),
                                                            @ExampleObject(value = "requested",
                                                                description = "Game related to this Loan are requested only")
                                                        })
                                                   @RequestParam(required = false) @Nullable String state,
                                                   @Parameter(
                                                        description = "Game Schema for manage amount of data received",
                                                        example = "fullGame") @RequestParam(required = false)
                                                   @Nullable String gameSchema, @RequestParam(required = false)
                                                   @Parameter(description = "Timestamp of endAt of a loan you are looking for",
                                                        example = "1577836800") @Nullable Long endAt,
                                                   @RequestParam(required = false) @Parameter(
                                                         description = "Timestamp of startedAt end of a loan you are looking for",
                                                         example = "1610534377339") @Nullable Long startedAt,
                                                   @Parameter(
                                                         description = "Id related to Game of loans you are looking for",
                                                         example = "T111") @RequestParam(required = false)
                                                   @Nullable String gameId, @RequestParam(required = false)
                                                   @Parameter(
                                                         description = "Timestamp for extract only loan created since this timestamp",
                                                         example = "1577836800")
                                                   @Nullable Long since, @RequestParam(required = false)
                                                   @Parameter(
                                                         description = "Timestamp for extract only loan created before this timestamp",
                                                         example = "1577836800")
                                                   @Nullable Long before ) throws AccessDeniedException, InvalidTokenException {
        identifyToken(token, true, "/library/loans");
        GameSchema schemaUsed = GameSchema.MINIMAL_DATA;
        for (GameSchema schema : ALL_SCHEMA)
            if (schema.name.equalsIgnoreCase(gameSchema))
                schemaUsed = schema;
        List<MinimalUserWithLoans> dataLibrary = new ArrayList<>();
        Map<String, List<Loan>> dataLoans = new HashMap<>();
        for (Loan loan: loanRepository.findAll())
            if ((state == null || loan.getState().getAccessKey().equalsIgnoreCase(state)) &&
                (endAt == null || loan.getEndAt().equals(endAt)) &&
                (startedAt == null || loan.getStartedAt().equals(startedAt)) &&
                (gameId == null || loan.getGame().getId().equalsIgnoreCase(gameId))&&
                (since == null || since <= loan.getStartedAt()) &&
                (before==null|| before >= loan.getStartedAt())) {
                if (!dataLoans.containsKey(loan.getUser().getId()))
                    dataLoans.put(loan.getUser().getId(), new ArrayList<>());
                List<Loan> temp = dataLoans.get(loan.getUser().getId());
                temp.add(new Loan(loan, gameRepository.getOne(loan.getGame().getId()), schemaUsed));
                dataLoans.put(loan.getUser().getId(), temp);
            }
        dataLoans.forEach((s, loans) -> {
            MinimalUser usr = MinimalUser.extractMinimalUserFrom(loans);
            dataLibrary.add(new MinimalUserWithLoans(usr, loans));
        });
        return dataLibrary;
    }

    /**
     * For get Loan by id
     * @param token String represent Token for identify your account
     * @param id String represent id of Loan research
     * @return loan
     */
    @CrossOrigin
    @Operation(summary = "Get a Loan by its id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found the Loan",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Loan.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        ),
        @ApiResponse(responseCode = "404", description = "Loan not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = NotFoundException.class)
            )
        )
    })
    @RequestMapping(value = "/library/loans/{id}", method = RequestMethod.GET)
    @NonNull
    public Loan libraryLoan(@Parameter(description = "Token for identify your account", example = "abcEFG145")
                                       @RequestHeader(required = false) @Nullable String token,
                                   @Parameter(description = "Id of loan you are looking for", example = "ABC")
                                    @PathVariable(value="id") @NonNull String id) throws Error {
        return loans.loan(token, id);
    }

    /**
     * For update Loan by id
     * @param token String represent Token for identify your account
     * @param id String represent id of Loan research
     * @return loan
     */
    @CrossOrigin
    @Operation(summary = "For update Loan by its id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Result of Loan update",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = LoanUpdateResult.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Bad Request",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = BadRequestException.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class)
            )
        ),
        @ApiResponse(responseCode = "404", description = "Loan not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = NotFoundException.class)
            )
        )
    })
    @RequestMapping(value = "/library/loans/{id}", method = RequestMethod.POST)
    @NonNull
    public LoanUpdateResult updateLibraryLoan(@Parameter(description = "Token for identify your account",
                                       example = "abcEFG145") @RequestHeader(required = false) @Nullable String token,
                                       @Parameter(description = "Id of loan you are looking for", example = "ABC")
                                       @PathVariable(value="id") @NonNull String id, @RequestBody LoanUpdate update)
                                       throws Error {
        identifyToken(token, true, "/library/loans/%s");
        return loans.updateLoan(token, id, update);
    }

    /**
     * For delete Loan by id
     * @param token String represent Token for identify your account
     * @param id String represent id of Loan research
     */
    @CrossOrigin
    @Operation(summary = "For delete Loan by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Loan Deleted"),
            @ApiResponse(responseCode = "401", description = "Not Connected",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = InvalidTokenException.class)
                    )
            ),
            @ApiResponse(responseCode = "403", description = "Not Allowed",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AccessDeniedException.class)
                    )
            ),
            @ApiResponse(responseCode = "404", description = "Loan not found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = NotFoundException.class)
                    )
            )
    })
    @RequestMapping(value = "/library/loans/{id}", method = RequestMethod.DELETE)
    @NonNull
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteLoan(@Parameter(description = "Token for identify your account",
                           example = "abcEFG145") @RequestHeader(required = false) @Nullable String token,
                           @Parameter(description = "Id of loan you are looking for", example = "ABC")
                           @PathVariable(value="id") @NonNull String id) throws Error {
        loans.deleteLoan(token, id);
    }

    /**
     * For get all users
     * @param token String represent Token for identify your account
     * @param firstName String represent firstName of user you are looking for
     * @param lastName String represent lastName of user you are looking for
     * @param schema String represent Schema for manage amount of data received
     * @param city String represent city of user you are looking for
     * @param role String represent role of user you are looking for
     * @return users
     */
    @CrossOrigin
    @Operation(summary = "Get all Users")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Users of Library",
            content = @Content(mediaType = "application/json",
                array = @ArraySchema(uniqueItems = true,
                    schema = @Schema(implementation = MinimalUser.class)
                )
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        )
    })
    @RequestMapping(value = "/library/users", method = RequestMethod.GET)
    @NonNull
    public List<MinimalUser> libraryUsers(@Parameter(description = "Token for identify your account",
                                              example = "abcEFG145") @RequestHeader(required = false)
                                          @Nullable String token, @RequestParam(required = false)
                                          @Parameter(description = "Firstname of user you are looking for",
                                              example = "bob") @Nullable String firstName,
                                          @Parameter(description = "LastName of user you are looking for",
                                              example = "le bricoleur") @RequestParam(required = false)
                                          @Nullable String lastName, @Parameter(
                                              description = "Schema for manage amount of data received",
                                              example = "fullUser",
                                              examples = {
                                                  @ExampleObject(value = "fullUser",
                                                      description = "For display all data related to this User"),
                                                  @ExampleObject(value = "minimalUser",
                                                      description = "For display only id, lastname, firstname and role related to this User")
                                              }
                                          ) @RequestParam(required = false) @Nullable String schema,
                                          @Parameter(description = "City of user you are looking for",
                                              example = "Nantes") @RequestParam(required = false)
                                          @Nullable String city, @RequestParam(required = false)
                                          @Parameter(description = "Role of user you are looking for",
                                              example = "manager") @Nullable String role) throws AccessDeniedException, InvalidTokenException {
        identifyToken(token, true, "/library/users");
        UserSchema schemaUsed = UserSchema.MINIMAL_DATA;
        for (UserSchema userSchema : UserSchema.ALL_SCHEMA)
            if (userSchema.name.equalsIgnoreCase(schema))
                schemaUsed = userSchema;
        List<User> ListUser = new ArrayList<>(userRepository.findAll());
        ListUser.removeIf(user ->
            (lastName != null && !user.getLastName().toLowerCase().startsWith(lastName.toLowerCase())) ||
            (firstName != null && !user.getFirstName().toLowerCase().startsWith(firstName.toLowerCase())) ||
            (city != null && (user.getCity() == null || !user.getCity().equalsIgnoreCase(city))) ||
            (role != null && !user.getRole().getName().equalsIgnoreCase(role)));
        return schemaUsed.matchingUserWithThisSchema(ListUser);
    }

    /**
     * For get User by id
     * @param token String represent Token for identify your account
     * @param id String represent id of User research
     * @return user
     */
    @CrossOrigin
    @Operation(summary = "Get a User by its id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found the User",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = User.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        ),
        @ApiResponse(responseCode = "404", description = "User not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Error.class)
            )
        )
    })
    @RequestMapping(value = "/library/users/{id}", method = RequestMethod.GET)
    @NonNull
    public User libraryUser(@Parameter(description = "Token for identify your account", example = "abcEFG145")
                                       @RequestHeader(required = false) @Nullable String token,
                                   @Parameter(description = "Id of user you are looking for", example = "T256")
                                @PathVariable(value="id") @NonNull String id) throws Error {
        return users.user(token, id);
    }


    /**
     * For get loans of User by user id
     * @param token String represent Token for identify your account
     * @param id String represent id of User research
     * @param state String represent State of loans you are looking for
     * @param gameSchema GameSchema represent Game Schema for manage amount of data received
     * @param endAt Long represent Timestamp of endAt of a loan you are looking for
     * @param startedAt Long represent Timestamp of startedAt end of a loan you are looking for
     * @param gameId Integer represent Id related to Game of loans you are looking for
     * @param since Long represent Timestamp for extract only loan created since this timestamp
     * @param before Long represent Timestamp for extract only loan created before this timestamp
     * @return user
     */
    @CrossOrigin
    @Operation(summary = "Get a loans of User by its User Id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found the User and get their loans",
            content = @Content(mediaType = "application/json",
                array = @ArraySchema(
                    uniqueItems = true, schema = @Schema(implementation = MinimalLoan.class)
                )
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        ),
        @ApiResponse(responseCode = "404", description = "User not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Error.class)
            )
        )
    })
    @RequestMapping(value = "/library/users/{id}/loans", method = RequestMethod.GET)
    @NonNull
    public List<MinimalLoan> libraryLoanOfUser(@Parameter(description = "Token for identify your account",
                                                    example = "abcEFG145") @RequestHeader(required = false)
                                               @Nullable String token, @Parameter(
                                                    description = "Id of user you are looking for",
                                                    example = "T256")
                                               @PathVariable(value="id") @NonNull String id,
                                               @Parameter(
                                                    description = "State of loans you are looking for",
                                                    example = "requested",
                                                    examples = {
                                                        @ExampleObject(value = "requested",
                                                            description = "Game related to this Loan are rendered"),
                                                        @ExampleObject(value = "at_home",
                                                            description = "Game related to this Loan are at home"),
                                                        @ExampleObject(value = "waiting",
                                                            description = "Game related to this Loan are at library and user can pick her game"),
                                                        @ExampleObject(value = "requested",
                                                            description = "Game related to this Loan are requested only")
                                               })
                                               @RequestParam(required = false) @Nullable String state,
                                               @Parameter(
                                                   description = "Game Schema for manage amount of data received",
                                                   example = "fullGame") @RequestParam(required = false)
                                               @Nullable String gameSchema, @RequestParam(required = false)
                                               @Parameter(description = "Timestamp of endAt of a loan you are looking for",
                                                           example = "1577836800") @Nullable Long endAt,
                                               @RequestParam(required = false) @Parameter(
                                                   description = "Timestamp of startedAt end of a loan you are looking for",
                                                   example = "1610534377339") @Nullable Long startedAt,
                                               @Parameter(
                                                   description = "Id related to Game of loans you are looking for",
                                                   example = "T111") @RequestParam(required = false)
                                               @Nullable String gameId, @RequestParam(required = false)
                                               @Parameter(
                                                   description = "Timestamp for extract only loan created since this timestamp",
                                                   example = "1577836800")
                                               @Nullable Long since, @RequestParam(required = false)
                                               @Parameter(
                                                   description = "Timestamp for extract only loan created before this timestamp",
                                                   example = "1577836800")
                                               @Nullable Long before) throws Error {
        return users.loans(token, id, state, gameSchema, endAt, startedAt, gameId, since, before);
    }

    /**
     * For get OAuth related to this User
     * @param token String represent Token for identify your account
     * @param id String represent id of User research
     * @return user
     */
    @CrossOrigin
    @Operation(summary = "For get OAuth related to this User")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OAuth related to this User",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(uniqueItems = true,
                                    schema = @Schema(implementation = UserOAuth.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not Connected",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = InvalidTokenException.class)
                    )
            ),
            @ApiResponse(responseCode = "403", description = "Not Allowed",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AccessDeniedException.class))
            ),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Error.class)
                    )
            )
    })
    @RequestMapping(value = "/library/users/{id}/oauth", method = RequestMethod.GET)
    @NonNull
    public List<UserOAuth> userOAuth(@Parameter(description = "Token for identify your account",
                                        example = "abcEFG145") @RequestHeader(required = false)
                                     @Nullable String token,
                                     @Parameter(description = "Id of user you are looking for",
                                             example = "T256") @PathVariable(value="id")
                                     @NonNull String id) throws Error {
        return users.userOAuth(token, id);
    }
}
