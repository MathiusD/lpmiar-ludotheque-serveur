package lp.projects.init.serveur.core.loan;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.game.MinimalGame;
import lp.projects.init.serveur.core.user.MinimalUser;
import org.springframework.lang.NonNull;

/**
 * Class represent Loan with Game Data Hidden.
 */
@JsonIgnoreProperties({"game", "gameDataIsMinified"})
@Schema(description = "Loan without game data.")
public class ReduceLoan extends Loan {
    /**
     * Shit Constructor for ReduceLoan Stored in DB
     * Used only by reflexion !
     */
    protected ReduceLoan() {
        super();
    }

    /**
     * Basic Constructor for ReduceLoan used only by JsonCreator
     * @param id String represent id of Loan
     * @param user MinimalUser represent who had this Loan
     * @param startedAt Long represent timestamp of start of this Loan
     * @param endAt Long represent timestamp of start of this Loan
     * @param state MinimalState represent State of this Loan
     */
    @JsonCreator
    protected ReduceLoan(@NonNull String id, @NonNull MinimalUser user, @NonNull Long startedAt,
                         @NonNull Long endAt, @NonNull MinimalState state) {
        super(id, new MinimalGame("Example"), user, startedAt, endAt, state, true);
    }
    /**
     * Constructor of ReduceLoan by Loan or child
     * @param otherLoan Loan used for extract informations
     */
    public ReduceLoan(@NonNull Loan otherLoan) {
        super(otherLoan);
    }
}
