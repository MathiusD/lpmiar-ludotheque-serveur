package lp.projects.init.serveur.core.user;

import lp.projects.init.serveur.core.auth.Role;
import org.junit.jupiter.api.Test;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test GameSchema
 */
public class UserSchemaTest extends UserTest {

    /**
     * Class for testing exception threw by UserSchema if constructor by copy is missing
     */
    public final static class testinguserInvalidConstructor extends MinimalUser {

        /**
         * Base Constructor for testingUserInvalid
         * @param id String represent id of User
         * @param lastName String represent lastName of User
         * @param firstName String represent lastName of User
         * @param email String represent email of User
         * @param nickName String represent nickName of User
         */
        public testinguserInvalidConstructor(@NonNull String id, @Nullable String lastName,
                                             @Nullable String firstName, @NonNull String email,
                                             @NonNull String nickName) {
            super(id, lastName, firstName, email, nickName);
        }
    }

    public final static UserSchema SchemaForInvalidConstructor = new UserSchema("Schema For Invalid Constructor",
            testinguserInvalidConstructor.class);

    /**
     * For test Instantiation of UserSchema
     */
    @Test
    public void testInstantiation() {
        assertEquals(new UserSchema(UserSchema.FULL_DATA.name), UserSchema.FULL_DATA);
        assertEquals(new UserSchema(UserSchema.FULL_DATA.name, UserSchema.FULL_DATA.description),
                UserSchema.FULL_DATA);
        assertThrows(IllegalArgumentException.class, () -> new UserSchema("truc"),
                "This Schema don't match with any existing Schema");
        assertThrows(IllegalArgumentException.class, () -> new UserSchema(UserSchema.FULL_DATA.name, "truc"),
                "This Schema don't match with any existing Schema");

    }
    /**
     * For test method matchinguserWithThisSchema of UserSchema
     */
    @Test
    public void testMatchingGameWithThisSchemaOfGameSchema() {
        User firstExample = new User("T256", "Le Bricoleur", "Bob",
                "bob@mail.org", "Bob!", Role.MANAGER,
                new GregorianCalendar(2000, Calendar.OCTOBER, 27)
                        .getTime(), "107 bis rue truc", "Patelin Paumé");
        assertUsersAreStrictlyEquals(UserSchema.FULL_DATA.matchingUserWithThisSchema(firstExample), firstExample);
        assertEquals(UserSchema.FULL_DATA.matchingUserWithThisSchema(firstExample).getClass(),
                UserSchema.FULL_DATA.userClass);
        assertUsersAreStrictlyEquals(UserSchema.MINIMAL_DATA.matchingUserWithThisSchema(firstExample), firstExample);
        assertEquals(UserSchema.MINIMAL_DATA.matchingUserWithThisSchema(firstExample).getClass(),
                UserSchema.MINIMAL_DATA.userClass);
        assertThrows(IllegalArgumentException.class, () ->
                        SchemaForInvalidConstructor.matchingUserWithThisSchema(firstExample),
                "Class in this object doesn't have constructor match to following signature public <? extends MinimalUser>(MinimalUser)"
        );
    }

}
