package lp.projects.init.serveur.core.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Class represent user data schemas
 */
@Schema(description = "Object To represent the different user data schemas")
public class UserSchema {

    @Schema(description = "Name of this Schema", example = "fullUser")
    public final String name;
    @Schema(description = "Explication for why or for what Schema exist",
            example = "For display full data of User")
    public final String description;
    @JsonIgnore
    public final Class<? extends MinimalUser> userClass;

    /**
     * Basic constructor for User Schema
     * @param name String represent name of this Schema
     * @param description String represent why this schema exist
     * @param userClass Class represent for what User schema exist
     */
    public UserSchema(@NonNull String name, @Nullable String description,
                      @NonNull Class<? extends MinimalUser> userClass) {
        this.name = name;
        this.description = description;
        this.userClass = userClass;
    }

    /**
     * Basic constructor for User Schema
     * @param name String represent name of this Schema
     */
    public UserSchema(@NonNull String name) {
        this(name, (String) null);
    }
    /**
     * Basic constructor for User Schema
     * @param name String represent name of this Schema
     * @param description String represent why this schema exist
     */
    @JsonCreator
    public UserSchema(@NonNull String name, @Nullable String description) {
        UserSchema foundSchema = null;
        for (UserSchema schema: ALL_SCHEMA)
            if (schema.name.equalsIgnoreCase(name) && foundSchema == null &&
                    (description == null || description.equalsIgnoreCase(schema.description)))
                foundSchema = schema;
        if (foundSchema == null)
            throw new IllegalArgumentException("This Schema don't match with any existing Schema");
        this.name = foundSchema.name;
        this.description = foundSchema.description;
        this.userClass = foundSchema.userClass;
    }

    /**
     * Basic constructor for User Schema
     * @param name String represent name of this Schema
     * @param userClass Class represent for what User schema exist
     */
    public UserSchema(@NonNull String name, @NonNull Class<? extends MinimalUser> userClass) {
        this(name, null, userClass);
    }

    /**
     * Method for transform User to User related to this Schema
     * @param user User represent Game used for convert
     * @return userMatchingWithThisSchema
     */
    @NonNull
    public MinimalUser matchingUserWithThisSchema(@NonNull User user) {
        try {
            Constructor<? extends MinimalUser> gameConstructor;
            try {
                gameConstructor = userClass.getDeclaredConstructor(userClass);
            } catch (NoSuchMethodException e) {
                gameConstructor = userClass.getDeclaredConstructor(MinimalUser.class);
            }
            return gameConstructor.newInstance(user);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException
                | InvocationTargetException e) {
            throw new IllegalArgumentException("Class in this object doesn't have constructor match to following signature public <? extends MinimalUser>(MinimalUser)");
        }
    }

    /**
     * Method for transform Collection of User to Users related to this Schema
     * @param users Collection of User represent users used for convert
     * @return usersMatchingWithThisSchema
     */
    @NonNull
    public List<MinimalUser> matchingUserWithThisSchema(@NonNull Collection<User> users) {
        List<MinimalUser> usersMatching = new ArrayList<>();
        for(User user: users)
            usersMatching.add(matchingUserWithThisSchema(user));
        return usersMatching;
    }

    /**
     * Method for transform Many Users to Users related to this Schema
     * @param users Many Users represent users used for convert
     * @return gameMatchingWithThisSchema
     */
    @NonNull
    public List<MinimalUser> matchingUserWithThisSchema(@NonNull User... users) {
        return matchingUserWithThisSchema(Arrays.asList(users));
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof UserSchema)) return false;
        UserSchema userSchema = (UserSchema) o;
        return hashCode() == userSchema.hashCode();
    }

    /**
     * Method for get HashCode of this UserSchema
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(name).append(userClass).toHashCode();
    }

    /**
     * Method for Write UserSchema
     * @return userSchemaReadable
     */
    @Override
    @NonNull
    public String toString(){
        return String.format("UserSchema %s for target class %s", name, userClass.getSimpleName());
    }

    public static final UserSchema FULL_DATA = new UserSchema("fullUser",
            "For display all data related to this User", User.class);
    public static final UserSchema MINIMAL_DATA = new UserSchema("minimalUser",
            "For display only id, lastname, firstname and role related to this User", MinimalUser.class);
    public static final List<UserSchema> ALL_SCHEMA;

    static {
        List<UserSchema> temp = new ArrayList<>();
        temp.add(FULL_DATA);
        temp.add(MINIMAL_DATA);
        ALL_SCHEMA = temp;
    }
}
