package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.loan.Loan;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Class represent Result of borrow request
 */
@Schema(description = "Result of Borrow Request")
public class BorrowResult {

    @Schema(description = "Loan related to this borrow request if accepted")
    public final Loan loanRelated;
    @Schema(description = "Message related to this request", example = "Game is Unavailable")
    public final String message;

    /**
     * Basic constructor of BorrowResult
     * @param loanRelated Loan represent Loan related to this borrow if accepted
     * @param message String represent message related to this request
     */
    @JsonCreator
    public BorrowResult(@Nullable Loan loanRelated, @NonNull String message) {
        this.loanRelated = loanRelated;
        this.message = message;
    }

    /**
     * Basic constructor of BorrowResult with default message
     * @param loanRelated Loan represent Loan related to this borrow if accepted
     */
    public BorrowResult(@NonNull Loan loanRelated) {
        this(loanRelated, String.format("The request was successful concluded. (Loan related : %s)",
            loanRelated.getId()));
    }

    /**
     * Basic constructor of BorrowResult with empty Loan
     * @param message String represent message related to this request
     */
    public BorrowResult(@NonNull String message) {
        this(null, message);
    }

    /**
     * Basic constructor of BorrowResult with empty Loan and default message
     */
    public BorrowResult() {
        this("The request could not be successfully concluded.");
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof BorrowResult)) return false;
        BorrowResult borrowResult = (BorrowResult) o;
        return hashCode() == borrowResult.hashCode();
    }

    /**
     * Method for get HashCode of this BorrowResult
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(message).append(loanRelated).toHashCode();
    }

    /**
     * Method for Write BorrowResult
     * @return borrowResultReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("Borrow Result related to this Loan : %s", loanRelated.toString());
    }
}
