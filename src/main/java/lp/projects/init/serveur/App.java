package lp.projects.init.serveur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.lang.NonNull;
import org.springframework.web.filter.ForwardedHeaderFilter;

/**
 * Class represent main app of API
 */
@SpringBootApplication
@PropertySources(value = {@PropertySource("classpath:catalina.properties")})
public class App extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    /**
     * For conserve protocol in header (For spring doc example is usefull)
     * @return forwardedHeaders
     */
    @Bean
    @NonNull
    ForwardedHeaderFilter forwardedHeaderFilter() {
        return new ForwardedHeaderFilter();
    }
}
