package lp.projects.init.serveur.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lp.projects.init.serveur.DBRepo;
import lp.projects.init.serveur.core.loan.MinimalLoan;
import lp.projects.init.serveur.core.user.User;
import lp.projects.init.serveur.core.user.UserOAuth;
import lp.projects.init.serveur.responses.AccessDeniedException;
import lp.projects.init.serveur.responses.Error;
import lp.projects.init.serveur.responses.InvalidTokenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Class represent Controller of My
 */
@RestController
public class My extends DBRepo {

    @Autowired
    protected Users users;

    /**
     * For get User connected
     * @param token String represent Token for identify your account
     * @return user
     */
    @CrossOrigin
    @Operation(summary = "Get your Profile")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Your Profile",
            content = @Content(mediaType = "application/json",
            schema = @Schema(implementation = User.class))
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
            schema = @Schema(implementation = InvalidTokenException.class))
        )
    })
    @RequestMapping(value = "/my", method = RequestMethod.GET)
    @NonNull
    public User myUser(@Parameter(description = "Token for identify your account", example = "abcEFG145")
                                  @RequestHeader(required = false) @Nullable String token) throws Error {
        return users.user(token, users.identifyToken(token).getUser().getId());
    }

    /**
     * For get loans of my User
     * @param token String represent Token for identify your account
     * @param state String represent State of loans you are looking for
     * @param gameSchema GameSchema represent Game Schema for manage amount of data received
     * @param endAt Long represent Timestamp of endAt of a loan you are looking for
     * @param startedAt Long represent Timestamp of startedAt end of a loan you are looking for
     * @param gameId Integer represent Id related to Game of loans you are looking for
     * @param since Long represent Timestamp for extract only loan created since this timestamp
     * @param before Long represent Timestamp for extract only loan created before this timestamp
     * @return loans
     */
    @CrossOrigin
    @Operation(summary = "Get your loans")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Your Loans",
            content = @Content(mediaType = "application/json",
                array = @ArraySchema(uniqueItems = true, schema = @Schema(implementation = MinimalLoan.class))
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        )
    })
    @RequestMapping(value = "/my/loans", method = RequestMethod.GET)
    @NonNull
    public List<MinimalLoan> myLoans(@Parameter(description = "Token for identify your account",
                                            example = "abcEFG145") @RequestHeader(required = false)
                                     @Nullable String token, @Parameter(
                                            description = "State of loans you are looking for",
                                            example = "requested",
                                            examples = {
                                                @ExampleObject(value = "requested",
                                                      description = "Game related to this Loan are rendered"),
                                                @ExampleObject(value = "at_home",
                                                      description = "Game related to this Loan are at home"),
                                                @ExampleObject(value = "waiting",
                                                      description = "Game related to this Loan are at library and user can pick her game"),
                                                @ExampleObject(value = "requested",
                                                      description = "Game related to this Loan are requested only")
                                            }
                                     ) @RequestParam(required = false) @Nullable String state,
                                     @Parameter(description = "Game Schema for manage amount of data received",
                                            example = "fullGame") @RequestParam(required = false) @Nullable
                                     String gameSchema, @RequestParam(required = false)
                                     @Parameter(description = "Timestamp of endAt of a loan you are looking for",
                                             example = "1577836800") @Nullable Long endAt,
                                     @RequestParam(required = false) @Parameter(
                                             description = "Timestamp of startedAt end of a loan you are looking for",
                                             example = "1610534377339") @Nullable Long startedAt,
                                     @Parameter(
                                             description = "Id related to Game of loans you are looking for",
                                             example = "T111") @RequestParam(required = false)
                                     @Nullable String gameId, @RequestParam(required = false)
                                     @Parameter(
                                             description = "Timestamp for extract only loan created since this timestamp",
                                             example = "1577836800")
                                     @Nullable Long since, @RequestParam(required = false)
                                     @Parameter(
                                             description = "Timestamp for extract only loan created before this timestamp",
                                             example = "1577836800")
                                     @Nullable Long before) throws Error {
        return users.loans(token, identifyToken(token).getUser().getId(), state, gameSchema,
            endAt, startedAt, gameId,since,before);
    }

    /**
     * For get OAuth related to my User
     * @param token String represent Token for identify your account
     * @return user
     */
    @CrossOrigin
    @Operation(summary = "For get OAuth related to my User")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OAuth related to my User",
                    content = @Content(mediaType = "application/json",
                            array = @ArraySchema(uniqueItems = true,
                                    schema = @Schema(implementation = UserOAuth.class)
                            )
                    )
            ),
            @ApiResponse(responseCode = "401", description = "Not Connected",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = InvalidTokenException.class)
                    )
            ),
            @ApiResponse(responseCode = "403", description = "Not Allowed",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = AccessDeniedException.class))
            ),
            @ApiResponse(responseCode = "404", description = "User not found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Error.class)
                    )
            )
    })
    @RequestMapping(value = "/my/oauth", method = RequestMethod.GET)
    @NonNull
    public List<UserOAuth> userOAuth(@Parameter(description = "Token for identify your account",
                                        example = "abcEFG145") @RequestHeader(required = false)
                                     @Nullable String token) throws Error {
        return users.userOAuth(token, identifyToken(token).getUser().getId());
    }
}
