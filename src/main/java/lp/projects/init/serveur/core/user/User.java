package lp.projects.init.serveur.core.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.auth.MinimalRole;
import lp.projects.init.serveur.core.loan.Loan;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

/**
 * Class represent User of this library
 */
@Entity
@Table(name = "user")
@DiscriminatorValue("USER")
@PrimaryKeyJoinColumn(name="user_id")
@Schema(description = "User of this Library")
public class User extends MinimalUser {

    @Column(name = "user_birthDate")
    @Schema(description = "Birth Date of this User", example = "2000-04-01T23:00.000+0000")
    protected Date birthDate;
    @Column(name = "user_address")
    @Schema(description = "Address of this User", example = "Sur la Route de Paris")
    protected String address;
    @Column(name = "user_city")
    @Schema(description = "City of this User", example = "Nantes")
    protected String city;

    /**
     * Shit Constructor for User Stored in DB
     * Used only by reflexion !
     */
    protected User() {
        super();
    }

    /**
     * Basic Constructor of User
     * @param id String represent id of User
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     * @param role MinimalRole represent role of User
     * @param birthDate Date represent birthDate of User
     * @param address String represent address of User
     * @param city String represent city of User
     */
    @JsonCreator
    @PersistenceConstructor
    public User(@NonNull String id, @Nullable String lastName, @Nullable String firstName,
                @NonNull String email, @NonNull String nickName, @NonNull MinimalRole role,
                @Nullable Date birthDate, @Nullable String address, @Nullable String city) {
        super(id, lastName, firstName, email, nickName, role);
        this.birthDate = birthDate;
        this.address = address;
        this.city = city;
    }

    /**
     * Basic Constructor of User
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     * @param role MinimalRole represent role of User
     * @param birthDate Date represent birthDate of User
     * @param address String represent address of User
     * @param city String represent city of User
     */
    public User(@Nullable String lastName, @Nullable String firstName,
                @NonNull String email, @NonNull String nickName, @NonNull MinimalRole role,
                @Nullable Date birthDate, @Nullable String address, @Nullable String city) {
        this(UUID.randomUUID().toString(), lastName, firstName, email, nickName,
            role, birthDate, address, city);
    }

    /**
     * Basic Constructor of User
     * @param id String represent id of User
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     * @param birthDate Date represent birthDate of User
     * @param address String represent address of User
     * @param city String represent city of User
     */
    public User(@NonNull String id, @Nullable String lastName, @Nullable String firstName,
                @NonNull String email, @NonNull String nickName, @Nullable Date birthDate,
                @Nullable String address, @Nullable String city) {
        this(id, lastName, firstName, email, nickName, DEFAULT_ROLE, birthDate, address, city);
    }

    /**
     * Basic Constructor of User
     * @param lastName String represent lastName of User
     * @param firstName String represent lastName of User
     * @param email String represent email of User
     * @param nickName String represent nickName of User
     * @param birthDate Date represent birthDate of User
     * @param address String represent address of User
     * @param city String represent city of User
     */
    public User(@Nullable String lastName, @Nullable String firstName,
                @NonNull String email, @NonNull String nickName, @Nullable Date birthDate,
                @Nullable String address, @Nullable String city) {
        this(lastName, firstName, email, nickName, DEFAULT_ROLE, birthDate, address, city);
    }

    /**
     * Constructor of User by MinimalUser or child
     * @param otherUser MinimalUser used for extract informations
     */
    public User(@NonNull MinimalUser otherUser) {
        this(otherUser.getId(), otherUser.getLastName(), otherUser.getFirstName(),
            otherUser.getEmail(), otherUser.getNickName(), otherUser.getRole(),
            null, null, null);
    }

    /**
     * Constructor of User by MinimalUser or child
     * @param otherUser User used for extract informations
     */
    public User(@NonNull User otherUser) {
        this(otherUser.getId(), otherUser.getLastName(), otherUser.getFirstName(),
            otherUser.getEmail(), otherUser.getNickName(), otherUser.getRole(),
            otherUser.getBirthDate(), otherUser.getAddress(), otherUser.getCity());
    }

    /**
     * Constructor of User by Loan or child
     * @param loan Loan used for extract informations
     */
    public User(@NonNull Loan loan) {
        this(loan.getUser());
    }

    /**
     * Getter for get address
     * @return address
     */
    @Nullable
    public String getAddress() {
        return address;
    }

    /**
     * Getter for get city
     * @return city
     */
    @Nullable
    public String getCity() {
        return city;
    }

    /**
     * Getter for get birthdate
     * @return birthDate
     */
    @NonNull
    public Date getBirthDate() {
        return birthDate;
    }
}
