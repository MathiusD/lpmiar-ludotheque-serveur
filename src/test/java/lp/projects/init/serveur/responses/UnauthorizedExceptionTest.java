package lp.projects.init.serveur.responses;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test UnauthorizedException
 */
public class UnauthorizedExceptionTest {

    /**
     * For test Instantiation of UnauthorizedException
     */
    @Test
    public void testInstantiation() {
        Error unAuthError = new Error(HttpStatus.UNAUTHORIZED);
        UnauthorizedException baseUnAuth = new UnauthorizedException();
        assertEquals(baseUnAuth, unAuthError);
        unAuthError = new Error(HttpStatus.UNAUTHORIZED, "truc");
        assertNotEquals(baseUnAuth, unAuthError);
        baseUnAuth = new UnauthorizedException("truc");
        assertEquals(baseUnAuth, unAuthError);
    }
}
