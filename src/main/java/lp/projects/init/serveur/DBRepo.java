package lp.projects.init.serveur;

import lp.projects.init.serveur.core.auth.MinimalRole;
import lp.projects.init.serveur.core.auth.Role;
import lp.projects.init.serveur.core.auth.Token;
import lp.projects.init.serveur.core.game.Game;
import lp.projects.init.serveur.core.game.MinimalGame;
import lp.projects.init.serveur.core.game.ShortGame;
import lp.projects.init.serveur.core.loan.Loan;
import lp.projects.init.serveur.core.user.MinimalUser;
import lp.projects.init.serveur.core.user.User;
import lp.projects.init.serveur.core.user.UserOAuth;
import lp.projects.init.serveur.db.auth.MinimalRoleRepo;
import lp.projects.init.serveur.db.auth.RoleRepo;
import lp.projects.init.serveur.db.auth.TokenRepo;
import lp.projects.init.serveur.db.game.GameRepo;
import lp.projects.init.serveur.db.game.MinimalGameRepo;
import lp.projects.init.serveur.db.game.ShortGameRepo;
import lp.projects.init.serveur.db.loan.LoanRepo;
import lp.projects.init.serveur.db.loan.MinimalLoanRepo;
import lp.projects.init.serveur.db.user.MinimalUserRepo;
import lp.projects.init.serveur.db.user.UserOAuthRepo;
import lp.projects.init.serveur.db.user.UserRepo;
import lp.projects.init.serveur.responses.AccessDeniedException;
import lp.projects.init.serveur.responses.InvalidTokenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Class represent Base DB of this app
 */
public class DBRepo {

    @Autowired
    protected TokenRepo tokenRepository;

    @Autowired
    protected GameRepo gameRepository;
    @Autowired
    protected ShortGameRepo shortGameRepository;
    @Autowired
    protected MinimalGameRepo minimalGameRepository;

    @Autowired
    protected UserRepo userRepository;
    @Autowired
    protected MinimalUserRepo minimalUserRepository;

    @Autowired
    protected UserOAuthRepo userOAuthRepo;

    @Autowired
    protected RoleRepo roleRepository;
    @Autowired
    protected MinimalRoleRepo minimalRoleRepository;

    @Autowired
    protected LoanRepo loanRepository;
    @Autowired
    protected MinimalLoanRepo minimalLoanRepository;

    /**
     * Method for remove each token
     */
    public void cleanToken() {
        tokenRepository.deleteAll();
    }

    /**
     * Method for remove each loans
     */
    public void cleanLoans() {
        loanRepository.deleteAll();
        minimalLoanRepository.deleteAll();
    }

    /**
     * Method for remove each users
     */
    public void cleanUsers() {
        userOAuthRepo.deleteAll();
        userRepository.deleteAll();
        minimalUserRepository.deleteAll();
    }

    /**
     * Method for remove each roles
     */
    public void cleanRoles() {
        roleRepository.deleteAll();
        minimalRoleRepository.deleteAll();
    }

    /**
     * Method for remove each games
     */
    public void cleanGames() {
        gameRepository.deleteAll();
        shortGameRepository.deleteAll();
        minimalGameRepository.deleteAll();
    }

    /**
     * Method for clean database
     */
    public void cleanRepo() {
        cleanToken();
        cleanLoans();
        cleanUsers();
        cleanRoles();
        cleanGames();
    }

    /**
     * Method for insert specific token
     * @param token new token in database
     * @return isInserted
     */
    public boolean insertToken(Token token) {
        if (tokenRepository.existsById(token.getToken()))
            return false;
        insertUser(token.getUser());
        tokenRepository.save(token);
        tokenRepository.flush();
        return true;
    }

    /**
     * Method for insert specific user
     * @param user new User in database
     * @return isInserted
     */
    public boolean insertUser(MinimalUser user) {
        return insertUser(new User(user));
    }

    /**
     * Method for insert specific user
     * @param user new User in database
     * @return isInserted
     */
    public boolean insertUser(User user) {
        if (userRepository.existsById(user.getId()))
            return false;
        insertRole(user.getRole());
        userRepository.save(user);
        userRepository.flush();
        return true;
    }

    /**
     * Method for insert specific userOAuth
     * @param user new UserOAuth in database
     * @return isInserted
     */
    public boolean insertUserOAuth(UserOAuth user) {
        if (userOAuthRepo.existsById(user.getId()))
            return false;
        insertUser(user.getUserRelated());
        userOAuthRepo.save(user);
        userOAuthRepo.flush();
        return true;
    }

    /**
     * Method for insert specific role
     * @param role new Role in database
     * @return isInserted
     */
    public boolean insertRole(MinimalRole role) {
        return insertRole(new Role(role));
    }

    /**
     * Method for insert specific role
     * @param role new Role in database
     * @return isInserted
     */
    public boolean insertRole(Role role) {
        if (roleRepository.existsById(role.getName()))
            return false;
        roleRepository.save(role);
        roleRepository.flush();
        return true;
    }

    /**
     * Method for insert specific game
     * @param game new game in database
     * @return isInserted
     */
    public boolean insertGame(MinimalGame game) {
        return insertGame(new ShortGame(game));
    }

    /**
     * Method for insert specific game
     * @param game new game in database
     * @return isInserted
     */
    public boolean insertGame(ShortGame game) {
        return insertGame(new Game(game));
    }

    /**
     * Method for insert specific game
     * @param game new game in database
     * @return isInserted
     */
    public boolean insertGame(Game game) {
        if (gameRepository.existsById(game.getId()))
            return false;
        gameRepository.save(game);
        gameRepository.flush();
        return true;
    }

    /**
     * Method for insert specific loan
     * @param loan new Loan in database
     * @return isInserted
     */
    public boolean insertLoan(Loan loan) {
        if (loanRepository.existsById(loan.getId()))
            return false;
        insertGame(loan.getGame());
        insertUser(loan.getUser());
        loanRepository.save(loan);
        loanRepository.flush();
        return true;
    }

    /**
     * Method for insert specific token
     * @param token new token in database
     * @return isInserted
     */
    public boolean updateToken(Token token) {
        if (!tokenRepository.existsById(token.getToken()))
            return false;
        updateUser(token.getUser());
        tokenRepository.save(token);
        tokenRepository.flush();
        return true;
    }

    /**
     * Method for insert specific user
     * @param user new User in database
     * @return isInserted
     */
    public boolean updateUser(MinimalUser user) {
        return updateUser(new User(user));
    }

    /**
     * Method for insert specific user
     * @param user new User in database
     * @return isInserted
     */
    public boolean updateUser(User user) {
        if (!userRepository.existsById(user.getId()))
            return false;
        updateRole(user.getRole());
        userRepository.save(user);
        userRepository.flush();
        return true;
    }

    /**
     * Method for insert specific userOAuth
     * @param user new UserOAuth in database
     * @return isInserted
     */
    public boolean updateUserOAuth(UserOAuth user) {
        if (!userOAuthRepo.existsById(user.getId()))
            return false;
        insertUser(user.getUserRelated());
        userOAuthRepo.save(user);
        userOAuthRepo.flush();
        return true;
    }

    /**
     * Method for insert specific role
     * @param role new Role in database
     * @return isInserted
     */
    public boolean updateRole(MinimalRole role) {
        return updateRole(new Role(role));
    }

    /**
     * Method for insert specific role
     * @param role new Role in database
     * @return isInserted
     */
    public boolean updateRole(Role role) {
        if (!roleRepository.existsById(role.getName()))
            return false;
        roleRepository.save(role);
        roleRepository.flush();
        return true;
    }

    /**
     * Method for insert specific game
     * @param game new game in database
     * @return isInserted
     */
    public boolean updateGame(MinimalGame game) {
        return updateGame(new ShortGame(game));
    }

    /**
     * Method for insert specific game
     * @param game new game in database
     * @return isInserted
     */
    public boolean updateGame(ShortGame game) {
        return updateGame(new Game(game));
    }

    /**
     * Method for insert specific game
     * @param game new game in database
     * @return isInserted
     */
    public boolean updateGame(Game game) {
        if (!gameRepository.existsById(game.getId()))
            return false;
        gameRepository.save(game);
        gameRepository.flush();
        return true;
    }

    /**
     * Method for insert specific loan
     * @param loan new Loan in database
     * @return isInserted
     */
    public boolean updateLoan(Loan loan) {
        if (!loanRepository.existsById(loan.getId()))
            return false;
        updateGame(loan.getGame());
        updateUser(loan.getUser());
        loanRepository.save(loan);
        return true;
    }

    public boolean removeToken(Token token) {
        if (!tokenRepository.existsById(token.getToken()))
            return false;
        tokenRepository.delete(token);
        tokenRepository.flush();
        return true;
    }

    /**
     * Method for delete specific role
     * @param role role for deletion
     * @return isRemoved
     */
    public boolean removeRole(Role role) {
        if (!roleRepository.existsById(role.getName()))
            return false;
        roleRepository.delete(role);
        roleRepository.flush();
        minimalRoleRepository.delete(role);
        minimalRoleRepository.flush();
        return true;
    }

    /**
     * Method for delete specific user
     * @param user user for deletion
     * @return isRemoved
     */
    public boolean removeUser(MinimalUser user) {
        return removeUser(new User(user));
    }

    /**
     * Method for delete specific user
     * @param user user for deletion
     * @return isRemoved
     */
    public boolean removeUser(User user) {
        if (!userRepository.existsById(user.getId()))
            return false;
        userRepository.delete(user);
        userRepository.flush();
        minimalUserRepository.delete(user);
        minimalUserRepository.flush();
        return true;
    }

    /**
     * Method for delete specific userOAuth
     * @param user userOAuth for deletion
     * @return isRemoved
     */
    public boolean removeUserOAuth(UserOAuth user) {
        if (!userOAuthRepo.existsById(user.getId()))
            return false;
        userOAuthRepo.delete(user);
        userOAuthRepo.flush();
        return true;
    }

    /**
     * Method for delete specific game
     * @param game game for deletion
     * @return isRemoved
     */
    public boolean removeGame(MinimalGame game) {
        return removeGame(new ShortGame(game));
    }

    /**
     * Method for delete specific game
     * @param game game for deletion
     * @return isRemoved
     */
    public boolean removeGame(ShortGame game) {
        return removeGame(new Game(game));
    }

    /**
     * Method for delete specific game
     * @param game game for deletion
     * @return isRemoved
     */
    public boolean removeGame(Game game) {
        if (!gameRepository.existsById(game.getId()))
            return false;
        gameRepository.delete(game);
        gameRepository.flush();
        shortGameRepository.delete(game);
        shortGameRepository.flush();
        minimalGameRepository.delete(game);
        minimalGameRepository.flush();
        return true;
    }

    /**
     * Method for delete specific loan
     * @param loan Loan for deletion
     * @return isRemoved
     */
    public boolean removeLoan(Loan loan) {
        if (!loanRepository.existsById(loan.getId()))
            return false;
        loanRepository.delete(loan);
        loanRepository.flush();
        minimalLoanRepository.delete(loan);
        minimalLoanRepository.flush();
        return true;
    }

    /**
     * Method for get Token Object related to this token value
     * @param tokenValue String represent value of token passed
     * @return tokenGiven
     */
    @NonNull
    public Token identifyToken(@Nullable String tokenValue) throws
            AccessDeniedException, InvalidTokenException {
        return identifyToken(tokenValue, false, null);
    }

    /**
     * Method for get Token Object related to this token value
     * @param tokenValue String represent value of token passed
     * @param isAdmin Boolean represent if Token must be administration auth
     * @param path String represent section reached for this request
     * @return tokenGiven
     */
    @NonNull
    public Token identifyToken(@Nullable String tokenValue, boolean isAdmin, @Nullable String path)
            throws InvalidTokenException, AccessDeniedException {
        Token tokenGiven = null;
        if (tokenValue != null)
            for (Token tokenData: tokenRepository.findAll())
                if (tokenData.getToken().equals(tokenValue) && tokenGiven == null)
                    tokenGiven = tokenData;
        if (tokenGiven == null)
            throw new InvalidTokenException(tokenValue);
        if (isAdmin)
            if (path == null)
                throw new IllegalArgumentException("Path must be not null if isAdmin is true !");
            else
                grantAccess(tokenGiven, path);
        return tokenGiven;
    }

    /**
     * Method for throw ForbiddenException if token don't have admin permission
     * @param token Token for this request
     * @param path String represent section reached for this request
     */
    @NonNull
    public void grantAccess(@NonNull Token token, @NonNull String path) throws AccessDeniedException {
        grantAccess(token, path,null);
    }

    /**
     * Method for throw ForbiddenException if token isn't same of request or token don't have admin permission
     * @param token Token for this request
     * @param path String represent section reached for this request
     * @param dataOfUser MinimalUser for represent user related to data reached
     */
    @NonNull
    public void grantAccess(@NonNull Token token, @NonNull String path, @Nullable MinimalUser dataOfUser)
            throws AccessDeniedException {
        if (!(token.getUser().equals(dataOfUser) || token.getUser().getRole().getIsAdmin()))
            throw new AccessDeniedException(token.getUser(), path);
    }
}
