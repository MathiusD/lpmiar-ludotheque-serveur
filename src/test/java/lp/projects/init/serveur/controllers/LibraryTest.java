package lp.projects.init.serveur.controllers;

import lp.projects.init.serveur.BaseAPITest;
import lp.projects.init.serveur.core.loan.*;
import lp.projects.init.serveur.core.user.MinimalUser;
import lp.projects.init.serveur.core.user.MinimalUserWithLoans;
import lp.projects.init.serveur.core.user.User;
import lp.projects.init.serveur.core.user.UserSchema;
import lp.projects.init.serveur.responses.AccessDeniedException;
import lp.projects.init.serveur.responses.BadRequestException;
import lp.projects.init.serveur.responses.InvalidTokenException;
import lp.projects.init.serveur.responses.NotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.http.*;
import org.springframework.lang.NonNull;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LibraryTest extends BaseAPITest {

    /**
     * Method for get base uri of Library testing
     * @return baseUri for springTests
     */
    @NonNull
    @Override
    public String baseUri() {
        return String.format("%s/library", super.baseUri());
    }

    /**
     * Test for route /library/loans without args
     */
    @Test
    public void testLoansWithoutArgs() {
        assertEquals(new InvalidTokenException(), customExchange("/loans", InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", "truc");
        assertEquals(customExchange("/loans", headers,  InvalidTokenException.class),
            new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        for (MinimalUserWithLoans usr: customExchange("/loans", headers, MinimalUserWithLoans[].class))
            assertTrue(libraryData.contains(usr));
        headers = new HttpHeaders();
        headers.add("token", otherToken.getToken());
        assertEquals(customExchange("/loans", headers,  AccessDeniedException.class),
                new AccessDeniedException(meToo, "/library/loans"));
    }

    /**
     * Test for route /library/loans with state arg only
     */
    @Test
    public void testLoansWithStateOnly() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(Arrays.asList(
             customExchange("/loans?state=truc", headers,  MinimalUserWithLoans[].class)), new ArrayList<>());
        for (State state : State.ALL_STATES) {
            MinimalUserWithLoans[] userWithLoans = customExchange(String.format("/loans?state=%s",
                state.getAccessKey()), headers,  MinimalUserWithLoans[].class);
            List<MinimalUserWithLoans> dataLibrary = new ArrayList<>();
            Map<String, List<Loan>> dataLoans = new HashMap<>();
            for (Loan loan: libraryLoans)
                if (loan.getState().equals(state)) {
                    if (!dataLoans.containsKey(loan.getUser().getId()))
                        dataLoans.put(loan.getUser().getId(), new ArrayList<>());
                    List<Loan> temp = dataLoans.get(loan.getUser().getId());
                    temp.add(loan);
                    dataLoans.put(loan.getUser().getId(), temp);
                }
            dataLoans.forEach((s, loans) -> dataLibrary.add(new MinimalUserWithLoans(loans)));
            for (MinimalUserWithLoans usr: userWithLoans)
                assertTrue(dataLibrary.contains(usr));
        }
    }

    /**
     * Test for route /library/loans/{id}
     */
    @Test
    public void testLoan() {
        assertEquals(new InvalidTokenException(), customExchange("/loans/truc",
                InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", "truc");
        assertEquals(customExchange("/loans/truc", headers,  InvalidTokenException.class),
            new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(libraryLoans.get(0),
                customExchange(String.format("/loans/%s", libraryLoans.get(0).getId()), headers, Loan.class));
        assertEquals(new NotFoundException("Loan with the given id (truc) was not found"),
                customExchange("/loans/truc", headers, NotFoundException.class));
        headers = new HttpHeaders();
        headers.add("token", otherToken.getToken());
        assertEquals(customExchange(String.format("/loans/%s", myLoans.get(0).getId()), headers,
                AccessDeniedException.class), new AccessDeniedException(meToo,
                String.format("/loans/%s", myLoans.get(0).getId())));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(customExchange(String.format("/loans/%s", meTooLoans.get(0).getId()), headers,
                Loan.class), meTooLoans.get(0));
    }

    /**
     * Test for route /library/loans/{id} by POST
     */
    @Test
    public void testUpdateLibraryLoan() {
        HttpHeaders headers = new HttpHeaders();
        LoanUpdate update = new LoanUpdate("truc");
        HttpEntity<LoanUpdate> entity = new HttpEntity<>(update, headers);
        assertEquals(new InvalidTokenException(), customExchange("/loans/truc", HttpMethod.POST,
                entity, InvalidTokenException.class));
        headers.add("token", "truc");
        entity = new HttpEntity<>(update, headers);
        assertEquals(customExchange("/loans/truc", HttpMethod.POST, entity, InvalidTokenException.class),
                new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        Loan temp = libraryLoans.get(0);
        update = new LoanUpdate("truc");
        entity = new HttpEntity<>(update, headers);
        assertEquals(new BadRequestException("This MinimalState don't match with any existing State"),
                customExchange(String.format("/loans/%s", temp.getId()), HttpMethod.POST, entity,
                        BadRequestException.class));
        update = new LoanUpdate("waiting");
        entity = new HttpEntity<>(update, headers);
        assertEquals(new LoanUpdateResult(new Loan(temp.getId(), temp.getGame(), temp.getUser(), temp.getStartedAt(),
                temp.getEndAt(), State.WAITING, temp.getGameDataIsMinified()), String.format(
                "Loan is update for state : %s", update.state)), customExchange(String.format(
                "/loans/%s", temp.getId()), HttpMethod.POST, entity, LoanUpdateResult.class));
        assertEquals(new BadRequestException(
                        String.format("Change not allowed ! (Initial State : %s, Target State %s",
                                State.WAITING.getAccessKey(), State.WAITING.getAccessKey())),
                customExchange(String.format("/loans/%s", temp.getId()), HttpMethod.POST, entity,
                        BadRequestException.class));
    }

    /**
     * Test for route /library/loans/{id} by DELETE
     */
    @Test
    public void testDeleteLibraryLoan() {
        HttpHeaders headers = new HttpHeaders();
        assertEquals(new InvalidTokenException(), customExchange("/loans/truc", HttpMethod.DELETE,
                headers, InvalidTokenException.class));
        headers.add("token", "truc");
        assertEquals(customExchange("/loans/truc", HttpMethod.DELETE, headers, InvalidTokenException.class),
                new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", otherToken.getToken());
        Loan temp = meTooLoans.get(0);
        customExchange(String.format("/loans/%s", temp.getId()), HttpMethod.DELETE, headers, HttpStatus.NO_CONTENT);
        temp = meTooLoans.get(1);
        assertEquals(customExchange(String.format("/loans/%s", temp.getId()), HttpMethod.DELETE, headers,
                AccessDeniedException.class),
                new AccessDeniedException(meToo, String.format("/loans/%s", temp.getId())));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        customExchange(String.format("/loans/%s", temp.getId()), HttpMethod.DELETE, headers, HttpStatus.NO_CONTENT);
    }

    /**
     * Test for route /library/users without args
     */
    @Test
    public void testUsersWithoutArgs() {
        assertEquals(new InvalidTokenException(), customExchange("/users", InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", "truc");
        assertEquals(customExchange("/users", headers,  InvalidTokenException.class),
                new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        for (MinimalUser user : customExchange("/users", headers,  MinimalUser[].class))
            assertTrue(libraryUsers.contains(user));
        headers = new HttpHeaders();
        headers.add("token", otherToken.getToken());
        assertEquals(customExchange("/users", headers,  AccessDeniedException.class),
                new AccessDeniedException(meToo, "/library/users"));
    }

    /**
     * Test for route /library/users with schema args
     * @throws ClassNotFoundException if class for schema array isn't found
     */
    @Test
    public void testUsersWithSchemaArgs() throws ClassNotFoundException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        Class<? extends MinimalUser[]> temp;
        for (UserSchema schema : UserSchema.ALL_SCHEMA) {
            temp = (Class<? extends MinimalUser[]>) Class.forName("[L" + schema.userClass.getName() + ";");
            for (MinimalUser user : customExchange(String.format("/users?schema=%s", schema.name), headers, temp))
                assertTrue(libraryUsers.contains(user));
        }
    }

    /**
     * Test for route /library/city with City args
     */
    @Test
    public void testUsersWithCityArgs() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        for (User cityuser : libraryAllUsers) {
            assertTrue(Arrays.asList(customExchange(String.format("/users?city=%s", cityuser.getCity()),
                    headers, MinimalUser[].class)).contains(cityuser));
        }
    }

    /**
     * Test for route /library/role with Role args
     */
    @Test
    public void testUsersWithRoleArgs() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        for (MinimalUser role_user : libraryUsers) {
            assertTrue(Arrays.asList(customExchange(String.format("/users?Role=%s", role_user.getRole()),
                    headers, MinimalUser[].class)).contains(role_user));
        }
    }

    /**
     * Test for route /library/users with lastName args
     */
    @Test
    public void testUsersWithLastNameArgs() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        for (MinimalUser user : libraryUsers) {
            assertTrue(Arrays.asList(customExchange(String.format("/users?lastName=%s", user.getLastName()),
                    headers, MinimalUser[].class)).contains(user));
        }
    }

    /**
     * Test for route /library/users with firstName args
     */
    @Test
    public void testUsersWithFirstNameArgs() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        for (MinimalUser user : libraryUsers) {
            assertTrue(Arrays.asList(customExchange(String.format("/users?firstName=%s", user.getFirstName()),
                headers, MinimalUser[].class)).contains(user));
        }
    }

    /**
     * Test for route /library/users with lastName and Schema args
     * @throws ClassNotFoundException if class for schema array isn't found
     */
    @Test
    public void testUsersWithLastNameAndSchemaArgs() throws ClassNotFoundException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        Class<? extends MinimalUser[]> temp;
        for (UserSchema schema : UserSchema.ALL_SCHEMA) {
            temp = (Class<? extends MinimalUser[]>) Class.forName("[L" + schema.userClass.getName() + ";");
            for (User user : libraryAllUsers) {
                assertTrue(Arrays.asList(customExchange(String.format("/users?lastName=%s&schema=%s",
                    user.getLastName(), schema.name), headers, temp)).contains(schema.matchingUserWithThisSchema(user)));
            }
        }
    }

    /**
     * Test for route /library/users with firstName and Schema args
     * @throws ClassNotFoundException if class for schema array isn't found
     */
    @Test
    public void testUsersWithFirstNameAndSchemaArgs() throws ClassNotFoundException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        Class<? extends MinimalUser[]> temp;
        for (UserSchema schema : UserSchema.ALL_SCHEMA) {
            temp = (Class<? extends MinimalUser[]>) Class.forName("[L" + schema.userClass.getName() + ";");
            for (User user : libraryAllUsers) {
                assertTrue(Arrays.asList(customExchange(String.format("/users?firstName=%s&schema=%s",
                    user.getFirstName(), schema.name), headers, temp))
                        .contains(schema.matchingUserWithThisSchema(user)));
            }
        }
    }

    /**
     * Test for route /library/users with lastName and firstName args
     */
    @Test
    public void testUsersWithLastNameAndFirstNameArgs() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        for (MinimalUser user : libraryUsers) {
            assertTrue(Arrays.asList(customExchange(String.format("/users?lastName=%s&firstName=%s",
                user.getLastName(), user.getFirstName()), headers, MinimalUser[].class)).contains(user));
        }
    }

    /**
     * Test for route /library/users with firstName, lastName and Schema args
     * @throws ClassNotFoundException if class for schema array isn't found
     */
    @Test
    public void testUsersWithFirstNameAndLastNameAndSchemaArgs() throws ClassNotFoundException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        Class<? extends MinimalUser[]> temp;
        for (UserSchema schema : UserSchema.ALL_SCHEMA) {
            temp = (Class<? extends MinimalUser[]>) Class.forName("[L" + schema.userClass.getName() + ";");
            for (User user : libraryAllUsers) {
                assertTrue(Arrays.asList(customExchange(String.format("/users?firstName=%s&schema=%s&lastName=%s",
                    user.getFirstName(), schema.name, user.getLastName()), headers, temp))
                        .contains(schema.matchingUserWithThisSchema(user)));
            }
        }
    }

    /**
     * Test for route /library/users/{id}
     */
    @Test
    public void testUser() {
        assertEquals(new InvalidTokenException(), customExchange("/users/truc",
            InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", "truc");
        assertEquals(customExchange("/users/truc", headers,  InvalidTokenException.class),
            new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(libraryUsers.get(0),
            customExchange(String.format("/users/%s", libraryUsers.get(0).getId()), headers,  User.class));
        assertEquals(new NotFoundException("User with the given id (truc) was not found"),
            customExchange("/users/truc", headers,  NotFoundException.class));
        headers = new HttpHeaders();
        headers.add("token", otherToken.getToken());
        assertEquals(customExchange(String.format("/users/%s", my.getId()), headers,
                AccessDeniedException.class), new AccessDeniedException(meToo,
                String.format("/users/%s", my.getId())));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(customExchange(String.format("/users/%s", meToo.getId()), headers,
                User.class), meToo);
    }

    /**
     * Test for route /library/users/{id}/loans without args
     */
    @Test
    public void testUserLoansWithoutArgs() {
        assertEquals(new InvalidTokenException(), customExchange("/users/truc/loans",
            InvalidTokenException.class));
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", "truc");
        assertEquals(customExchange("/users/truc/loans", headers,  InvalidTokenException.class),
                new InvalidTokenException("truc"));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(Arrays.asList(customExchange(String.format("/users/%s/loans", my.getId()), headers,
            MinimalLoan[].class)), myMinimalLoans);
        assertEquals(customExchange("/users/truc/loans", headers,  NotFoundException.class),
            new NotFoundException("User with the given id (truc) was not found"));
        headers = new HttpHeaders();
        headers.add("token", otherToken.getToken());
        assertEquals(customExchange(String.format("/users/%s/loans", my.getId()), headers,
                AccessDeniedException.class), new AccessDeniedException(meToo,
                String.format("/users/%s/loans", my.getId())));
        headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(Arrays.asList(customExchange(String.format("/users/%s/loans", meToo.getId()), headers,
                MinimalLoan[].class)), meTooMinimalLoans);
    }

    /**
     * Test for route /library/users/{id}/loans with state arg only
     */
    @Test
    public void testUserLoansWithStateOnly() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("token", myToken.getToken());
        assertEquals(Arrays.asList(
                customExchange(String.format("/users/%s/loans?state=truc", my.getId()), headers,
                    MinimalLoan[].class)), new ArrayList<>());
        for (State state : State.ALL_STATES) {
            MinimalLoan[] loans = customExchange(String.format("/users/%s/loans?state=%s", my.getId(),
                state.getAccessKey()), headers, MinimalLoan[].class);
            List<MinimalLoan> dataLoans = new ArrayList<>();
            for (MinimalLoan loan: myLoans)
                if (loan.getState().equals(state))
                    dataLoans.add(loan);
            assertEquals(Arrays.asList(loans), dataLoans);
        }
    }
}
