package lp.projects.init.serveur.core.loan;

import lp.projects.init.serveur.core.game.Game;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
/**
 * Class for test MinimalLoan
 */
public class MinimalLoanTest {

    /**
     * For test Instantiation of MinimalLoan
     */
    @Test
    public void testMinimalLoanInstantiation() {
        Game game = new Game("ABC", "Truc", null, null, null, null,
            null, null, null, null, null, null);
        MinimalLoan myLoan = new MinimalLoan("ABC", game, 4800);
        assertEquals(myLoan.getEndAt(), myLoan.startedAt + 4800);
        myLoan = new MinimalLoan("ABC", game, TimeUnit.MINUTES, 4800);
        assertEquals(myLoan.getEndAt(), myLoan.startedAt + (4800 * 60));
        assertEquals(myLoan.getMinimalLoan(), new MinimalLoan(myLoan));
    }
}
