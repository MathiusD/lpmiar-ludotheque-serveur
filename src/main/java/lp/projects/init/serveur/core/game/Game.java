package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.opencsv.CSVReader;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Class represent Game in this library
 */
@Entity
@Table(name = "game")
@DiscriminatorValue("GAME")
@PrimaryKeyJoinColumn(name = "game_id")
@Schema(description = "Game of this Library")
public class Game extends ShortGame {

    @Column(name = "game_company_name")
    @Schema(example = "Fantasy Flight Game", description = "Name of Company related to this Game")
    protected String companyName;
    @Column(name = "game_description", columnDefinition = "LONGTEXT")
    @Schema(description = "For describe/present the Game", example = "Wonderful Game")
    protected String description;
    @Column(name = "game_author")
    @Schema(description = "Authors of this game", example = "Madoka Kitao")
    protected String author;
    @Column(name = "game_illustrator")
    @Schema(description = "Illustrators of this game", example = "Naïade")
    protected String illustrator;
    @Column(name = "game_demo_link")
    @Schema(description = "Link related to demo of this Game", example = "https://www.youtube.com/watch?v=dQw4w9WgXcQ")
    protected String demoLink;
    @Column(name = "game_location")
    @Schema(description = "Location of this game in library", example = "RDC")
    protected String location;

    /**
     * Shit Constructor for Game Stored in DB
     * Used only by reflexion !
     */
    protected Game() {
        super();
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param state GameState represent state of this game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    @JsonCreator
    @PersistenceConstructor
    public Game(@NonNull String id, @NonNull String name, @NonNull GameState state, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, @Nullable YearRecommended yearRecommended, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        super(id, name, state, nbPlayer, yearRecommended, time, type);
        this.companyName = companyName;
        this.author = author;
        this.illustrator = illustrator;
        this.description = description;
        this.demoLink = demoLink;
        this.location = location;
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param state GameState represent state of this game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @NonNull GameState state, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, int actualYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        this(id, name, state, companyName, nbPlayer, new YearRecommended(actualYear), time, type,
            description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param state GameState represent state of this game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @NonNull GameState state, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, int minYear, int maxYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        this(id, name, state, companyName, nbPlayer, new YearRecommended(minYear, maxYear), time, type,
                description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param state GameState represent state of this game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @NonNull GameState state, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, @Nullable YearRecommended yearRecommended, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        this(UUID.randomUUID().toString(), name, state, companyName, nbPlayer, yearRecommended, time, type, description,
            author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param state GameState represent state of this game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @NonNull GameState state, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, int actualYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        this(UUID.randomUUID().toString(), name, state, companyName, nbPlayer,
            new YearRecommended(actualYear), time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param state GameState represent state of this game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @NonNull GameState state, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, int minYear, int maxYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        this(UUID.randomUUID().toString(), name, state, companyName, nbPlayer,
            new YearRecommended(minYear, maxYear), time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, @Nullable YearRecommended yearRecommended, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        this(id, name, DEFAULT_STATE, companyName, nbPlayer, yearRecommended, time, type,
            description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, int actualYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        this(id, name, DEFAULT_STATE, companyName, nbPlayer, new YearRecommended(actualYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, int minYear, int maxYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        this(id, name, DEFAULT_STATE, companyName, nbPlayer, new YearRecommended(minYear, maxYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, @Nullable YearRecommended yearRecommended, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        this(name, DEFAULT_STATE, companyName, nbPlayer, yearRecommended, time, type,
            description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, int actualYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        this(name, DEFAULT_STATE, companyName, nbPlayer, new YearRecommended(actualYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @Nullable String companyName,
                @Nullable NbPlayer nbPlayer, int minYear, int maxYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink, @Nullable String location) {
        this(name, DEFAULT_STATE, companyName, nbPlayer, new YearRecommended(minYear, maxYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @Nullable String companyName, int actualNbPlayer,
                @Nullable YearRecommended yearRecommended, @Nullable GameTime time, @Nullable String type,
                @Nullable String description, @Nullable String author, @Nullable String illustrator,
                @Nullable String demoLink, @Nullable String location) {
        this(id, name, companyName, new NbPlayer(actualNbPlayer), yearRecommended, time, type,
                description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @Nullable String companyName, int actualNbPlayer,
                int actualYear, @Nullable GameTime time, @Nullable String type,
                @Nullable String description, @Nullable String author, @Nullable String illustrator,
                @Nullable String demoLink, @Nullable String location) {
        this(id, name, companyName, new NbPlayer(actualNbPlayer), new YearRecommended(actualYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @Nullable String companyName, int actualNbPlayer,
                int minYear, int maxYear, @Nullable GameTime time, @Nullable String type,
                @Nullable String description, @Nullable String author, @Nullable String illustrator,
                @Nullable String demoLink, @Nullable String location) {
        this(id, name, companyName, new NbPlayer(actualNbPlayer), new YearRecommended(minYear, maxYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @Nullable String companyName, int actualNbPlayer,
                @Nullable YearRecommended yearRecommended, @Nullable GameTime time, @Nullable String type,
                @Nullable String description, @Nullable String author, @Nullable String illustrator,
                @Nullable String demoLink, @Nullable String location) {
        this(name, companyName, new NbPlayer(actualNbPlayer), yearRecommended, time, type,
                description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @Nullable String companyName, int actualNbPlayer,
                int actualYear, @Nullable GameTime time, @Nullable String type,
                @Nullable String description, @Nullable String author, @Nullable String illustrator,
                @Nullable String demoLink, @Nullable String location) {
        this(name, companyName, new NbPlayer(actualNbPlayer), new YearRecommended(actualYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @Nullable String companyName, int actualNbPlayer,
                int minYear, int maxYear, @Nullable GameTime time, @Nullable String type,
                @Nullable String description, @Nullable String author, @Nullable String illustrator,
                @Nullable String demoLink, @Nullable String location) {
        this(name, companyName, new NbPlayer(actualNbPlayer), new YearRecommended(minYear, maxYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @Nullable String companyName, int actualNbPlayer,
                @Nullable GameTime time, @Nullable String type, @Nullable String description,
                @Nullable String author, @Nullable String illustrator, @Nullable String demoLink,
                @Nullable String location) {
        this(id, name, companyName, new NbPlayer(actualNbPlayer), null, time, type,
            description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @Nullable String companyName, int actualNbPlayer,
                @Nullable GameTime time, @Nullable String type, @Nullable String description,
                @Nullable String author, @Nullable String illustrator, @Nullable String demoLink,
                @Nullable String location) {
        this(name, companyName, new NbPlayer(actualNbPlayer), null, time, type,
                description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @Nullable String companyName, int minNbPlayer,
                int maxNbPlayer, @Nullable YearRecommended yearRecommended, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink,
                @Nullable String location) {
        this(id, name, companyName, new NbPlayer(minNbPlayer, maxNbPlayer), yearRecommended, time,
            type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @Nullable String companyName, int minNbPlayer,
                int maxNbPlayer, @Nullable Integer actualYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink,
                @Nullable String location) {
        this(id, name, companyName, new NbPlayer(minNbPlayer, maxNbPlayer), new YearRecommended(actualYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String id, @NonNull String name, @Nullable String companyName, int minNbPlayer,
                int maxNbPlayer, int minYear, int maxYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink,
                @Nullable String location) {
        this(id, name, companyName, new NbPlayer(minNbPlayer, maxNbPlayer), new YearRecommended(minYear, maxYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @Nullable String companyName, int minNbPlayer,
                int maxNbPlayer, @Nullable YearRecommended yearRecommended, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink,
                @Nullable String location) {
        this(name, companyName, new NbPlayer(minNbPlayer, maxNbPlayer), yearRecommended, time,
                type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @Nullable String companyName, int minNbPlayer,
                int maxNbPlayer, @NonNull Integer actualYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink,
                @Nullable String location) {
        this(name, companyName, new NbPlayer(minNbPlayer, maxNbPlayer), new YearRecommended(actualYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param companyName String represent Company Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     * @param description String represent description of this Game
     * @param author String represent Author of this Game
     * @param illustrator String represent Illustrator of this Game
     * @param demoLink String contain URI for demo of this Game
     * @param location String represent location of Game in this Library
     */
    public Game(@NonNull String name, @Nullable String companyName, int minNbPlayer,
                int maxNbPlayer, int minYear, int maxYear, @Nullable GameTime time,
                @Nullable String type, @Nullable String description, @Nullable String author,
                @Nullable String illustrator, @Nullable String demoLink,
                @Nullable String location) {
        this(name, companyName, new NbPlayer(minNbPlayer, maxNbPlayer), new YearRecommended(minYear, maxYear),
            time, type, description, author, illustrator, demoLink, location);
    }

    /**
     * Constructor of Game by MinimalGame or child
     * @param otherGame MinimalGame used for extract informations
     */
    public Game(@NonNull MinimalGame otherGame) {
        this(otherGame.getId(), otherGame.getName(), otherGame.getGameState(), null,
            null, null, null, null, null, null,
            null, null, null);
    }

    /**
     * Constructor of Game by ShortGame or child
     * @param otherGame ShortGame used for extract informations
     */
    public Game(@NonNull ShortGame otherGame) {
        this(otherGame.getId(), otherGame.getName(), otherGame.getGameState(), null,
                otherGame.getNbPlayer(), otherGame.getYearRecommended(), otherGame.getTime(), otherGame.getType(),
                null, null, null, null, null);
    }

    /**
     * Constructor of Game by Game or child
     * @param otherGame Game used for extract informations
     */
    public Game(@NonNull Game otherGame) {
        this(otherGame.getId(), otherGame.getName(), otherGame.getGameState(), otherGame.getCompanyName(),
                otherGame.getNbPlayer(), otherGame.getYearRecommended(), otherGame.getTime(), otherGame.getType(),
                otherGame.getDescription(), otherGame.getAuthor(), otherGame.getIllustrator(),
                otherGame.getDemoLink(), otherGame.getLocation());
    }

    /**
     * Getter for Location
     * @return location
     */
    @Nullable
    public String getLocation() {
        return location;
    }

    /**
     * Getter for description of Game
     * @return description
     */
    @Nullable
    public String getDescription() {
        return description;
    }

    /**
     * Setter for Location
     */
    public void setLocation(@Nullable String location) {
        this.location = location;
    }

    /**
     * Setter for Description
     */
    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    /**
     * Getter for author of Game
     * @return author
     */
    @Nullable
    public String getAuthor() {
        return author;
    }

    /**
     * Getter for companyName of Game
     * @return companyName
     */
    @Nullable
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Getter for illustrator of Game
     * @return illustrator
     */
    @Nullable
    public String getIllustrator() {
        return illustrator;
    }

    /**
     * Getter for demoLink of Game
     * @return demoLink
     */
    @Nullable
    public String getDemoLink() {
        return demoLink;
    }

    /**
     * Method for extract Games stored in CSV file
     * @param path String represent path where csv is stored
     * @throws IOException Exception throw if file not found or missing permission for access file
     * @return gamesExtract
     */
    @NonNull
    public static List<Game> extractGamesFromCSV(@NonNull String path) throws IOException {
        return extractGamesFromCSV(Paths.get(path));
    }

    /**
     * Method for extract Games stored in CSV file
     * @param path Path represent path where csv is stored
     * @throws IOException Exception throw if file not found or missing permission for access file
     * @return gamesExtract
     */
    @NonNull
    public static List<Game> extractGamesFromCSV(@NonNull Path path) throws IOException {
        Reader readerUsed = Files.newBufferedReader(path);
        List<Game> gameExtract = extractGamesFromCSV(readerUsed);
        readerUsed.close();
        return gameExtract;
    }

    /**
     * Method for extract Games stored in CSV file
     * @param baseFile MultipartFile represent CSV file
     * @throws IOException Exception throw if file not found or missing permission for access file
     * @return gamesExtract
     */
    @NonNull
    public static List<Game> extractGamesFromCSV(@NonNull MultipartFile baseFile) throws IOException {
        Reader readerUsed = new BufferedReader(new InputStreamReader(baseFile.getInputStream()));
        List<Game> gameExtract = extractGamesFromCSV(readerUsed);
        readerUsed.close();
        return gameExtract;
    }

    /**
     * Method for extract Games stored in CSV file
     * @param reader Reader represent reader used for read csv
     * @throws IOException Exception throw if file not found or missing permission for access file
     * @return gamesExtract
     */
    @NonNull
    public static List<Game> extractGamesFromCSV(@NonNull Reader reader) throws IOException {
        List<Game> gamesExtract = new ArrayList<>();
        CSVReader csvReader = new CSVReader(reader);
        String[] firstLine = null;
        String[] line;
        while ((line = csvReader.readNext()) != null) {
            if (firstLine != null) {
                Game gameLine = extractGameFromCSV(firstLine, line);
                if (gameLine != null)
                    gamesExtract.add(gameLine);
            } else
                firstLine = line;
        }
        csvReader.close();
        return gamesExtract;
    }

    protected final static Map<String, String> csvHeader;
    protected final static Map<String, GameState> gameStates;

    static {
        Map<String, String> tempCsvHeader = new HashMap<>();
        tempCsvHeader.put("id", "CodeJeu");
        tempCsvHeader.put("name", "Nom");
        tempCsvHeader.put("state", "Etat");
        tempCsvHeader.put("companyName", "Marque");
        tempCsvHeader.put("nbPlayer", "NbJoueurs");
        tempCsvHeader.put("yearRecommended", "AgeJoueurs");
        tempCsvHeader.put("time", "TempsJeu");
        tempCsvHeader.put("type", "TypeJeu");
        tempCsvHeader.put("description0", "Descriptif");
        tempCsvHeader.put("description1", "Descriptif1");
        tempCsvHeader.put("description2", "Descriptif2");
        tempCsvHeader.put("description3", "Descriptif3");
        tempCsvHeader.put("author", "Auteur");
        tempCsvHeader.put("illustrator", "Illustrateur");
        tempCsvHeader.put("demoLink", "Demo");
        tempCsvHeader.put("location", "Emplacement");
        csvHeader = tempCsvHeader;
        Map<String, GameState> tempGameStates = new HashMap<>();
        tempGameStates.put("disponible", GameState.AVAILABLE);
        tempGameStates.put("réservé", GameState.REQUESTED);
        tempGameStates.put("emprunté", GameState.BORROWED);
        tempGameStates.put("non disponible", GameState.UNAVAILABLE);
        gameStates = tempGameStates;
    }

    @Nullable
    public static Game extractGameFromCSV(@NonNull String[] CSVColumns, @NonNull String[] line) {
        return extractGameFromCSV(Arrays.asList(CSVColumns), Arrays.asList(line));
    }

    @Nullable
    public static Game extractGameFromCSV(@NonNull List<String> CSVColumns, @NonNull List<String> line) {
        try {
            String id = extractBaseDataOf(CSVColumns ,line, "id");
            String name = extractBaseDataOf(CSVColumns ,line, "name");
            GameState state = gameStates.get(extractBaseDataOf(CSVColumns ,line, "state"));
            String companyName = extractBaseDataOf(CSVColumns ,line, "companyName");
            NbPlayer nbPlayer = NbPlayer.extractFrom(extractBaseDataOf(CSVColumns ,line, "nbPlayer"));
            YearRecommended yearRecommended = YearRecommended.extractFrom(extractBaseDataOf(CSVColumns ,line,
                "yearRecommended"));
            GameTime time = GameTime.extractFrom(extractBaseDataOf(CSVColumns ,line, "time"));
            String type = extractBaseDataOf(CSVColumns ,line, "type");
            String description0 = extractBaseDataOf(CSVColumns ,line, "description0");
            String description1 = extractBaseDataOf(CSVColumns ,line, "description1");
            String description2 = extractBaseDataOf(CSVColumns ,line, "description2");
            String description3 = extractBaseDataOf(CSVColumns ,line, "description3");
            String description = description0;
            if (description != null) {
                description = description1 != null ? String.format("%s%s", description, description1) : description;
                description = description2 != null ? String.format("%s%s", description, description2) : description;
                description = description3 != null ? String.format("%s%s", description, description3) : description;
            }
            String author = extractBaseDataOf(CSVColumns ,line, "author");
            String illustrator = extractBaseDataOf(CSVColumns ,line, "illustrator");
            String demoLink = extractBaseDataOf(CSVColumns ,line, "demoLink");
            String location = extractBaseDataOf(CSVColumns ,line, "location");
            return new Game(Objects.requireNonNull(id), Objects.requireNonNull(name), state, companyName, nbPlayer, yearRecommended, time, type,
                description, author, illustrator, demoLink, location);
        } catch (Exception ignored) {
            return null;
        }
    }

    @Nullable
    protected static String extractBaseDataOf(@NonNull List<String> CSVColumns, @NonNull List<String> line,
                                              @NonNull String headerName) {
        String baseString = line.get(CSVColumns.indexOf(csvHeader.get(headerName)));
        return !baseString.equalsIgnoreCase("") ? baseString : null;
    }
}
