package lp.projects.init.serveur.responses;

import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;

/**
 * Class represent Handler for my Error
 */
@RestControllerAdvice
@RestController
public class MyErrorController implements ErrorController {

    final String PATH = "/error";

    /**
     * Handler of Exception Throw
     * @param exception Exception throw
     * @return error
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Error> handleError(@NonNull Exception exception) {
        if (exception instanceof Error)
            return new ResponseEntity<>((Error) exception, HttpStatus.valueOf(((Error) exception).error_code));
        exception.printStackTrace();
        if (exception instanceof HttpMessageConversionException || exception instanceof ServletException)
            return new ResponseEntity<>(new BadRequestException(), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(new Error(HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Basic route of /error
     */
    @Hidden
    @RequestMapping(value = PATH)
    public void pageNotSet() throws NotFoundException {
        throw new NotFoundException();
    }

    /**
     * For get error Path
     * @return errorPath
     */
    @Override
    @NonNull
    public String getErrorPath() {
        return PATH;
    }
}