package lp.projects.init.serveur.core.loan;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.game.MinimalGame;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Class represent Loans Related to specific Game
 */
@Schema(description = "Loans Related to specific Game")
public class LoanRelated {

    @Schema(description = "All Loans related to this game")
    protected List<ReduceLoan> allLoansRelated;
    @Schema(description = "Specific Game related to this object")
    protected MinimalGame gameRelated;
    @Schema(description = "Current Loan related to this Game if applicable")
    protected ReduceLoan currentLoanRelated;

    /**
     * Default Constructor of LoanRelated
     * @param allLoansRelated List of Loans represent all loans related to this game
     * @param gameRelated MinimalGame represent this game
     * @param currentLoanRelated Loan represent current loan if exist
     */
    @JsonCreator
    public LoanRelated(@NonNull List<ReduceLoan> allLoansRelated, @Nullable MinimalGame gameRelated,
                       @Nullable ReduceLoan currentLoanRelated) {
        if (gameRelated == null)
            throw new IllegalArgumentException("LoanRelated required valid game.");
        this.allLoansRelated = allLoansRelated;
        this.gameRelated = gameRelated;
        this.currentLoanRelated = currentLoanRelated;
    }

    /**
     * Basic Constructor of LoanRelated
     * @param allLoansRelated List of Loans represent all loans related to this game
     * @param gameRelated MinimalGame represent this game
     * @param currentLoanRelated Loan represent current loan if exist
     */
    public LoanRelated(@NonNull List<Loan> allLoansRelated, @Nullable MinimalGame gameRelated,
                       @Nullable Loan currentLoanRelated) {
        if (gameRelated == null)
            throw new IllegalArgumentException("LoanRelated required valid game.");
        this.allLoansRelated = new ArrayList<>();
        for (Loan loan : allLoansRelated)
            this.allLoansRelated.add(new ReduceLoan(loan));
        this.gameRelated = gameRelated;
        this.currentLoanRelated = currentLoanRelated != null ? new ReduceLoan(currentLoanRelated) : null;
    }

    /**
     * Basic Constructor of LoanRelated
     * @param allLoansRelated List of Loans represent all loans related to this game
     * @param currentLoanRelated Loan represent current loan if exist
     */
    public LoanRelated(@NonNull List<Loan> allLoansRelated, @Nullable Loan currentLoanRelated) {
        this(allLoansRelated,
            currentLoanRelated != null ? currentLoanRelated.getGame() :
                (allLoansRelated.size() > 0 ? allLoansRelated.get(0).getGame() : null),
            currentLoanRelated);
    }

    /**
     * Basic Constructor of LoanRelated
     * @param loansRelated List of Loans represent all loans related to this game
     * @param gameRelated MinimalGame represent this game
     */
    public LoanRelated(@NonNull List<Loan> loansRelated, @NonNull MinimalGame gameRelated) {
        this.allLoansRelated = new ArrayList<>();
        for (Loan loan : loansRelated)
            this.allLoansRelated.add(new ReduceLoan(loan));
        this.currentLoanRelated = null;
        for (Loan loan : loansRelated)
            if (!loan.getState().equals(State.RENDERED))
                this.currentLoanRelated = new ReduceLoan(loan);
        this.gameRelated = gameRelated;
    }

    /**
     * Basic Constructor of LoanRelated
     * @param gameRelated MinimalGame represent this game
     */
    public LoanRelated(@NonNull MinimalGame gameRelated) {
        this(new ArrayList<>(), gameRelated, (Loan) null);
    }

    /**
     * Basic Constructor of LoanRelated
     * @param loansRelated List of Loans represent all loans related to this game
     */
    public LoanRelated(@NonNull List<Loan> loansRelated) {
        this.allLoansRelated = new ArrayList<>();
        for (Loan loan : loansRelated)
            this.allLoansRelated.add(new ReduceLoan(loan));
        this.currentLoanRelated = null;
        for (Loan loan : loansRelated)
            if (!loan.getState().equals(State.RENDERED))
                this.currentLoanRelated = new ReduceLoan(loan);
        this.gameRelated = currentLoanRelated != null ? currentLoanRelated.getGame() :
                (allLoansRelated.size() > 0 ? allLoansRelated.get(0).getGame() : null);
        if (gameRelated == null)
            throw new IllegalArgumentException("LoanRelated required valid game.");
    }

    /**
     * Getter for get allLoansRelated
     * @return allLoansRelated
     */
    @NonNull
    public List<ReduceLoan> getAllLoansRelated() {
        return new ArrayList<>(allLoansRelated);
    }

    /**
     * Getter for get currentLoanRelated
     * @return currentLoanRelated
     */
    @Nullable
    public ReduceLoan getCurrentLoanRelated() {
        return currentLoanRelated;
    }

    /**
     * Getter for get gameRelated
     * @return gameRelated
     */
    @NonNull
    public MinimalGame getGameRelated() {
        return gameRelated;
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof LoanRelated)) return false;
        LoanRelated loanRelated = (LoanRelated) o;
        return hashCode() == loanRelated.hashCode();
    }

    /**
     * Method for get HashCode of this LoanRelated
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(21, 53)
            .append(allLoansRelated).append(currentLoanRelated)
            .append(gameRelated).toHashCode();
    }

    /**
     * Method for Write LoanRelated
     * @return loanRelatedReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("LoanRelated to game %s.", gameRelated.toString());
    }
}
