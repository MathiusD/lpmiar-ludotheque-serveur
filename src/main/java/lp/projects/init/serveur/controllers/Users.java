package lp.projects.init.serveur.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lp.projects.init.serveur.DBRepo;
import lp.projects.init.serveur.core.auth.Token;
import lp.projects.init.serveur.core.game.GameSchema;
import lp.projects.init.serveur.core.game.MinimalGame;
import lp.projects.init.serveur.core.loan.Loan;
import lp.projects.init.serveur.core.loan.MinimalLoan;
import lp.projects.init.serveur.core.user.User;
import lp.projects.init.serveur.core.user.UserOAuth;
import lp.projects.init.serveur.core.user.UserSchema;
import lp.projects.init.serveur.responses.*;
import lp.projects.init.serveur.responses.Error;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static lp.projects.init.serveur.core.game.GameSchema.ALL_SCHEMA;

/**
 * Class represent Controller of Users
 */
@RestController
public class Users extends DBRepo {

    /**
     * For get User by id
     * @param token String represent Token for identify your account
     * @param id String represent id of User research
     * @return user
     */
    @CrossOrigin
    @Operation(summary = "Get a User by its id")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Found the User",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = User.class)
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        ),
        @ApiResponse(responseCode = "404", description = "User not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Error.class)
            )
        )
    })
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    @NonNull
    public User user(@Parameter(description = "Token for identify your account", example = "abcEFG145")
                            @RequestHeader(required = false) @Nullable String token,
                            @Parameter(description = "Id of user you are looking for",
                                example = "T256") @PathVariable(value="id") @NonNull String id) throws Error {
        Token userToken = identifyToken(token);
        User user = userRepository.findById(id).orElseThrow(
            new NotFoundException(String.format("User with the given id (%s) was not found", id)));
        grantAccess(userToken, String.format("/users/%s", id), user);
        return user;
    }

    /**
     * For get loans of my User
     * @param token String represent Token for identify your account
     * @param id String represent Id of user you are looking for
     * @param state String represent State of loans you are looking for
     * @param gameSchema GameSchema represent Game Schema for manage amount of data received
     * @param endAt Long represent Timestamp of endAt of a loan you are looking for
     * @param startedAt Long represent Timestamp of startedAt end of a loan you are looking for
     * @param gameId Integer represent Id related to Game of loans you are looking for
     * @param since Long represent Timestamp for extract only loan created since this timestamp
     * @param before Long represent Timestamp for extract only loan created before this timestamp
     * @return loans
     */
    @CrossOrigin
    @Operation(summary = "Get your loans")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Your Loans",
            content = @Content(mediaType = "application/json",
                array = @ArraySchema(uniqueItems = true, schema = @Schema(implementation = MinimalLoan.class))
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        ),
        @ApiResponse(responseCode = "404", description = "User Not Found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Error.class))
        )
    })
    @RequestMapping(value = "/users/{id}/loans", method = RequestMethod.GET)
    @NonNull
    public List<MinimalLoan> loans(@Parameter(description = "Token for identify your account",
                                        example = "abcEFG145") @RequestHeader(required = false) @Nullable
                                   String token, @Parameter(description = "Id of user you are looking for",
                                        example = "T256") @PathVariable(value="id") @NonNull String id,
                                   @Parameter(description = "State of loans you are looking for",
                                        example = "requested",
                                        examples = {
                                            @ExampleObject(value = "requested",
                                                description = "Game related to this Loan are rendered"),
                                            @ExampleObject(value = "at_home",
                                                description = "Game related to this Loan are at home"),
                                            @ExampleObject(value = "waiting",
                                                description = "Game related to this Loan are at library and user can pick her game"),
                                            @ExampleObject(value = "requested",
                                                description = "Game related to this Loan are requested only")
                                        }
                                   ) @RequestParam(required = false) @Nullable String state,
                                   @Parameter(description = "Game Schema for manage amount of data received",
                                        example = "fullGame") @RequestParam(required = false) @Nullable
                                   String gameSchema, @RequestParam(required = false)
                                   @Parameter(description = "Timestamp of endAt of a loan you are looking for",
                                        example = "1577836800") @Nullable Long endAt,
                                   @RequestParam(required = false) @Parameter(
                                        description = "Timestamp of startedAt end of a loan you are looking for",
                                        example = "1610534377339") @Nullable Long startedAt,
                                   @Parameter(
                                        description = "Id related to Game of loans you are looking for",
                                        example = "T111") @RequestParam(required = false)
                                   @Nullable String gameId, @RequestParam(required = false)
                                   @Parameter(
                                        description = "Timestamp for extract only loan created since this timestamp",
                                        example = "1577836800")
                                   @Nullable Long since, @RequestParam(required = false)
                                   @Parameter(
                                        description = "Timestamp for extract only loan created before this timestamp",
                                        example = "1577836800")
                                   @Nullable Long before) throws Error {
        Token userToken = identifyToken(token);
        List<MinimalLoan> loans = new ArrayList<>();
        GameSchema schemaUsed = GameSchema.MINIMAL_DATA;
        for (GameSchema schema : ALL_SCHEMA)
            if (schema.name.equalsIgnoreCase(gameSchema))
                schemaUsed = schema;
        User user = userRepository.findById(id).orElseThrow(
            new NotFoundException(String.format("User with the given id (%s) was not found", id)));
        for (Loan loan : loanRepository.findAll())
            if (loan.getUser().equals(user))
                loans.add(new MinimalLoan(loan, gameRepository.getOne(loan.getGame().getId()), schemaUsed));
        grantAccess(userToken, String.format("/users/%s/loans", id), user);
        loans.removeIf(loan ->
            (state != null && !loan.getState().getAccessKey().equalsIgnoreCase(state)) ||
            (endAt != null && !(loan.getEndAt().equals(endAt))) ||
            (startedAt != null && !(loan.getStartedAt().equals(startedAt))) ||
            (gameId != null && !loan.getGame().getId().equalsIgnoreCase(gameId)) ||
            (since != null && since > loan.getStartedAt()) ||
            (before != null && before < loan.getStartedAt())
        );
        return loans;
    }

    /**
     * For get OAuth related to this User
     * @param token String represent Token for identify your account
     * @param id String represent id of User research
     * @return user
     */
    @CrossOrigin
    @Operation(summary = "For get OAuth related to this User")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "OAuth related to this User",
            content = @Content(mediaType = "application/json",
                    array = @ArraySchema(uniqueItems = true,
                        schema = @Schema(implementation = UserOAuth.class)
                )
            )
        ),
        @ApiResponse(responseCode = "401", description = "Not Connected",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = InvalidTokenException.class)
            )
        ),
        @ApiResponse(responseCode = "403", description = "Not Allowed",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = AccessDeniedException.class))
        ),
        @ApiResponse(responseCode = "404", description = "User not found",
            content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Error.class)
            )
        )
    })
    @RequestMapping(value = "/users/{id}/oauth", method = RequestMethod.GET)
    @NonNull
    public List<UserOAuth> userOAuth(@Parameter(description = "Token for identify your account",
                                        example = "abcEFG145") @RequestHeader(required = false)
                                     @Nullable String token,
                                     @Parameter(description = "Id of user you are looking for",
                                        example = "T256") @PathVariable(value="id")
                                     @NonNull String id) throws Error {
        Token userToken = identifyToken(token);
        User user = userRepository.findById(id).orElseThrow(
                new NotFoundException(String.format("User with the given id (%s) was not found", id)));
        grantAccess(userToken, String.format("/users/%s/oauth", id), user);
        List<UserOAuth> oAuthList = new ArrayList<>();
        for(UserOAuth userOAuth : userOAuthRepo.findAll())
            if (userOAuth.getUserRelated().equals(user))
                oAuthList.add(userOAuth);
        return oAuthList;
    }

    /**
     * For get user schema for manage amount of data received
     * @return schemas
     */
    @CrossOrigin
    @Operation(summary = "Get users Schema for manage amount of data received")
    @ApiResponse(responseCode = "200", description = "View All Users Schema",
        content = @Content(mediaType = "application/json",
            array = @ArraySchema(uniqueItems = true, schema = @Schema(implementation = UserSchema.class))
        )
    )
    @RequestMapping(value = "/users/schemas", method = RequestMethod.GET)
    @NonNull
    public static List<UserSchema> schemas() {
        return UserSchema.ALL_SCHEMA;
    }
}
