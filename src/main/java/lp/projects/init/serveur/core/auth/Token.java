package lp.projects.init.serveur.core.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.core.user.MinimalUser;
import lp.projects.init.serveur.db.BaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.UUID;

/**
 * Class represent Token of User
 */
@Entity
@Table(name = "token")
@Schema(description = "Token of User")
public class Token extends BaseEntity {

    @ManyToOne(optional = false, targetEntity = MinimalUser.class, fetch= FetchType.EAGER)
    @JoinColumn(name = "user_id", nullable = false)
    @Schema(description = "User related to this Token")
    protected MinimalUser user;
    @Id
    @Column(name = "token_value")
    @Schema(description = "Token value")
    protected String token;

    /**
     * Shit Constructor for Token Stored in DB
     * Used only by reflexion !
     */
    protected Token() {
        super();
    }

    /**
     * Default constructor for Token
     * @param user User represent user related to this token
     * @param token String represent token value
     */
    @JsonCreator
    @PersistenceConstructor
    public Token(@NonNull MinimalUser user, @NonNull String token) {
        this.user = user.getMinimalUser();
        this.token = token;
    }

    /**
     * Default constructor for Token
     * @param user User represent user related to this token
     */
    public Token(@NonNull MinimalUser user) {
        this(user, UUID.randomUUID().toString());
    }

    /**
     * Getter for get user
     * @return user
     */
    public MinimalUser getUser() {
        return new MinimalUser(user);
    }

    /**
     * Getter for get token
     * @return token
     */
    public String getToken() {
        return token;
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof Token)) return false;
        Token token = (Token) o;
        return hashCode() == token.hashCode();
    }

    /**
     * Method for get HashCode of this Token
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(token).toHashCode();
    }

    /**
     * Method for Write Token
     * @return tokenReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("Token for %s", user.toString());
    }
}
