package lp.projects.init.serveur.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Class represent Bad Request Exception
 */
@CrossOrigin
@ResponseBody
@ResponseStatus(HttpStatus.CONFLICT)
@Schema(description = "Conflict Error")
public class ConflictException extends Error {

    @Hidden
    @NonNull
    public static HttpStatus getBaseHttpStatus() {
        return HttpStatus.CONFLICT;
    }

    /**
     * Basic Constructor for ConflictException
     * @param raisonProvided String represent specific message for this error
     */
    @JsonCreator
    public ConflictException(@Nullable String raisonProvided) {
        super(getBaseHttpStatus(), raisonProvided);
    }

    /**
     * Basic Constructor for ConflictException
     */
    public ConflictException() {
        this(null);
    }
}