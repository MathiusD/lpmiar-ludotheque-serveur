package lp.projects.init.serveur.core.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test NbPlayer
 */
public class NbPlayerTest {

    /**
     * For test Instantiation of NbPlayer
     */
    @Test
    public void testNbPlayerInstantiation() {
        NbPlayer baseNbPlayer = new NbPlayer(1, 2);
        NbPlayer targetNbPlayer = new NbPlayer(1, 2, false);
        assertEquals(baseNbPlayer, targetNbPlayer);
        targetNbPlayer = new NbPlayer(1, 2, true);
        assertNotEquals(baseNbPlayer, targetNbPlayer);
        baseNbPlayer = new NbPlayer(1);
        targetNbPlayer = new NbPlayer(1, null, true);
        assertEquals(baseNbPlayer, targetNbPlayer);
        targetNbPlayer = new NbPlayer(1, true);
        assertEquals(baseNbPlayer, targetNbPlayer);
    }

    /**
     * For test Exception are throw if arguments are incorrect
     */
    @Test
    public void testNbPlayerFailedInstantiation() {
        assertThrows(IllegalArgumentException.class, () -> new NbPlayer(2, 1),
            "max must be superior or equals to min");
    }
}
