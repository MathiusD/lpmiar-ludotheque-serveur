VPN=$1;
USER=$2;
MDP=$3;
ROLE=$4
DestFile=$5;
if ! [ -f "${DestFile}" ]; then
  echo "${MDP}" >> "${DestFile}";
  echo "${ROLE}" >> "${DestFile}";
fi
openconnect "${VPN}" --juniper --user="${USER}" --passwd-on-stdin -b < "${DestFile}";