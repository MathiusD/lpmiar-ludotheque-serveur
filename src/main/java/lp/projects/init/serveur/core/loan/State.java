package lp.projects.init.serveur.core.loan;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Class represent State of Loan
 */
@Schema(description = "State of Loan")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY)
@JsonIgnoreProperties({"permittedStateChange"})
public class State extends MinimalState {

    @Schema(description = "Description of this State")
    protected String description;

    /**
     * Shit Constructor for State Stored in DB
     * Used only by reflexion !
     */
    protected State() {
        super();
    }

    /**
     * Basic Constructor for State
     * @param name String represent name of State
     */
    public State(@NonNull String name) {
        this(name, (String) null);
    }

    /**
     * Basic Constructor for State
     * @param name String represent name of State
     * @param description String represent name of State
     */
    @JsonCreator
    public State(@NonNull String name, @Nullable String description) {
        super(name);
        State foundState = null;
        for (State stat: State.ALL_STATES)
            if (stat.name.equalsIgnoreCase(name) &&
                    (description == null || stat.description.equalsIgnoreCase(description)) && foundState == null)
                foundState = stat;
        if (foundState == null)
            throw new IllegalArgumentException("This State don't match with any existing State");
        this.description = foundState.description;
    }

    /**
     * Basic Constructor for State
     * @param name String represent name of State
     * @param states many States represents states where changes are allowed
     */
    public State(@NonNull String name, @NonNull MinimalState... states) {
        this(name, Arrays.asList(states));
    }

    /**
     * Basic Constructor for State
     * @param name String represent name of State
     * @param states Collection of State or Child represents states where changes are allowed
     */
    public State(@NonNull String name, @NonNull Collection<? extends MinimalState> states) {
        this(name, null, states);
    }

    /**
     * Basic Constructor for State
     * @param name String represent name of State
     * @param description String represent name of State
     * @param states many States represents states where changes are allowed
     */
    public State(@NonNull String name, @Nullable String description, @NonNull MinimalState... states) {
        this(name, description, Arrays.asList(states));
    }

    /**
     * Basic Constructor for State
     * @param name String represent name of State
     * @param description String represent name of State
     * @param states Collection of State or Child represents states where changes are allowed
     */
    public State(@NonNull String name, @Nullable String description,
                 @NonNull Collection<? extends MinimalState> states) {
        super(name, states);
        this.description = description;
    }

    public final static State RENDERED = new State("Rendered", "Game related to this Loan are rendered",
            (MinimalState) null);
    public final static State AT_HOME = new State("At Home", "Game related to this Loan are at home",
            RENDERED);
    public final static State WAITING = new State("Waiting",
            "Game related to this Loan are at library and user can pick her game", AT_HOME);
    public final static State REQUESTED = new State("Requested",
            "Game related to this Loan are requested only", WAITING);
    public final static List<State> ALL_STATES;
    static {
        List<State> temp = new ArrayList<>();
        temp.add(REQUESTED);
        temp.add(WAITING);
        temp.add(AT_HOME);
        temp.add(RENDERED);
        ALL_STATES = temp;
    }

    /**
     * Getter for get description
     * @return description
     */
    @NonNull
    public String getDescription() {
        return description;
    }

}
