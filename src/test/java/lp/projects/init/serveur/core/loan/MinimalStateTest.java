package lp.projects.init.serveur.core.loan;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test MinimalState
 */
public class MinimalStateTest {

    /**
     * For test Instantiation of MinimalState
     */
    @Test
    public void testMinimalStateInstantiation() {
        MinimalState firstState = State.REQUESTED.getMinimalState();
        assertEquals(firstState, new MinimalState(State.REQUESTED));
        assertEquals(new MinimalState(State.REQUESTED.name), State.REQUESTED.getMinimalState());
        assertThrows(IllegalArgumentException.class, () -> new MinimalState("truc"),
                "This MinimalState don't match with any existing State");
    }

    /**
     * For test changeArePermitted method
     */
    @Test
    public void testChangeArePermitted() {
        assertTrue(State.REQUESTED.changeArePermitted(State.WAITING));
        assertFalse(State.REQUESTED.changeArePermitted(State.AT_HOME));
    }
}
