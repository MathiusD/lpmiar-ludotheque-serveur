package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Class represent result of Upload Games
 */
@Schema(description = "Result of Upload Games")
public class GameUploadSummary {

    @Schema(description = "Message related for upload", example = "Successful uploaded")
    public final String message;
    @Schema(description = "Games uploaded after this request")
    public final List<Game> gamesUploaded;
    @Schema(description = "Games not uploaded after this request because game with id is already in base")
    public final List<Game> gamesNotUploaded;

    /**
     * Basic Constructor of GameUploadSummary
     * @param gamesUploaded List of Game uploaded
     * @param gamesNotUploaded List of Game not uploaded
     * @param message String represent message of this upload
     */
    @JsonCreator
    public GameUploadSummary(@NonNull List<Game> gamesUploaded, @NonNull List<Game> gamesNotUploaded,
                             @Nullable String message) {
        this.message = message;
        this.gamesUploaded = gamesUploaded;
        this.gamesNotUploaded = gamesNotUploaded;
    }

    /**
     * Basic Constructor of GameUploadSummary
     * @param gamesUploaded List of Game related to this upload
     * @param gamesNotUploaded List of Game not uploaded
     */
    public GameUploadSummary(@NonNull List<Game> gamesUploaded, @NonNull List<Game> gamesNotUploaded) {
        this(gamesUploaded, gamesNotUploaded,
            String.format("%s games are successfully uploaded%s.",
                !gamesUploaded.isEmpty() ? String.valueOf(gamesUploaded.size()) : "No",
                !gamesNotUploaded.isEmpty() ?
                    String.format(" and %d games aren't uploaded", gamesNotUploaded.size()) : "")
        );
    }

    /**
     * Basic Constructor of GameUploadSummary
     * @param gamesUploaded List of Game related to this upload
     */
    public GameUploadSummary(@NonNull List<Game> gamesUploaded) {
        this(gamesUploaded, new ArrayList<>());
    }

    /**
     * Basic Constructor of GameUploadSummary
     * @param message String represent message of this upload
     */
    public GameUploadSummary(@Nullable String message) {
        this(new ArrayList<>(), new ArrayList<>(), message);
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof GameUploadSummary)) return false;
        GameUploadSummary gameUploadSummary = (GameUploadSummary) o;
        return hashCode() == gameUploadSummary.hashCode();
    }

    /**
     * Method for get HashCode of this GameUploadSummary
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(message).append(gamesUploaded).toHashCode();
    }

    /**
     * Method for Write GameUploadSummary
     * @return gameUploadSummaryReadable
     */
    @Override
    @NonNull
    public String toString(){
        return String.format("GameUploadSummary for games %s (With message : %s)", gamesUploaded.toString(), message);
    }
}
