package lp.projects.init.serveur.db.game;

import lp.projects.init.serveur.core.game.ShortGame;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface represent Repo for ShortGame
 */
public interface ShortGameRepo extends JpaRepository<ShortGame, String> {
}
