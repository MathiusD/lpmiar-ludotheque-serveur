default_target: start

clear: clean

clean:
	@mvn clean

compile : clean
	@mvn compile

test: compile
	@mvn test

start: compile
	@mvn spring-boot:run

package: compile
	@mvn package

install: package
	@mvn install

deployMathius: package
	@mvn tomcat7:redeploy-only -Dtarget=mathius-production

deployIUT: package
	@mvn tomcat7:redeploy-only -Dtarget=iut-production

deployMathiusTesting: package
	@mvn tomcat7:redeploy-only

deployIUTTesting: package
	@mvn tomcat7:redeploy-only -Dtarget=iut-develop
