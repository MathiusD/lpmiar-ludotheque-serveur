package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.db.BaseEntity;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.*;

import java.util.UUID;

import static javax.persistence.InheritanceType.SINGLE_TABLE;

/**
 * Class represent Base informations of this Game of this library
 */
@Entity
@Inheritance(strategy=SINGLE_TABLE)
@DiscriminatorValue("MIN_GAME")
@Schema(description = "Base Information of Game of this Library")
public class MinimalGame extends BaseEntity {

    public final static GameState DEFAULT_STATE = GameState.AVAILABLE;

    @Id
    @Column(name = "game_id")
    @Schema(example = "T111", description = "Id for identify this Game")
    protected String id;
    @Column(name = "game_name")
    @Schema(example = "Seven Wonders", description = "Name of this Game")
    protected String name;
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "name", column = @Column(name = "game_state_name")),
        @AttributeOverride(name = "isAvailable", column = @Column(name = "game_state_isAvailable")),
        @AttributeOverride(name = "note", column = @Column(name = "game_state_note"))
    })
    @Schema(description = "State of this game")
    protected GameState state;

    /**
     * Shit Constructor for MinimalGame Stored in DB
     * Used only by reflexion !
     */
    protected MinimalGame() {
        super();
    }

    /**
     * Basic constructor for MinimalGame
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param state GameState represent state of this game
     */
    @JsonCreator
    @PersistenceConstructor
    public MinimalGame(@NonNull String id, @NonNull String name, @NonNull GameState state) {
        this.id = id;
        this.name = name;
        this.state = state;
    }

    /**
     * Basic constructor for MinimalGame without State
     * @param name String represent Name of Game
     * @param state GameState represent state of this game
     */
    public MinimalGame(@NonNull String name, @NonNull GameState state) {
        this(UUID.randomUUID().toString(), name, state);
    }

    /**
     * Basic constructor for MinimalGame without State
     * @param id String represent id of Game
     * @param name String represent Name of Game
     */
    public MinimalGame(@NonNull String id, @NonNull String name) {
        this(id, name, DEFAULT_STATE);
    }

    /**
     * Basic constructor for MinimalGame without State
     * @param name String represent Name of Game
     */
    public MinimalGame(@NonNull String name) {
        this(name, DEFAULT_STATE);
    }

    /**
     * Constructor of MinimalGame by MinimalGame or child
     * @param otherGame MinimalGame used for extract informations
     */
    public MinimalGame(@NonNull MinimalGame otherGame) {
        this(otherGame.getId(), otherGame.getName(), otherGame.getGameState());
    }

    /**
     * Getter for state of Game
     * @return state
     */
    @NonNull
    public GameState getGameState() {
        return state;
    }

    /**
     * Setter for state of Game
     * @param state State represent new State of this game
     */
    public void setGameState(@NonNull GameState state) {
        this.state = state;
    }

    /**
     * Getter for name of Game
     * @return name
     */
    @NonNull
    public String getName() {
        return name;
    }
    /**
     * Getter for id of Game
     * @return id
     */
    @NonNull
    public String getId() {
        return id;
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof MinimalGame)) return false;
        MinimalGame minimalGame = (MinimalGame) o;
        return hashCode() == minimalGame.hashCode();
    }

    /**
     * Method for get HashCode of this MinimalGame
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id).toHashCode();
    }

    /**
     * Method for Write MinimalGame
     * @return minimalGameReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("Game %s with id %s", name, id);
    }

    /**
     * Method for generate MinimalGame related to this Game
     * @return minimalGame
     */
    @JsonIgnore
    @NonNull
    public MinimalGame getMinimalGame() {
        return new MinimalGame(this);
    }
}
