package lp.projects.init.serveur.core.loan;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Class represent Loan Update Arguments
 */
@Schema(description = "Json for update Loan")
public class LoanUpdate {

    @Schema(description = "accessKey related to State for update", example = "Waiting")
    public final String state;

    /**
     * Basic constructor for LoanUpdate
     * @param state String represent accessKey for State update
     */
    @JsonCreator
    public LoanUpdate(@NonNull String state) {
        this.state = state;
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof LoanUpdate)) return false;
        LoanUpdate loanUpdate = (LoanUpdate) o;
        return hashCode() == loanUpdate.hashCode();
    }

    /**
     * Method for get HashCode of this LoanUpdate
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(state).toHashCode();
    }

    /**
     * Method for Write LoanUpdate
     * @return loanUpdateReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("LoanUpdate requested change state for %s", state);
    }
}
