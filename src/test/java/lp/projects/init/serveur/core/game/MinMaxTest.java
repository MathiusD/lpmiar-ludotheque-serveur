package lp.projects.init.serveur.core.game;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test MinMax
 */
public class MinMaxTest {

    /**
     * For test Instantiation of MinMax
     */
    @Test
    public void testMinMaxInstantiation() {
        MinMax<Integer> baseMinMax = new MinMax<>(1, 2);
        MinMax<Integer> targetMinMax = new MinMax<>(1, 2, false);
        assertEquals(baseMinMax, targetMinMax);
        targetMinMax = new MinMax<>(1, 2, true);
        assertNotEquals(baseMinMax, targetMinMax);
        baseMinMax = new NbPlayer(1);
        targetMinMax = new MinMax<>(1, null, true);
        assertEquals(baseMinMax, targetMinMax);
        targetMinMax = new MinMax<>(1, true);
        assertEquals(baseMinMax, targetMinMax);
    }

    /**
     * For test Exception are throw if arguments are incorrect
     */
    @Test
    public void testMinMaxFailedInstantiation() {
        assertThrows(IllegalArgumentException.class, () -> new MinMax<>(2, 1),
                "max must be superior or equals to min");
        assertThrows(IllegalArgumentException.class, () -> new MinMax<>(2L, 1L),
                "max must be superior or equals to min");
    }
}
