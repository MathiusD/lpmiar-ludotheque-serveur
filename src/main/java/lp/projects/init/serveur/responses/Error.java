package lp.projects.init.serveur.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.function.Supplier;

/**
 * Class represent HTTP Error may throw
 */
@Schema(description = "HTTP Error may throw")
@JsonIgnoreProperties({"cause", "stackTrace", "localizedMessage", "suppressed" })
public class Error extends Exception implements Supplier<Error> {

    @Nullable
    @Hidden
    public static HttpStatus getBaseHttpStatus() {
        return null;
    }

    @Schema(description = "HTTP code related to this Error", example = "404")
    public final int error_code;
    @Schema(description = "Raison of throw this error if given", example = "Game Not Found")
    public final String raisonProvided;

    /**
     * Method for return message of this Error
     * @return message
     */
    @Schema(description = "Message related to this Error", example = "Not Found")
    @Override
    public String getMessage() {
        return super.getMessage();
    }

    /**
     * Basic Constructor for Error
     * @param message String represent base message of error
     * @param error_code Integer represent code of this error
     * @param raisonProvided String represent specific message for this error
     */
    @JsonCreator
    public Error(@NonNull String message, int error_code, @Nullable String raisonProvided) {
        super(message);
        this.error_code = error_code;
        this.raisonProvided = raisonProvided;
    }

    /**
     * Basic Constructor for Error
     * @param message String represent base message of error
     * @param error_code Integer represent code of this error
     */
    public Error(@NonNull String message, int error_code) {
        this(message, error_code, null);
    }

    /**
     * Basic Constructor for Error
     * @param status HttpStatus represent status related to this error
     * @param raisonProvided String represent specific message for this error
     */
    public Error(@NonNull HttpStatus status, @Nullable String raisonProvided) {
        this(status.getReasonPhrase(), status.value(), raisonProvided);
    }

    /**
     * Basic Constructor for Error
     * @param status HttpStatus represent status related to this error
     */
    public Error(@NonNull HttpStatus status) {
        this(status, null);
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof Error)) return false;
        Error error = (Error) o;
        return hashCode() == error.hashCode();
    }

    /**
     * Method for get HashCode of this Error
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(error_code).append(raisonProvided).append(getMessage()).toHashCode();
    }

    @Override
    public Error get() {
        return this;
    }

    /**
     * Method for Write Error
     * @return errorReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("Error supplied with %d errorCode%s" , error_code,
            raisonProvided != null ? String.format("and with raisonProvided %s", raisonProvided) : "");
    }
}
