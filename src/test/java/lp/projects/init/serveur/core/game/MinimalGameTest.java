package lp.projects.init.serveur.core.game;

import org.junit.jupiter.api.Test;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test MinimalGame
 */
public class MinimalGameTest {

    /**
     * Assert MinimalGames are exactly same attributes value
     * @param first MinimalGame represent first game
     * @param second MinimalGame represent second game
     */
    public static void assertGamesAreStrictlyEquals(@NonNull MinimalGame first, @NonNull MinimalGame second) {
        assertGamesAreStrictlyEquals(first, second, null);
    }

    /**
     * Assert MinimalGames are exactly same attributes value
     * @param first MinimalGame represent first game
     * @param second MinimalGame represent second game
     * @param message String represent message send if assertion are false
     */
    public static void assertGamesAreStrictlyEquals(@NonNull MinimalGame first, @NonNull MinimalGame second, @Nullable String message) {
        assertEquals(first, second, message);
        assertEquals(first.id, second.id, message);
        assertEquals(first.name, second.name, message);
        assertEquals(first.getGameState(), second.getGameState(), message);
    }

    /**
     * For test Instantiation of MinimalGame
     */
    @Test
    public void testMinimalGameInstantiation() {
        MinimalGame firstMinGame = new MinimalGame("ABC", "Example");
        assertEquals(MinimalGame.DEFAULT_STATE, firstMinGame.getGameState());
        MinimalGame tempMinGame = new MinimalGame(firstMinGame);
        assertGamesAreStrictlyEquals(firstMinGame, tempMinGame);
        firstMinGame = new MinimalGame("ABC", "Example", GameState.BORROWED);
        tempMinGame = new MinimalGame(firstMinGame);
        assertGamesAreStrictlyEquals(firstMinGame, tempMinGame);
    }
}
