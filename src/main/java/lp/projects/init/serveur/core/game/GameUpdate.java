package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * Class represent Game Update Arguments
 */
@Schema(description = "Json for update Game")
public class GameUpdate {

    @Schema(description = "new GameState for update this Game (Option is available only if Game isAvailable)")
    public final GameState state;
    @Schema(description = "For describe/present the Game", example = "Wonderful Game")
    public final String description;
    @Schema(description = "Location of this game in library", example = "RDC")
    public final String location;

    /**
     * Basic constructor for GameUpdate
     * @param state GameState represent new GameState for Game update
     * @param description String represent description of this Game
     * @param location String represent location of this Game
     */
    @JsonCreator
    public GameUpdate(@Nullable GameState state, @Nullable String description, @Nullable String location) {
        this.state = state;
        this.description = description;
        this.location = location;
    }

    /**
     * Method for compare this object to other
     * @param o Object to compare
     * @return equals
     */
    @Override
    public boolean equals(@Nullable Object o) {
        if (this == o) return true;
        if (!(o instanceof GameUpdate)) return false;
        GameUpdate gameUpdate = (GameUpdate) o;
        return hashCode() == gameUpdate.hashCode();
    }

    /**
     * Method for get HashCode of this LoanUpdate
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(53, 43)
            .append(state).append(description).append(location).toHashCode();
    }

    /**
     * Method for Write GameUpdate
     * @return gameUpdateReadable
     */
    @Override
    @NonNull
    public String toString() {
        return String.format("GameUpdate requested change state for %s, description for %s and location for %s",
            state, description, location);
    }
}
