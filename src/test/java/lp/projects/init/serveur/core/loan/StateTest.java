package lp.projects.init.serveur.core.loan;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test State
 */
public class StateTest {
    /**
     * For test Instantiation of MinimalState
     */
    @Test
    public void testStateInstantiation() {
        assertEquals(new State(State.REQUESTED.name), State.REQUESTED);
        assertThrows(IllegalArgumentException.class, () -> new State("truc"),
                "This MinimalState don't match with any existing State");
        assertEquals(new State(State.REQUESTED.name, State.REQUESTED.description), State.REQUESTED);
        assertThrows(IllegalArgumentException.class, () -> new State(State.REQUESTED.name, "truc"),
                "This State don't match with any existing State");
    }
}
