package lp.projects.init.serveur.core.user;

import lp.projects.init.serveur.core.game.MinimalGame;
import lp.projects.init.serveur.core.loan.Loan;
import org.junit.jupiter.api.Test;
import org.springframework.lang.NonNull;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test User
 */
public class UserTest extends MinimalUserTest{

    /**
     * Assert Users are exactly same attributes value
     * @param first User represent first user
     * @param second User represent second user
     */
    public static void assertUsersAreStrictlyEquals(@NonNull User first, @NonNull User second) {
        assertUsersAreStrictlyEquals(first.getMinimalUser(), second.getMinimalUser());
        assertEquals(first.birthDate, second.birthDate);
        assertEquals(first.getAddress(), second.getAddress());
        assertEquals(first.getCity(), second.getCity());
    }

    /**
     * For test Instantiation of User
     */
    @Test
    public void testUserInstantiation() {
        User firstUser = new User("ABC", "Example", "truc",
                "truc@mail.org", "Truc", null, null,
                null);
        User tempUser = new User(firstUser);
        assertUsersAreStrictlyEquals(firstUser, tempUser);
        MinimalGame game = new MinimalGame("machin", "truc");
        Loan loan = new Loan("zef", game, firstUser, 42);
        assertUsersAreStrictlyEquals(firstUser, new User(loan));
        assertUsersAreStrictlyEquals(firstUser, new User(firstUser));
        assertUsersAreStrictlyEquals(firstUser.getMinimalUser(), new MinimalUser(firstUser));
        assertUsersAreStrictlyEquals(firstUser.getMinimalUser(), new User(firstUser.getMinimalUser()));
    }
}
