package lp.projects.init.serveur.core.loan;

import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import lp.projects.init.serveur.db.BaseEntity;
import org.springframework.lang.NonNull;

import javax.persistence.Embeddable;

@Embeddable
@Schema(description = "Base name for State of Loan")
public class StateName extends BaseEntity {

    @Schema(description = "Name for this State", example = "At Home")
    protected String name;

    /**
     * Shit Constructor for StateName Stored in DB
     * Used only by reflexion !
     */
    protected StateName() {
        super();
    }

    /**
     * Basic Constructor for MinimalState
     * @param name String represent name of State
     */
    @JsonCreator
    public StateName(@NonNull String name) {
        MinimalState foundState = null;
        for (MinimalState stat: State.ALL_STATES)
            if (stat.name.equalsIgnoreCase(name) && foundState == null)
                foundState = stat;
        if (foundState == null)
            throw new IllegalArgumentException("This MinimalState don't match with any existing State");
        this.name = foundState.name;
    }

    /**
     * Getter for get name
     * @return name
     */
    @NonNull
    public String getName() {
        return name;
    }

    /**
     * Method for Write StateName
     * @return stateNameReadable
     */
    @Override
    @NonNull
    public String toString(){
        return String.format("State of Loan %s", name);
    }
}
