package lp.projects.init.serveur;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import org.springframework.lang.NonNull;

import lp.projects.init.serveur.core.auth.Role;
import lp.projects.init.serveur.core.auth.Token;
import lp.projects.init.serveur.core.game.Game;
import lp.projects.init.serveur.core.game.GameSchema;
import lp.projects.init.serveur.core.game.GameState;
import lp.projects.init.serveur.core.game.GameTime;
import lp.projects.init.serveur.core.game.MinMax;
import lp.projects.init.serveur.core.game.MinimalGame;
import lp.projects.init.serveur.core.game.NbPlayer;
import lp.projects.init.serveur.core.loan.Loan;
import lp.projects.init.serveur.core.loan.MinimalLoan;
import lp.projects.init.serveur.core.loan.State;
import lp.projects.init.serveur.core.user.MinimalUser;
import lp.projects.init.serveur.core.user.MinimalUserWithLoans;
import lp.projects.init.serveur.core.user.User;
import lp.projects.init.serveur.core.user.UserOAuth;

/**
 * Class represent PlaceHolder and default Data
 */
public class DBData extends DBRepo {
    protected final User my = new User("T256", "Le Bricoleur", "Bob",
            "grandBob@mail.com", "Bob Le Grand", Role.MANAGER,
            new GregorianCalendar(2000, Calendar.OCTOBER, 27)
                    .getTime(), "107 bis rue truc", "Patelin Paumé");
    protected final User meToo =  new User("Le Pas Bricoleur", "Bob",
            "bob@mail.com", "Bobby",
            new GregorianCalendar(2003, Calendar.JULY, 12)
                    .getTime(), "2 Boulevard truc", "Patelin Pas Si Paumé");
    protected final Token myToken = new Token(my, "abcEFG145");
    protected final Token otherToken = new Token(meToo, "156ZRsfrg");
    protected final List<UserOAuth> oAuthList;
    protected final List<User> users;
    protected final List<Token> userTokens;
    protected final List<Game> games;
    protected final List<String> nameGame;
    protected final List<MinimalGame> minimalGames;
    protected final List<Loan> myLoans;
    protected final List<MinimalLoan> myMinimalLoans;
    protected final List<MinimalLoan> meTooMinimalLoans;
    protected final List<Loan> meTooLoans;
    protected final List<MinimalUserWithLoans> libraryData;
    protected final List<MinimalUser> libraryUsers;
    protected final List<Loan> libraryLoans;
    protected final List<User> libraryAllUsers;
    protected final List<String> types;
    protected final List<String> companyNames;
    protected final List<String> illustrators;
    protected final List<String> authors;
    protected final List<Long> endAt;
    protected final List<Long> startedAt;
    protected final List<String> gameId;
    protected final List<String> locations;
    protected final MinMax<Integer> nbPlayerMinMax;
    protected final MinMax<Long> startedAtMinMax;
    protected final MinMax<Long> endAtMinMax;

    /**
     * Default Constructor for DBData
     */
    public DBData() {
        this(false);
    }

    /**
     * Constructor for DBData
     * @param forTestPurpose Flag for initiate db with test data or not
     */
    public DBData(@NonNull Boolean forTestPurpose) {
        List<User> tempUser = new ArrayList<>();
        tempUser.add(meToo);
        tempUser.add(my);
        users = tempUser;
        List<UserOAuth> tempUserOAuth = new ArrayList<>();
        //Mathius Discord
        tempUserOAuth.add(new UserOAuth(my, "discord", "288566387234963456"));
        //luc Discord
        tempUserOAuth.add(new UserOAuth(my, "discord", "488395153942249472"));
        //Kuro Neko Discord
        tempUserOAuth.add(new UserOAuth(my, "discord", "325454360589107200"));
        //Taura Discord
        tempUserOAuth.add(new UserOAuth(my, "discord", "179282700597854208"));
        //Wadra Discord
        tempUserOAuth.add(new UserOAuth(my, "discord", "285092057809420289"));
        //Berdjgin GitHub
        tempUserOAuth.add(new UserOAuth(my, "github", "10314266"));
        //Lanoix Discord
        tempUserOAuth.add(new UserOAuth(my, "discord", "388341241701728257"));
        //ADH Discord
        tempUserOAuth.add(new UserOAuth(my, "discord", "709378589774184550"));
        //Féry Mathieu Google
        tempUserOAuth.add(new UserOAuth(my, "google", "101621294206727500191"));
        //Alban Goupil Google
        tempUserOAuth.add(new UserOAuth(my, "google", "115901621497166190391"));
        oAuthList = tempUserOAuth;
        List<Token> tempToken = new ArrayList<>();
        tempToken.add(otherToken);
        tempToken.add(myToken);
        userTokens = tempToken;
        List<Game> tempGames;
        if (!forTestPurpose)
            try {
                tempGames = Game.extractGamesFromCSV(Objects.requireNonNull(getClass().getClassLoader()
                        .getResource("base.csv")).getPath());
            } catch (Exception e) {
                tempGames = defaultGameValue();
            }
        else
            tempGames = defaultGameValue();
        games = tempGames;
        minimalGames = GameSchema.MINIMAL_DATA.matchingGameWithThisSchema(games);

        List<Loan> tempLoans = new ArrayList<>();
        tempLoans.add(new Loan("ABC",games.get(0), my, 4600));
        games.get(0).setGameState(GameState.REQUESTED);
        tempLoans.add(new Loan(games.get(1), my, 4600));
        games.get(1).setGameState(GameState.REQUESTED);
        tempLoans.add(new Loan(games.get(2), my, 946684800L,
                1577836800L, State.AT_HOME));
        games.get(2).setGameState(GameState.BORROWED);
        myLoans = tempLoans;
        List<MinimalLoan> tempMinimalLoans = new ArrayList<>();
        for (Loan laon : myLoans)
            tempMinimalLoans.add(laon.getMinimalLoan());
        myMinimalLoans = tempMinimalLoans;
        List<Loan> tempLoans2 = new ArrayList<>();
        tempLoans2.add(new Loan(games.get(3), meToo, 4600));
        games.get(3).setGameState(GameState.REQUESTED);
        Loan meTooLoan = new Loan(games.get(4), meToo, 4600);
        meTooLoan.setState(State.WAITING);
        games.get(4).setGameState(GameState.BORROWED);
        tempLoans2.add(meTooLoan);
        meTooLoans = tempLoans2;
        List<MinimalLoan> tempMeTooMinimalLoans = new ArrayList<>();
        for (Loan laon : meTooLoans)
            tempMeTooMinimalLoans.add(laon.getMinimalLoan());
        meTooMinimalLoans = tempMeTooMinimalLoans;
        List<Loan> tempLibraryLoans = new ArrayList<>();
        tempLibraryLoans.addAll(myLoans);
        tempLibraryLoans.addAll(meTooLoans);
        libraryLoans = tempLibraryLoans;
        List<Long> tempEndAt = new ArrayList<>();
        List<Long> tempStartedAt = new ArrayList<>();
        List<String> tempGameId = new ArrayList<>();
        long minEndAt = Long.MAX_VALUE;
        long maxEndAt = Long.MIN_VALUE;
        long minStartedAt = Long.MAX_VALUE;
        long maxStartedAt = Long.MIN_VALUE;
        for (Loan laon : libraryLoans){
            tempEndAt.add(laon.getEndAt());
            tempStartedAt.add(laon.getStartedAt());
            tempGameId.add(laon.getGame().getId());
            if (minEndAt > laon.getEndAt())
                minEndAt = laon.getEndAt();
            if (maxEndAt < laon.getEndAt())
                maxEndAt = laon.getEndAt();
            if (minStartedAt > laon.getStartedAt())
                minStartedAt = laon.getStartedAt();
            if (maxStartedAt < laon.getStartedAt())
                maxStartedAt = laon.getStartedAt();

        }
        gameId = tempGameId;
        endAt = tempEndAt;
        startedAt = tempStartedAt;
        endAtMinMax = new MinMax<>(minEndAt, maxEndAt);
        startedAtMinMax = new MinMax<>(minStartedAt, maxStartedAt);
        List<MinimalUserWithLoans> tempLibraryData = new ArrayList<>();
        tempLibraryData.add(new MinimalUserWithLoans(myLoans));
        tempLibraryData.add(new MinimalUserWithLoans(meTooLoans));
        libraryData = tempLibraryData;
        List<MinimalUser> tempUsers = new ArrayList<>();
        for (MinimalUserWithLoans userWithLoans : libraryData)
            tempUsers.add(userWithLoans.getMinimalUser());
        libraryUsers = tempUsers;
        List<User> tempAllUsers = new ArrayList<>();
        tempAllUsers.add(my);
        tempAllUsers.add(meToo);
        libraryAllUsers = tempAllUsers;
        List<String> tempTypes = new ArrayList<>();
        List<String> tempNameGame = new ArrayList<>();
        List<String> tempIllustrators = new ArrayList<>();
        List<String> tempAuthors = new ArrayList<>();
        List<String> tempCompanys = new ArrayList<>();
        List<String> tempLocations = new ArrayList<>();
        int minNbPlayer = Integer.MAX_VALUE;
        int maxNbPlayer = Integer.MIN_VALUE;
        for (Game game : games) {
            if (game.getType() != null && !tempTypes.contains(game.getType().toLowerCase().trim()))
                tempTypes.add(game.getType().toLowerCase().trim());
            if (!tempNameGame.contains(game.getName()))
                tempNameGame.add(game.getName());
            if (game.getIllustrator() != null && !tempIllustrators.contains(game.getIllustrator()))
                tempIllustrators.add(game.getIllustrator());
            if (game.getAuthor() != null && !tempAuthors.contains(game.getAuthor()))
                tempAuthors.add(game.getAuthor());
            if (game.getCompanyName() != null && !tempCompanys.contains(game.getCompanyName().toLowerCase().trim()))
                tempCompanys.add(game.getCompanyName().toLowerCase().trim());
            if (game.getLocation() != null && !tempLocations.contains(game.getLocation().toLowerCase().trim()))
                tempLocations.add(game.getLocation().toLowerCase().trim());
            if (game.getNbPlayer() != null) {
                if (minNbPlayer > game.getNbPlayer().getMin())
                    minNbPlayer = game.getNbPlayer().getMin();
                if (maxNbPlayer != Integer.MAX_VALUE) {
                    if (game.getNbPlayer().getMax() != null && maxNbPlayer < game.getNbPlayer().getMax())
                        maxNbPlayer = game.getNbPlayer().getMax();
                    if (game.getNbPlayer().getAndMore())
                        maxNbPlayer = Integer.MAX_VALUE;
                }
            }
        }
        Collections.sort(tempTypes);
        Collections.sort(tempCompanys);
        Collections.sort(tempLocations);
        types = tempTypes;
        companyNames = tempCompanys;
        nameGame = tempNameGame;
        illustrators = tempIllustrators;
        authors = tempAuthors;
        locations = tempLocations;
        nbPlayerMinMax = new MinMax<>(minNbPlayer, maxNbPlayer);
    }

    public List<Game> defaultGameValue() {
        List<Game> tempGames = new ArrayList<>();
        tempGames.add(new Game("T111", "Trésor de Glace", GameState.BORROWED, "Haba",
            new NbPlayer(2, 4), 5, new GameTime(15), "Adresse",
            "Les jeunes dragons ont découvert un trésor de glace extraordinaire: une colonne glacée contenant des pierres étincelantes. ... Avec l'aide de papa dragon, les joueurs retirent les anneaux gelés les uns après les autres et font fondre la colonne glacée.",
            null, null, "https://youtu.be/S2zX1m_63wE", "GB")
        );
        tempGames.add(new Game("A058", "Acrobates", GameState.BORROWED, "Ravensburger",
            new NbPlayer(1, 4), 5, new GameTime(15, 20), "Adresse",
            "Un amusant jeu d’adresse pour toute la famille ! Champions de l’équilibre, les acrobates s’accrochent par les bonnets, les jambes ou les pieds dans tous les sens. Peu importe la manière, du moment que les acrobates colorés restent accrochés. La tour ...",
            "Theo et Ora Coster", null, null, "GB")
        );
        tempGames.add(new Game("H027", "Halli Galli", GameState.BORROWED,
            "Gigamic", new NbPlayer(2, 6), 4, new GameTime(15, true),
            "Cartes", "Neuf clowns de couleurs diverses sont dans le manège. La plupart des clowns sont joyeux, mais certains sont tristes, parce qu'ils ont perdu leur chapeau. A chaque fois qu’il y a deux clowns identiques et joyeux dans le manège, il faut frapper la sonn...",
            "Haim Shafir", null, null, null)
        );
        tempGames.add(new Game("P061", "Place de la loi", GameState.BORROWED, "Gallimard",
            new NbPlayer(2, 4), 6, new GameTime(TimeUnit.HOURS, 1),
            "Apprentisage", "Pour apprendre la loi et devenir citoyen en s’amusant.\nLe jeu est conçu pour réunir parents et enfants, enseignants et élèves, adultes et jeunes, nourrir le dialogue et les amener à réfléchir ensemble aux valeurs qui fondent la loi",
            null, "Bidule Truc", null, null)
        );
        tempGames.add(new Game("H026", "Halli Galli", GameState.BROKEN
            .fork("This game has been too worn out, in this sense we will take it away in the long run."),
            "Gigamic", new NbPlayer(2, 6), 4, new GameTime(15, true),
            "Cartes", "Neuf clowns de couleurs diverses sont dans le manège. La plupart des clowns sont joyeux, mais certains sont tristes, parce qu'ils ont perdu leur chapeau. A chaque fois qu’il y a deux clowns identiques et joyeux dans le manège, il faut frapper la sonn...",
            "Haim Shafir", null, null, null)
        );
        tempGames.add(new Game("P187", "PERPLEXUS niveau 1", null,
            1, null, null, "Casse-tête",
            "Un labyrinthe tridimensionnel", null, null,
            null, null)
        );
        tempGames.add(new Game("P182", "PERPLEXUS niveau 1", null,
            1, null, null, "Casse-tête",
            "Un labyrinthe tridimensionnel", null, null,
            null, null)
        );
        tempGames.add(new Game("P183", "PERPLEXUS niveau 1", null,
            1, null, null, "Casse-tête",
            "Un labyrinthe tridimensionnel", null, null,
            null, null)
        );
        tempGames.add(new Game("P184", "PERPLEXUS niveau 1", null,
            1, null, null, "Casse-tête",
            "Un labyrinthe tridimensionnel", null, null,
            null, null)
        );
        return tempGames;
    }

    /**
     * Method for insert base data in clean db
     */
    public void insertData() {
        insertData(true);
    }

    /**
     * Method for insert base data and clean db if requested
     * @param cleanDb Boolean for know if db are drop before insert
     */
    public void insertData(boolean cleanDb) {
        if (cleanDb)
            cleanRepo();
        for (Role role : Role.ALL_ROLES)
            insertRole(role);
        for (User user : users)
            insertUser(user);
        for (UserOAuth userOAuth : oAuthList)
            insertUserOAuth(userOAuth);
        for (Token token : userTokens)
            insertToken(token);
        for (Game game : games)
            insertGame(game);
        for (Loan loan : libraryLoans)
            insertLoan(loan);
    }
}
