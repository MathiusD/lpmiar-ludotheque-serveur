package lp.projects.init.serveur.core.user;

import lp.projects.init.serveur.core.game.MinimalGame;
import lp.projects.init.serveur.core.loan.Loan;
import org.junit.jupiter.api.Test;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class for test MinimalUserWithLoans
 */
public class MinimalUserWithLoansTest extends MinimalUserTest {

    /**
     * Assert MinimalUsersWithLoans are exactly same attributes value
     * @param first MinimalUserWithLoans represent first user
     * @param second MinimalUserWithLoans represent second user
     */
    public static void assertUsersAreStrictlyEquals(@NonNull MinimalUserWithLoans first,
                                                    @NonNull MinimalUserWithLoans second) {
        assertUsersAreStrictlyEquals(first.getMinimalUser(), second.getMinimalUser());
        assertEquals(first.loans, second.loans);
    }

    /**
     * For test Instantiation of MinimalUserWithLoans
     */
    @Test
    public void testMinimalUserWithLoansInstantiation() {
        MinimalUser tempFirst = new MinimalUser("ABC", "Example", "truc",
                "truc@mail.org", "Truc");
        MinimalGame game = new MinimalGame("machin", "truc");
        Loan loan = new Loan("zef", game, tempFirst, 42);
        MinimalUserWithLoans firstUser = new MinimalUserWithLoans(tempFirst, loan);
        MinimalUserWithLoans tempUser = new MinimalUserWithLoans(firstUser);
        assertUsersAreStrictlyEquals(firstUser, tempUser);
        assertUsersAreStrictlyEquals(firstUser, new MinimalUserWithLoans(loan));
        MinimalUserWithLoans otherUser = new MinimalUserWithLoans("PAS_BIDULE", "Truc",
            "Machin", "truc@mail.org", "Truc");
        Loan otherLoan = new Loan("Ba", game, otherUser, 4600);
        assertUsersAreStrictlyEquals(firstUser, new MinimalUserWithLoans(loan));
        assertNotEquals(firstUser, new MinimalUserWithLoans(otherLoan));
        assertUsersAreStrictlyEquals(firstUser, new MinimalUserWithLoans(loan, otherLoan));
        List<Loan> temp = new ArrayList<>();
        temp.add(null);
        temp.add(loan);
        assertUsersAreStrictlyEquals(firstUser, new MinimalUserWithLoans(temp));
        assertThrows(IllegalArgumentException.class, () ->
                        new MinimalUserWithLoans((Loan) null, null),
                "This method required at least one Loan"
        );
        assertThrows(IllegalArgumentException.class, () ->
                        new MinimalUserWithLoans(new ArrayList<>()),
                "This method required at least one Loan"
        );
    }
}
