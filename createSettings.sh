MathiusTomcat_USER=$1;
MathiusTomcat_PASS=$2;
IUTTomcat_USER=$3;
IUTTomcat_PASS=$4;
DestFolder="${HOME}/.m2";
DestFile="${DestFolder}/settings.xml";
if ! [ -d "${DestFolder}" ]; then
  mkdir "${DestFolder}";
fi
if [ -f "${DestFile}" ]; then
  rm "${DestFile}";
fi
echo "<settings xsi:schemaLocation=\"http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd\"
    xmlns=\"http://maven.apache.org/SETTINGS/1.1.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">
    <servers>
        <server>
            <id>MathiusTomCat</id>
            <username>${MathiusTomcat_USER}</username>
            <password>${MathiusTomcat_PASS}</password>
        </server>
        <server>
            <id>IUTNantesTomCat</id>
            <username>${IUTTomcat_USER}</username>
            <password>${IUTTomcat_PASS}</password>
        </server>
    </servers>
</settings>" >> "${DestFile}";