package lp.projects.init.serveur.core.game;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.*;

import java.util.UUID;

import static javax.persistence.InheritanceType.SINGLE_TABLE;

/**
 * Class represent more informations than MinimalGame of this Game of this library
 */
@Entity
@Inheritance(strategy=SINGLE_TABLE)
@DiscriminatorValue("SHORT_GAME")
@PrimaryKeyJoinColumn(name = "game_id")
@Schema(description = "More Information than MinimalGame of Game of this Library")
public class ShortGame extends MinimalGame {

    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "min", column = @Column(name = "game_nb_player_min")),
        @AttributeOverride(name = "max", column = @Column(name = "game_nb_player_max")),
        @AttributeOverride(name = "andMore", column = @Column(name = "game_nb_player_and_more"))
    })
    @Schema(description = "Recommended player for this Game")
    protected NbPlayer nbPlayer;
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "min", column = @Column(name = "game_year_recommended_min")),
        @AttributeOverride(name = "max", column = @Column(name = "game_year_recommended_max")),
        @AttributeOverride(name = "andMore", column = @Column(name = "game_year_recommended_and_more"))
    })
    @Schema(description = "Recommended age")
    protected YearRecommended yearRecommended;
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "timeUnit", column = @Column(name = "game_time_unit")),
        @AttributeOverride(name = "minTime", column = @Column(name = "game_time_min")),
        @AttributeOverride(name = "maxTime", column = @Column(name = "game_time_max")),
        @AttributeOverride(name = "averageTime", column = @Column(name = "game_time_average")),
        @AttributeOverride(name = "upToMin", column = @Column(name = "game_time_up_to_min"))
    })
    @Schema(description = "Average Time for play this Game")
    protected GameTime time;
    @Column(name = "game_type")
    @Schema(description = "For class this game", example = "Strategy")
    protected String type;

    /**
     * Shit Constructor for ShortGame Stored in DB
     * Used only by reflexion !
     */
    protected ShortGame() {
        super();
    }

    /**
     * Basic constructor for ShortGame
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    @JsonCreator
    @PersistenceConstructor
    public ShortGame(@NonNull String id, @NonNull String name, @NonNull GameState state, @Nullable NbPlayer nbPlayer,
                     @Nullable YearRecommended yearRecommended, @Nullable GameTime time, @Nullable String type) {
        super(id, name, state);
        this.nbPlayer = nbPlayer;
        this.yearRecommended = yearRecommended;
        this.time = time;
        this.type = type;
    }

    /**
     * Basic constructor for ShortGame
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, @NonNull GameState state, @Nullable NbPlayer nbPlayer,
                     int minYear, int maxYear, @Nullable GameTime time, @Nullable String type) {
        this(id, name, state, nbPlayer, new YearRecommended(minYear, maxYear),
            time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, @NonNull GameState state, @Nullable NbPlayer nbPlayer,
                     int actualYear, @Nullable GameTime time, @Nullable String type) {
        this(id, name, state, nbPlayer, new YearRecommended(actualYear),
                time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, @NonNull GameState state, @Nullable NbPlayer nbPlayer,
                     @Nullable YearRecommended yearRecommended, @Nullable GameTime time, @Nullable String type) {
        this(UUID.randomUUID().toString(), name, state, nbPlayer, yearRecommended, time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, @NonNull GameState state, @Nullable NbPlayer nbPlayer,
                     int actualYear, @Nullable GameTime time, @Nullable String type) {
        this(UUID.randomUUID().toString(), name, state, nbPlayer, new YearRecommended(actualYear),
            time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, @NonNull GameState state, @Nullable NbPlayer nbPlayer,
                     int minYear, int maxYear, @Nullable GameTime time, @Nullable String type) {
        this(UUID.randomUUID().toString(), name, state, nbPlayer, new YearRecommended(minYear, maxYear),
            time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, @Nullable NbPlayer nbPlayer,
                     @Nullable YearRecommended yearRecommended, @Nullable GameTime time, @Nullable String type) {
        this(id, name, DEFAULT_STATE, nbPlayer, yearRecommended, time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, @Nullable NbPlayer nbPlayer,
                     int actualYear, @Nullable GameTime time, @Nullable String type) {
        this(id, name, DEFAULT_STATE, nbPlayer, new YearRecommended(actualYear), time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, @Nullable NbPlayer nbPlayer,
                     int minYear, int maxYear, @Nullable GameTime time, @Nullable String type) {
        this(id, name, DEFAULT_STATE, nbPlayer, new YearRecommended(minYear, maxYear), time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, @Nullable NbPlayer nbPlayer,
                     @Nullable YearRecommended yearRecommended, @Nullable GameTime time, @Nullable String type) {
        this(name, DEFAULT_STATE, nbPlayer, yearRecommended, time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, @Nullable NbPlayer nbPlayer,
                     int actualYear, @Nullable GameTime time, @Nullable String type) {
        this(name, DEFAULT_STATE, nbPlayer, new YearRecommended(actualYear), time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param name String represent Name of Game
     * @param nbPlayer NbPlayer represent Number of Player recommended for Game
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, @Nullable NbPlayer nbPlayer,
                     int minYear, int maxYear, @Nullable GameTime time, @Nullable String type) {
        this(name, DEFAULT_STATE, nbPlayer, new YearRecommended(minYear, maxYear), time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, int actualNbPlayer,
                     @Nullable YearRecommended yearRecommended, @Nullable GameTime time, @Nullable String type) {
        this(id, name, new NbPlayer(actualNbPlayer), yearRecommended, time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, int actualNbPlayer,
                     int actualYear, @Nullable GameTime time, @Nullable String type) {
        this(id, name, new NbPlayer(actualNbPlayer), new YearRecommended(actualYear), time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, int actualNbPlayer,
                     int minYear, int maxYear, @Nullable GameTime time, @Nullable String type) {
        this(id, name, new NbPlayer(actualNbPlayer), new YearRecommended(minYear, maxYear),
            time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param name String represent Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, int actualNbPlayer,
                     @Nullable YearRecommended yearRecommended, @Nullable GameTime time, @Nullable String type) {
        this(name, new NbPlayer(actualNbPlayer), yearRecommended, time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param name String represent Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, int actualNbPlayer,
                     int actualYear, @Nullable GameTime time, @Nullable String type) {
        this(name, new NbPlayer(actualNbPlayer), new YearRecommended(actualYear), time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param name String represent Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, @NonNull Integer actualNbPlayer,
                     int minYear, int maxYear, @Nullable GameTime time, @Nullable String type) {
        this(name, new NbPlayer(actualNbPlayer), new YearRecommended(minYear, maxYear), time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, int actualNbPlayer, @Nullable GameTime time,
                     @Nullable String type) {
        this(id, name, new NbPlayer(actualNbPlayer), null, time, type);
    }

    /**
     * Basic constructor for ShortGame
     * @param name String represent Name of Game
     * @param actualNbPlayer Integer used for create NbUser with only actual number of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, int actualNbPlayer, @Nullable GameTime time,
                     @Nullable String type) {
        this(name, new NbPlayer(actualNbPlayer), null, time, type);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, int minNbPlayer, int maxNbPlayer,
                     @Nullable YearRecommended yearRecommended, @Nullable GameTime time, @Nullable String type) {
        this(id, name, new NbPlayer(minNbPlayer, maxNbPlayer), yearRecommended, time, type);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, int minNbPlayer, int maxNbPlayer,
                     int minYear, int maxYear, @Nullable GameTime time, @Nullable String type) {
        this(id, name, new NbPlayer(minNbPlayer, maxNbPlayer), new YearRecommended(minYear, maxYear),
            time, type);
    }

    /**
     * Basic constructor for Game
     * @param id String represent id of Game
     * @param name String represent Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String id, @NonNull String name, int minNbPlayer, int maxNbPlayer,
                     @NonNull Integer actualYear, @Nullable GameTime time, @Nullable String type) {
        this(id, name, new NbPlayer(minNbPlayer, maxNbPlayer), new YearRecommended(actualYear),
            time, type);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param yearRecommended YearRecommended represent Year recommended for play this game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, int minNbPlayer, int maxNbPlayer,
                     @Nullable YearRecommended yearRecommended, @Nullable GameTime time, @Nullable String type) {
        this(name, new NbPlayer(minNbPlayer, maxNbPlayer), yearRecommended, time, type);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param minYear Integer represent minimum year for this Game
     * @param maxYear Integer represent maximum year for this Game
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, int minNbPlayer, int maxNbPlayer,
                     int minYear, int maxYear, @Nullable GameTime time, @Nullable String type) {
        this(name, new NbPlayer(minNbPlayer, maxNbPlayer), minYear, maxYear, time, type);
    }

    /**
     * Basic constructor for Game
     * @param name String represent Name of Game
     * @param minNbPlayer Integer used for create NbUser with maximum of player
     * @param maxNbPlayer Integer used for create NbUser with maximum of player
     * @param actualYear Integer used for create NbUser with actual year of player
     * @param time GameTime represent average duration of Game
     * @param type String represent type of this Game
     */
    public ShortGame(@NonNull String name, int minNbPlayer, int maxNbPlayer,
                     int actualYear, @Nullable GameTime time, @Nullable String type) {
        this(name, new NbPlayer(minNbPlayer, maxNbPlayer), actualYear, time, type);
    }

    /**
     * Constructor of ShortGame by MinimalGame or child
     * @param otherGame MinimalGame used for extract informations
     */
    public ShortGame(@NonNull MinimalGame otherGame) {
        this(otherGame.getId(), otherGame.getName(), otherGame.getGameState(),
                null, null, null, null);
    }

    /**
     * Constructor of ShortGame by ShortGame or child
     * @param otherGame ShortGame used for extract informations
     */
    public ShortGame(@NonNull ShortGame otherGame) {
        this(otherGame.getId(), otherGame.getName(), otherGame.getGameState(), otherGame.getNbPlayer(),
            otherGame.getYearRecommended(), otherGame.getTime(), otherGame.getType());
    }

    /**
     * Method for generate ShortGame related to this Game
     * @return shortGame
     */
    @JsonIgnore
    @NonNull
    public ShortGame getShortGame() {
        return new ShortGame(this);
    }

    /**
     * Getter for state of Game
     * @return nbPlayer
     */
    @Nullable
    public NbPlayer getNbPlayer() {
        return nbPlayer;
    }

    /**
     * Getter for year recommended of Game
     * @return yearRecommended
     */
    @Nullable
    public YearRecommended getYearRecommended() {
        return yearRecommended;
    }

    /**
     * Getter for time of Game
     * @return time
     */
    @Nullable
    public GameTime getTime() {
        return time;
    }

    /**
     * Getter for type of Game
     * @return type
     */
    @Nullable
    public String getType() {
        return type;
    }
}
